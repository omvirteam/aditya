<?php session_start();
if(isset($_SESSION['userName']))
{
	include_once('include/config.php');
	if(isset($_GET['delete_id']))
	{
		$id = $_GET['delete_id'];
		
		$q = "select logo from company where companyId = '$id'";
		$rs = mysqli_query($con,$q);
		$row = mysqli_fetch_array($rs);
		$name = $row['logo'];
			
		$deleteCompanyQry = "delete from company where companyId = '$id'";
		$rsDeleteCompany = mysqli_query($con,$deleteCompanyQry);
		if($rsDeleteCompany)
		{
			$_SESSION['session'] = "Record Deleted Successfully";
			$temp = "logo/".$name;
			unlink($temp);
			header('location:createCompany.php');
		}
	}
	$selectCompanyQry = "select * from company";	
	$rsSelectCompany = mysqli_query($con,$selectCompanyQry) or print mysqli_error($con);
	if(!$rsSelectCompany)
	{
		echo mysqli_error($con);die;
	}
	if(isset($_POST['ok']))
	{
		$reportHeader = $_POST['report_header1']." ".$_POST['report_header2'];
		$address = $_POST['address1']." ".$_POST['address2']." ".$_POST['address3'];
		$name = $_FILES["logo_image"]["name"];
		$fd = $_POST['from_date'];
		$td = $_POST['to_date'];
		$fromDate = date('Y-m-d',strtotime($fd));
		$toDate = date('Y-m-d',strtotime($td));
	
		$companyQry = "insert into company (state,number,companyName,companyShortName,company,groupName,logo,natureOfBusiness,";
		$companyQry .= "fromDate,toDate,securityType,reportHeader,address,city,pincode,phone,mobile,fax,email,website,";
		$companyQry .= "tinNo,tinCstNo,panNo,exciseNo,tcsNo,serviceTaxNo,gstNo) values ";
		$companyQry .= "('".$_POST['state']."',".$_POST['number'].",'".$_POST['company_name']."','".$_POST['company_short_name']."',";
		$companyQry .= "'".$_POST['company']."','".$_POST['group_name']."','$name','".$_POST['nature_of_business']."',";
		$companyQry .= "'$fromDate','$toDate','".$_POST['security_type']."','$reportHeader',";
		$companyQry .= "'$address','".$_POST['city']."','".$_POST['pincode']."',";
		$companyQry .= "'".$_POST['phone']."','".$_POST['mobile']."','".$_POST['fax']."','".$_POST['email']."','".$_POST['website']."',";
		$companyQry .= "'".$_POST['tin_no']."','".$_POST['tin_cst_no']."','".$_POST['pan_no']."','".$_POST['excise_no']."','".$_POST['tcs_no']."',";
		$companyQry .= "'".$_POST['service_tax_no']."','".$_POST['gst_no']."')";
		$rsCompany = mysqli_query($con,$companyQry);
		if($rsCompany)
		{
			$uploads_dir = "logo";
			$tmp_name = $_FILES["logo_image"]["tmp_name"];
			move_uploaded_file($tmp_name, "$uploads_dir/$name");
			$_SESSION['success']="Record inserted successfully";
			header('location:createCompany.php');
		}
		else
		{
			$_SESSION['error']="Record insert Fail";
			echo mysqli_error($con);die;
		}
	}
	
?>
<!DOCTYPE html>
<html>
<head>
	<?php include("include/header.php"); ?>
	<script type="text/javascript">
		window.onload = function(){
			setTimeout(function () {document.getElementById('msg').style.display='none'}, 3000);
		}
		
	</script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <?php include("include/mainheader.php"); ?>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Aditya</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <?php include("include/menu.php"); ?>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1 align="center">
        Company Creation
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Create Company</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="box-body">
			<div id="msg">
				<?php include("msg.php"); ?>
			</div>
			<form action="" method="post" name="create_company" id="create_company" enctype="multipart/form-data">
				<div class="form-group has-feedback">
					<div class="col-xs-12">
						<div class="nav-tabs-custom">
							<ul class="nav nav-tabs">
							  <li class="active"><a href="#company_details" data-toggle="tab">Company Details</a></li>
							  <li><a href="#address_details" data-toggle="tab">Address Details</a></li>
							  <li><a href="#other_details" data-toggle="tab">Other Details</a></li>
							</ul>
							<div class="tab-content">
								<div class="active tab-pane" id="company_details">
									<div class="col-xs-6">
										<div class="row">
											<div class="col-xs-4">
												<label>State : </label>
											</div>
											<div class="col-xs-8">
												<select class="form-control" name="state">
													<option value="">Select</option>
													<option>Gujarat</option>
													<option>Maharashtra</option>
													<option>Rajsthan</option>
												</select>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-4">
												<label>Number</label>
											</div>
											<div class="col-xs-8">
												<input type="text" class="form-control" placeholder="Number" name="number"/>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-4">
												<label>Company Name</label>
											</div>
											<div class="col-xs-8">
												<input type="text" class="form-control" placeholder="Company Name" name="company_name"/>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-4">
												<label>Company Short Name</label>
											</div>
											<div class="col-xs-8">
												<input type="text" class="form-control" placeholder="Company Short Name" name="company_short_name"/>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-4">
												<label>Company : </label>
											</div>
											<div class="col-xs-8">
												<select class="form-control" name="company">
													<option value="">Select</option>
													<option>With Stock</option>
												</select>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-4">
												<label>Group Name</label>
											</div>
											<div class="col-xs-8">
												<input type="text" class="form-control" placeholder="Group Name" name="group_name"/>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-4">
												<label>Logo Image</label>
											</div>
											<div class="col-xs-8">
												<input type="file" class="form-control" name="logo_image"/>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-4">
												<label>Nature of Business</label>
											</div>
											<div class="col-xs-8">
												<select class="form-control" name="nature_of_business">
													<option value="">Select</option>
													<option>Bullion</option>
													<option>Gold and Siver Manufacture</option>
													<option>Retail</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-xs-6">
										<br/>
										<br/>
										<div class="row">
											<div class="col-xs-3">
												<label>&nbsp;</label>
											</div>
											<div class="col-xs-6">
												<label>Financial Year</label>
											</div>
											<div class="col-xs-3">
												<label>&nbsp;</label>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-4">
												<label>From Date</label>
											</div>
											<div class="col-xs-8">
												<input class="datepicker" type="text" class="form-control" name="from_date" placeholder="From Date"/>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-4">
												<label>To Date</label>
											</div>
											<div class="col-xs-8">
												<input class="datepicker" type="text" class="form-control" name="to_date" placeholder="To Date"/>
											</div>
										</div>
										<br/>
										<br/>
										<div class="row">
											<div class="col-xs-3">
												<label>&nbsp;</label>
											</div>
											<div class="col-xs-6">
												<label>Security</label>
											</div>
											<div class="col-xs-3">
												<label>&nbsp;</label>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-4">
												<label>Security Type : </label>
											</div>
											<div class="col-xs-8">
												<select class="form-control" name="security_type">
													<option value="">Select</option>
													<option>Normal</option>
												</select>
											</div>
										</div>
									</div>
									<div class="row">
											<div class="col-xs-3">
												<label>&nbsp;</label>
											</div>
											<div class="col-xs-6">
												<label>Report Header</label>
											</div>
											<div class="col-xs-3">
												<label>&nbsp;</label>
											</div>
									</div>
									<div class="row">
										<div class="col-xs-4">
											<label>Report Header : </label>
										</div>
										<div class="col-xs-8">
											<input type="text" class="form-control" placeholder="" name="report_header1"/>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-4">
											<label>&nbsp;</label>
										</div>
										<div class="col-xs-8">
											<input type="text" class="form-control" placeholder="" name="report_header2"/>
										</div>
									</div>
							
								</div>
								<div class="tab-pane" id="address_details">
									<div class="row">
										<div class="col-xs-2">
											<label>Address : </label>
										</div>
										<div class="col-xs-4">
											<input type="text" class="form-control" placeholder="Address 1" name="address1"/>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-2">
											<label>&nbsp;</label>
										</div>
										<div class="col-xs-4">
											<input type="text" class="form-control" placeholder="Address 2" name="address2"/>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-2">
											<label>&nbsp;</label>
										</div>
										<div class="col-xs-4">
											<input type="text" class="form-control" placeholder="Address 3" name="address3"/>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-2">
											<label>City</label>
										</div>
										<div class="col-xs-4">
											<input type="text" class="form-control" placeholder="City" name="city"/>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-2">
											<label>Pin code</label>
										</div>
										<div class="col-xs-4">
											<input type="text" class="form-control" placeholder="Pin Code" name="pincode"/>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-2">
											<label>Phone No</label>
										</div>
										<div class="col-xs-4">
											<input type="text" class="form-control" placeholder="Phone Number" name="phone"/>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-2">
											<label>Mobile</label>
										</div>
										<div class="col-xs-4">
											<input type="text" class="form-control" placeholder="Mobile Number" name="mobile"/>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-2">
											<label>Fax</label>
										</div>
										<div class="col-xs-4">
											<input type="text" class="form-control" placeholder="Fax" name="fax"/>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-2">
											<label>E-Mail</label>
										</div>
										<div class="col-xs-4">
											<input type="text" class="form-control" placeholder="E-Mail" name="email"/>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-2">
											<label>Website</label>
										</div>
										<div class="col-xs-4">
											<input type="text" class="form-control" placeholder="Website" name="website"/>
										</div>
									</div>
								</div>
								<div class="tab-pane" id="other_details">
									<div class="row">
										<div class="col-xs-2">
											<label>Tin No. :</label>
										</div>
										<div class="col-xs-4">
											<input type="text" class="form-control" placeholder="Tin No" name="tin_no"/>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-2">
											<label>Tin CST No. :</label>
										</div>
										<div class="col-xs-4">
											<input type="text" class="form-control" placeholder="Tin CST No" name="tin_cst_no"/>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-2">
											<label>PAN No. :</label>
										</div>
										<div class="col-xs-4">
											<input type="text" class="form-control" placeholder="PAN No" name="pan_no"/>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-2">
											<label>Excise No. :</label>
										</div>
										<div class="col-xs-4">
											<input type="text" class="form-control" placeholder="Excise No" name="excise_no"/>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-2">
											<label>TCS No. :</label>
										</div>
										<div class="col-xs-4">
											<input type="text" class="form-control" placeholder="TCS No" name="tcs_no"/>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-2">
											<label>Service Tax No. :</label>
										</div>
										<div class="col-xs-4">
											<input type="text" class="form-control" placeholder="Service Tax No" name="service_tax_no"/>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-2">
											<label>GST No. :</label>
										</div>
										<div class="col-xs-4">
											<input type="text" class="form-control" placeholder="GST No" name="gst_no"/>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
								<div class="col-xs-2">
									<button type="submit" class="btn btn-primary btn-block btn-flat" name="ok">Submit</button>
								</div>
								<div class="col-xs-2">
									<button type="reset" class="btn btn-primary btn-block btn-flat" name="reset" onClick="document.location.href='createCompany.php'"/>Reset</button>
								</div>
								<!-- /.col -->
						</div>
					</div>
				</div>
			</form>
		</div>
		<div class="box-body table-responsive no-padding">
			<div class="col-xs-12">
				<div class="row">
					<table class="table table-hover">
						<tr>
						  <th>Action</th>		
						  <th>ID</th>
						  <th>Company Name</th>
						  <th>Group Name</th>
						  <th>Nature of Business</th>
						  <th>City</th>
						  <th>Phone</th>
						  <th>Mobile</th>
						</tr>
					<?php while($rowSelectCompany = mysqli_fetch_array($rsSelectCompany)) { ?>
						<tr>
						  <!--<td><a href="createCompany.php?delete_id=<?php echo $rowSelectCompany['companyId']; ?>">Delete</a></td>-->
						  <td><button type="button" id="modal_call" data-id="<?php echo $rowSelectCompany['companyId']; ?>" >Delete</button></td>
						  <td><?php echo $rowSelectCompany['companyId']; ?></td>
						  <td><?php echo $rowSelectCompany['companyName']; ?></td>
						  <td><?php echo $rowSelectCompany['groupName']; ?></td>
						  <td><?php echo $rowSelectCompany['natureOfBusiness']; ?></td>
						  <td><?php echo $rowSelectCompany['city']; ?></td>
						  <td><?php echo $rowSelectCompany['phone']; ?></td>
						  <td><?php echo $rowSelectCompany['mobile']; ?></td>
						</tr>	
					<?php } ?>
					</table>
				</div>
			</div>
		</div>
		<div class="modal modal-danger" class="modal fade" role="dialog" id="myModal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Confirm Delete</h4>
					</div>
					<div class="modal-body">
						<p>Are You Sure You Want To Delete ?? &hellip;</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-outline" id="delete_record" >Save changes</button>
					</div>
				</div>
			</div>
		</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <?php include("include/footer.php"); ?>
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
	<?php include("include/filelinks.php"); ?>
	<script type="text/javascript">
	var t = "";
	$('.datepicker').datepicker({
		format: 'dd-mm-yyyy',
		autoclose: true
	});
	$("#modal_call").click(function(){
		$("#myModal").modal('show');
		t = $(this).data('id');
	});	
	$("#delete_record").click(function(){
		$(location).attr('href',"createCompany.php?delete_id="+t);
	});
	</script>
</body>
</html>
<?php
}
else
{
	header('location:index.php');
}
?>