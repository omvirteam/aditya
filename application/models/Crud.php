<?php 
class Crud extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	
	function insert($table_name,$data_array){
		if($this->db->insert($table_name,$data_array))
		{
			return $this->db->insert_id();
		}
		return false;
	}
	
	function get_all_records($table_name,$order_by_column,$order_by_value){
		$this->db->select("*");
		$this->db->from($table_name);
		$this->db->order_by($order_by_column,$order_by_value);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function get_all_with_where($table_name,$order_by_column,$order_by_value,$where_array){
		$this->db->select("*");
		$this->db->from($table_name);
		$this->db->where($where_array);
		$this->db->order_by($order_by_column,$order_by_value);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function get_column_value_by_id($tbl_name,$column_name,$where_id)
	{
		$this->db->select("*");
		$this->db->from($tbl_name);
		$this->db->where($where_id);
		$query = $this->db->get();
		return $query->row($column_name);
	}
	
	function get_row_by_id($table_name,$where_id){
		$this->db->select("*");
		$this->db->from($table_name);
		$this->db->where($where_id);
		$query = $this->db->get();
		return $query->result();
	}
	
	function delete($table_name,$where_array){		
		$result = $this->db->delete($table_name,$where_array);
		return $result;
	}
	
	function update($table_name,$data_array,$where_array){
		$this->db->where($where_array);
		$rs = $this->db->update($table_name, $data_array);
		return $rs;
	}
	
	function get_state()
	{
		if(isset($_POST['country_id'])){
		
			$countryID =  $_POST['country_id'];
			$where = array('country_id'=>$countryID);
			$result = $this->get_all_with_where('state','state_id','ASC',$where);
			$option = '<option value="">Select State</option>';
		
			if($result)
			{
				foreach ($result as $row){
					$state_id = $row->state_id;
					$state = $row->state;
					$option .= '<option value="'.$state_id.'">'.$state.'</option>';
				}
			}
			return $option;
		}
	}
	
	function search_leads($data){
		
			$this->db->select("*");
			$this->db->from("lead_details");
			
			if($data['lead_source']!="")
				$this->db->where("lead_source",$data['lead_source']);
			
			if($data['lead_stage']!="")
				$this->db->where("lead_stage",$data['lead_stage']);
				
			if($data['industry_type']!="")
				$this->db->where("industry_type",$data['industry_type']);
				
			if($data['lead_owner']!="")
				$this->db->where("lead_owner",$data['lead_owner']);
				
			if($data['assigned_to']!="")
				$this->db->where("assigned_to",$data['assigned_to']);
				
			$query = $this->db->get();
			return $result = $query->result();
			
	}
	
	function upload_file($name, $path)
	{
		$config['upload_path'] = $path;
		$config ['allowed_types'] = '*';
		$this->upload->initialize($config);
		if($this->upload->do_upload($name))
		{
			$upload_data = $this->upload->data();
			return $upload_data['file_name'];
		}
		return false;
	}
	
}
