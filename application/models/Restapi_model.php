<?php
ini_set('max_execution_time', 3000);

/**
 * Class Ws_model
 */
class Restapi_model extends CI_Model
{
	protected $CurrCompetition = 0;
	function __construct()
	{
		ini_set('max_execution_time', 300);
		// Call the Model constructor
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper("file");
		$this->CurrCompetition = $this->getCurrCompetition();
	}

	/**
	 * @param $request_params
	 * @return bool
	 */
	function login($request_params)
	{
		if(!isset($request_params['provider_id'])){
			$username = $request_params['username'];
			$password = $request_params['password'];
			$login_data = array(
				'username' => $username,
				'password' => md5($password),
				'competition_id'=>isset($request_params['competition_id'])?$request_params['competition_id']:$this->CurrCompetition
			);
			$query = $this->db->get_where('users', $login_data);
			if ($query->num_rows() == 1) {
				if(isset($request_params['device_id'])){
					$this->db->where('user_id',$query->row()->user_id);
					$this->db->update('users',array('device_id'=>$request_params['device_id']));
				}
				$userData['user_id'] = $query->row()->user_id;
				$userData['username'] = $query->row()->username;
				$userData['email'] = $query->row()->email;
				if($query->row()->profile_pic_type == 1){
					$userData['profile_pic'] = $query->row()->profile_pic != null ? profile_pics_url('thumbs/'.$query->row()->profile_pic) : null;
				}else{
					$userData['profile_pic'] = $query->row()->profile_pic;
				}
				$userData['user_type'] = $query->row()->user_type;
				$userData['given_comments'] = $this->fetchGivenComments($query->row()->user_id);
				$userData['remain_votes_for_give'] = $this->getRemailVotesForGive($query->row()->user_id);
				if($query->row()->user_type == 2){
					$userData['received_votes'] = $query->row()->received_votes;
					$userData['identity_video'] = $query->row()->identity_video != null ? video_url('identity-video/'.$query->row()->identity_video) : null;
					$userData['city'] = $query->row()->city;
					$userData['address'] = $query->row()->address;
					$userData['images'] = $this->fetchContestantImages($query->row()->user_id);
					$userData['received_comments'] = $this->fetchReceivedComments($query->row()->user_id);
				}
				$userData['password'] = $request_params['password'];
				return $userData;
			} else {
				return false;
			}
		}else{
			return $this->social_login($request_params);
		}
	}

	/**
	 * @param $request_params
	 * @return bool
	 */
	function social_login($request_params)
	{
		if(!isset($request_params['provider_id'])){
			return false;
		}
		$this->db->select('user_id');
		$this->db->from('users');
		$this->db->where('provider_id',$request_params['provider_id']);
		$this->db->where('login_provider',isset($request_params['login_provider'])?$request_params['login_provider']:'INSTA');
		$this->db->limit('1');
		$query = $this->db->get();

		if($query->num_rows() > 0){
			return $this->getProfileData($query->row()->user_id);
		}else{
			if(isset($request_params['profile_pic']) && $request_params['profile_pic'] != '' && $request_params['profile_pic'] != null){
				$profile_pic = $this->upload_image($request_params['profile_pic'],image_dir('profile-pics/'),2);
				if($profile_pic){
					$profile_pic_type = 1;
				}else{
					return false;
				}
			}
			$user_data = array(
				'username' => isset($request_params['username'])?$request_params['username']:'',
				'email' => isset($request_params['email'])?$request_params['email']:null,
				'profile_pic' => $profile_pic,
				'profile_pic_type' => $profile_pic_type,
				'login_provider' => isset($request_params['login_provider']) ? $request_params['login_provider'] : 'INSTA',
				'provider_id' => isset($request_params['provider_id']) ? $request_params['provider_id'] : '',
				'device_id' => isset($request_params['device_id']) ? $request_params['device_id'] : '',
				'competition_id' => isset($request_params['competition_id'])?$request_params['competition_id']:$this->CurrCompetition,
				'user_type' => isset($request_params['user_type']) ? $request_params['user_type'] : 3,
				'created_at' => date('Y-m-d H:i:s')
			);
			$this->db->insert('users',$user_data);
			if ($this->db->insert_id() > 0){
				return $this->getProfileData($this->db->insert_id());
			} else {
				return false;
			}
		}
	}


	/**
	 * @param $request_params
	 * @return bool|string
	 */
	function register($request_params)
	{
		$competition_id = isset($request_params['competition_id'])?$request_params['competition_id']:$this->CurrCompetition;
		if (!$this->is_username_available($request_params['username'],$competition_id)) {
			return 'exist_username';
		} elseif (!$this->is_email_available($request_params['email'],$competition_id)) {
			return 'exist_email';
		} else {
			if (!empty($_FILES['profile_pic']['name'])) {
				$profile_pic = $this->upload_image('profile_pic',image_dir('profile-pics/'));
				if ($profile_pic != false) {

				} else {
					return 'profile_pic_error';
				}
			}else{
				if(isset($request_params['profile_pic']) && $request_params['profile_pic'] != null && $request_params['profile_pic'] != ''){
					$profile_pic = $this->upload_image($request_params['profile_pic'],image_dir('profile-pics/'),2);
				}
			}

			if (!empty($_FILES['identity_video']['name'])) {
				$identity_video = $this->upload_video('identity_video',video_dir('identity-video/'));
				if ($identity_video != false) {

				} else {
					return 'identity_video_error';
				}
			}
			
			$register_data = array('username' => $request_params['username'],
				'password' => md5($request_params['password']),
				'email' => $request_params['email'],
				'contact_no' => isset($request_params['contact_no'])?$request_params['contact_no']:null,
				'profile_pic' => isset($profile_pic) ? $profile_pic : null,
				'profile_pic_type' => isset($request_params['profile_pic_type']) ? $request_params['profile_pic_type'] : 1,
				'identity_video' => isset($identity_video) ? $identity_video : null,
				'address' => isset($request_params['address']) ? $request_params['address'] : null,
				'city' => isset($request_params['city']) ? $request_params['city'] : null,
				'user_type' => isset($request_params['user_type']) ? $request_params['user_type'] : 3,
				'login_provider' => isset($request_params['login_provider']) ? $request_params['login_provider'] : 'E',
				'provider_id' => isset($request_params['provider_id']) ? $request_params['provider_id'] : null,
				'device_id' => isset($request_params['device_id']) ? $request_params['device_id'] : null,
				'competition_id'=>isset($request_params['competition_id'])?$request_params['competition_id']:$this->CurrCompetition,
				'created_at' => date('Y-m-d H:i:s'));
				$emailData = array('email'=>$request_params['email'],'username'=>$request_params['username'],'password'=>$request_params['password']);
				$valid_mail = true;//$this->send_mail($request_params['email'],'photo-competition',$this->load->view('email/welcome-html',$emailData,true));
			if($valid_mail){
				$insert_status = $this->db->insert('users', $register_data);
				if ($insert_status) {
					$data['user_id'] = $this->db->insert_id();
					$data['username'] = $request_params['username'];
					$data['email'] = $request_params['email'];
					if(isset($request_params['profile_pic_type']) && $request_params['profile_pic_type'] == 2){
						$data['profile_pic'] = isset($profile_pic) ? $profile_pic : null;
					}else{
						$data['profile_pic'] = isset($profile_pic) ? profile_pics_url('thumbs/'.$profile_pic) : null;
					}
					$data['user_type'] = isset($request_params['user_type']) ? $request_params['user_type'] : 3;
					if($data['user_type'] == 2){
						$data['identity_video'] = isset($identity_video) ? video_url('identity-video/'.$identity_video) : null;
						$data['address'] = isset($request_params['address']) ? $request_params['address'] : null;
						$data['city'] = isset($request_params['city']) ? $request_params['city'] : null;
					}
					$data['password'] = $request_params['password'];
					return $data;
				} else {
					return false;
				}
			}else{
				return 'unvalid_email';
			}
		}

	}

	/**
	 * @param $request_params
	 * @return bool|string
	 */
	function forgot_password($request_params)
	{
		$login = '';
		$competition_id = isset($request_params['competition_id'])?$request_params['competition_id']:$this->CurrCompetition;
		if (isset($request_params['email'])) {
			$login = $request_params['email'];
		} elseif (isset($request_params['username'])) {
			$login = $request_params['username'];
		}

		if (strlen($login) > 0) {
			if (!is_null($user = $this->get_user_by_login($login,$competition_id))) {
				$data = array(
					'user_id' => $user->user_id,
					'username' => $user->username,
					'email' => $user->email,
					'new_pass_key' => md5(rand() . microtime()),
					'site_name' => 'photo-competition'
				);
				$this->set_password_key($user->user_id, $data['new_pass_key']);
				$status = $this->send_mail($user->email,'photo-competition Reset Password',$this->load->view('email/forgot_password-html',$data,true));
				if ($status == true || $status == 1 || $status == '1') {
					return true;
				} else {
					return 'not_send';
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 * @param $request_params
	 * @return bool
	 */
	function fetch_profile($request_params)
	{
		if (!isset($request_params['user_id'])  || !$this->is_val_available('users','user_id',$request_params['user_id'])) {
			return false;
		}
		$user_id = $request_params['user_id'];
		$this->db->select('user_id,username,email,profile_pic,profile_pic_type,identity_video,city,address,user_type,received_votes,given_votes_quota,given_votes');
		$this->db->from('users');
		$this->db->where('user_id', $user_id);
		$this->db->limit('1');
		$query = $this->db->get();
		if ($query->num_rows() == 1) {
			$userData['user_id'] = $query->row()->user_id;
			$userData['username'] = $query->row()->username;
			$userData['email'] = $query->row()->email;
			if($query->row()->profile_pic_type == 1){
				$userData['profile_pic'] = $query->row()->profile_pic != null ? profile_pics_url('thumbs/'.$query->row()->profile_pic) : null;
			}else{
				$userData['profile_pic'] = $query->row()->profile_pic;
			}
			$userData['user_type'] = $query->row()->user_type;
			$userData['given_comments'] = $this->fetchGivenComments($query->row()->user_id);
			$userData['remain_votes_for_give'] = $this->getRemailVotesForGive($query->row()->user_id);
			if($query->row()->user_type == 2) {
				$userData['received_votes'] = $query->row()->received_votes;
				$userData['identity_video'] = $query->row()->identity_video != null ? video_url('identity-video/'.$query->row()->identity_video) : null;
				$userData['city'] = $query->row()->city;
				$userData['address'] = $query->row()->address;
				$userData['images'] = $this->fetchContestantImages($query->row()->user_id);
				$userData['received_comments'] = $this->fetchReceivedComments($query->row()->user_id);
			}
			return $userData;
		} else {
			return false;
		}
	}

	/**
	 * @param $request_params
	 * @return bool
	 */
	function update_profile($request_params)
	{
		$competition_id = isset($request_params['competition_id'])?$request_params['competition_id']:$this->CurrCompetition;
		if (!isset($request_params['user_id'])  || !$this->is_val_available('users','user_id',$request_params['user_id'])) {
			return false;
		}

		$provider_id = $this->get_val_by_id('users','provider_id','user_id',$request_params['user_id']);

		if($provider_id == null || $provider_id == ''){
			if (isset($request_params['username'])) {
				if (!$this->is_username_available($request_params['username'],$competition_id,'user_id', $request_params['user_id'])) {
					return 'exist_username';
				}
			}

			if (isset($request_params['email'])) {
				if (!$this->is_email_available($request_params['email'],$competition_id,'user_id', $request_params['user_id'])) {
					return 'exist_email';
				}
			}
		}

		if (!empty($_FILES['profile_pic']['name'])) {
			$profile_pic = $this->upload_image('profile_pic',image_dir('profile-pics/'));
			if ($profile_pic != false) {
				$unlink_image = $this->get_val_by_id('users', 'profile_pic', 'user_id', $request_params['user_id']);
				if ($unlink_image != null && $unlink_image != '') {
					$this->unlink_file(image_dir('profile-pics/'.$unlink_image));
					$this->unlink_file(image_dir('profile-pics/thumbs/'.$unlink_image));
				}
				$request_params['profile_pic'] = $profile_pic;
				$request_params['profile_pic_type'] = 1;
			} else {
				return 'profile_pic_error';
			}
		}else{
			if(isset($request_params['profile_pic']) && $request_params['profile_pic'] != null && $request_params['profile_pic'] != ''){
				$profile_pic = $this->upload_image($request_params['profile_pic'],image_dir('profile-pics/'),2);
				if($profile_pic){
					$request_params['profile_pic'] = $profile_pic;
					$request_params['profile_pic_type'] = 1;
					$unlink_image = $this->get_val_by_id('users', 'profile_pic', 'user_id', $request_params['user_id']);
					if ($unlink_image != null && $unlink_image != '') {
						$this->unlink_file(image_dir('profile-pics/'.$unlink_image));
						$this->unlink_file(image_dir('profile-pics/thumbs/'.$unlink_image));
					}
				}
			}
		}

		if (!empty($_FILES['identity_video']['name'])) {
			$identity_video = $this->upload_video('identity_video',video_dir('identity-video/'));
			if ($identity_video != false) {
				$unlink_video = $this->get_val_by_id('users', 'identity_video', 'user_id', $request_params['user_id']);
				if ($unlink_video != null && $unlink_video != '') {
					$this->unlink_file(video_dir('identity-video/'.$unlink_video));
				}
				$request_params['identity_video'] = $identity_video;
			} else {
				return 'identity_video_error';
			}
		}

		if(isset($request_params['profile_pic_type']) && $request_params['profile_pic_type'] == 2){
			$request_params['profile_pic'] = isset($request_params['profile_pic'])?$request_params['profile_pic']:null;
			$unlink_image = $this->get_val_by_id('users', 'profile_pic', 'user_id', $request_params['user_id']);
			if ($unlink_image != null && $unlink_image != '') {
				$this->unlink_file(image_dir('profile-pics/'.$unlink_image));
				$this->unlink_file(image_dir('profile-pics/thumbs/'.$unlink_image));
			}
		}

		if(isset($request_params['password'])){
			$original_password = $request_params['password'];
			$request_params['password'] = md5($request_params['password']);
		}

		$this->db->where('user_id', $request_params['user_id']);
		if ($this->db->update('users', $request_params)) {
			$this->db->select('user_id,username,email,password,profile_pic,profile_pic_type,identity_video,city,address,user_type,received_votes');
			$this->db->from('users');
			$this->db->where('user_id',$request_params['user_id']);
			$this->db->limit('1');
			$query = $this->db->get();
			if ($query->num_rows() == 1) {
				$userData['user_id'] = $query->row()->user_id;
				$userData['username'] = $query->row()->username;
				$userData['email'] = $query->row()->email;
				$userData['password'] = isset($original_password) ? $original_password : $query->row()->password;
				if ($query->row()->profile_pic_type == 1) {
					$userData['profile_pic'] = $query->row()->profile_pic != null ? profile_pics_url('thumbs/' . $query->row()->profile_pic) : null;
				} else {
					$userData['profile_pic'] = $query->row()->profile_pic;
				}
				$userData['user_type'] = $query->row()->user_type;
				if ($query->row()->user_type == 2) {
					$userData['received_votes'] = $query->row()->received_votes;
					$userData['identity_video'] = $query->row()->identity_video != null ? video_url('identity-video/'.$query->row()->identity_video) : null;
					$userData['city'] = $query->row()->city;
					$userData['address'] = $query->row()->address;
					$userData['images'] = $this->fetchContestantImages($query->row()->user_id);
					$userData['comments'] = $this->fetchContestantComments($query->row()->user_id);
				}
				return $userData;
				return $this->fetch_profile(array('user_id' => $request_params['user_id']));
			}
		} else {
			return false;
		}
	}

	/**
	 * @return bool
	 */
	function contestants_list($request_params)
	{
		if(!isset($request_params['dummy_str']) || $request_params['dummy_str'] != 'dummy'){
			return 'dummy_str_not_found';
		}

		$this->db->select('user_id,username,profile_pic,profile_pic_type,received_votes');
		$this->db->from('users');
		$this->db->where('user_type', 2);
		$this->db->order_by('received_votes', 'DESC');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
				$i = 0;
			foreach ($query->result() as $row) {
				$response_data[$i]['user_id'] = $row->user_id;
				$response_data[$i]['username'] = $row->username;
				if($row->profile_pic_type == 1){
					$response_data[$i]['profile_pic'] = $row->profile_pic != null ? profile_pics_url('thumbs/'.$row->profile_pic) : null;
				}else{
					$response_data[$i]['profile_pic'] = $row->profile_pic;
				}
				$response_data[$i]['received_votes'] = $row->received_votes;
				$i++;
			}
			return $response_data;
		} else {
			return false;
		}
	}

	/**
	 * @param $request_params
	 * @return bool
	 */
	function search_contestants($request_params)
	{
		if (isset($request_params['search_text'])) {
			$search_text = $request_params['search_text'];
			$this->db->like('username', $search_text);
			$this->db->select('user_id,username,profile_pic,profile_pic_type,received_votes');
			$this->db->from('users');
			$this->db->like('user_type',2);
			$query = $this->db->get();
			if ($query->num_rows() > 0) {
				$i = 0;
				foreach ($query->result() as $row) {
					$response_data[$i]['user_id'] = $row->user_id;
					$response_data[$i]['username'] = $row->username;
					if($row->profile_pic_type == 1){
						$response_data[$i]['profile_pic'] = $row->profile_pic != null ? profile_pics_url('thumbs/'.$row->profile_pic) : null;
					}else{
						$response_data[$i]['profile_pic'] = $row->profile_pic;
					}
					$response_data[$i]['received_votes'] = $row->received_votes;
					$i++;
				}
				return $response_data;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 * @param $request_params
	 * @return bool|string
	 */
	function upload_competition_image($request_params){
		if(!isset($request_params['user_id']) || !isset($request_params['image_type'])
			|| !$this->is_val_available('users','user_id',$request_params['user_id'])){
			return false;
		}
		if($this->check_image_quota($request_params['user_id'])){
			if($request_params['image_type'] == 'upload'){
				if (!empty($_FILES['competition_image']['name'])) {
					$competition_image = $this->upload_image('competition_image',image_dir('competition-images/'),1,320,320);
					if ($competition_image == false) {
						return 'invalid_image';
					}
				}else{
					return false;
				}
			}else{
				if(isset($request_params['competition_image']) && $request_params['competition_image'] != null && $request_params['competition_image'] != ''){
					$competition_image = $this->upload_image($request_params['competition_image'],image_dir('competition-images/'),2,320,320);
					if($competition_image == false){
						return 'invalid_image';
					}
				}else{
					return false;
					$competition_image = '';
				}
			}
			if($this->addCompetitionImage($request_params['user_id'],$competition_image,'upload')){
				return true;
			}else{
				return 'wrong';
			}
		}else{
			return 'no_image_quota';
		}
	}

	/**
	 * @param $request_params
	 * @return bool|string
	 */
	function update_competition_image($request_params){
		if(!isset($request_params['image_id']) || !isset($request_params['image_type'])
			|| !$this->is_val_available('competition_images','image_id',$request_params['image_id'])){
			return false;
		}
		if($request_params['image_type'] == 'upload'){
			if (!empty($_FILES['competition_image']['name'])) {
				$competition_image = $this->upload_image('competition_image',image_dir('competition-images/'),1,320,320);
				if ($competition_image == false) {
					return 'invalid_image';
				}
			}else{
				return false;
			}
		}else{
			if(isset($request_params['competition_image']) && $request_params['competition_image'] != null && $request_params['competition_image'] != ''){
				$competition_image = $this->upload_image($request_params['competition_image'],image_dir('competition-images/'),2,320,320);
				if($competition_image == false){
					return 'invalid_image';
				}
			}else{
				return false;
				$competition_image = '';
			}
		}

		$unlink_image = $this->get_val_by_id('competition_images','image','image_id',$request_params['image_id']);
		if ($unlink_image != null && $unlink_image != '') {
			$this->unlink_file(image_dir('competition-images/'.$unlink_image));
			$this->unlink_file(image_dir('competition-images/thumbs/'.$unlink_image));
		}
		if($this->updateCompetitionImage($request_params['image_id'],$competition_image,'upload')){
			return true;
		}else{
			return 'wrong';
		}
	}


	/**
	 * @param $request_params
	 * @return bool|string
	 */
	function delete_competition_image($request_params)
	{
		if (!isset($request_params['image_id']) || !$this->is_val_available('competition_images','image_id',$request_params['image_id'])) {
			return false;
		}
		if($this->deleteCompetitionImage($request_params['image_id'])){
			return true;
		}else{
			return 'wrong';
		}

	}

	/**
	 * @param $request_params
	 * @return bool|string
	 */
	function add_comment($request_params){
		if(!isset($request_params['on_user_id']) || !isset($request_params['by_user_id']) || !isset($request_params['comment_text'])
				|| !$this->is_val_available('users','user_id',$request_params['on_user_id'])
				|| !$this->is_val_available('users','user_id',$request_params['by_user_id'])){
			return false;
		}

		$comment_data = array(
							'on_user_id'=>$request_params['on_user_id'],
							'by_user_id'=>$request_params['by_user_id'],
							'comment_text'=> trim($request_params['comment_text']),
							'created_at' => date('Y-m-d H:i:s')
							);
		$this->db->insert('comments',$comment_data);
		if($this->db->insert_id() > 0){
			$response_data = array( 'comment_id'=>$this->get_val_by_id('comments','comment_id','comment_id',$this->db->insert_id()),
									'comment'=>trim($comment_data['comment_text']),
									'commented_by'=>$this->get_val_by_id('users','username','user_id',$comment_data['by_user_id']),
									'user_id'=>$comment_data['by_user_id'],
									'commented_at'=>date("M j 'y  G:i",strtotime(str_replace('-','/',$comment_data['created_at']))));
			return array('comments'=>$response_data);
		}else{
			return 'wrong';
		}
	}

	/**
	 * @param $request_params
	 * @return bool|string
	 */
	function update_comment($request_params){
		if(!isset($request_params['comment_id']) || !isset($request_params['comment_text'])
			|| !$this->is_val_available('comments','comment_id',$request_params['comment_id'])){
			return false;
		}
		$this->db->where('comment_id',$request_params['comment_id']);
		$this->db->update('comments',array('comment_text'=>$request_params['comment_text']));
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return 'wrong';
		}
	}

	/**
	 * @param $request_params
	 * @return bool|string
	 */
	function delete_comment($request_params)
	{
		if (!isset($request_params['comment_id']) || !$this->is_val_available('comments', 'comment_id', $request_params['comment_id'])) {
			return false;
		}
		$this->db->where('comment_id', $request_params['comment_id']);
		$this->db->limit('1');
		$this->db->delete('comments');
		if ($this->db->affected_rows() > 0) {
			return true;
		} else {
			return 'wrong';
		}
	}

	/**
	 * @param $request_params
	 * @return bool|string
	 */
	function give_vote($request_params)
	{
		if (!isset($request_params['contestant_id']) || !isset($request_params['voter_id'])
					|| !$this->is_val_available('users', 'user_id', $request_params['contestant_id'])
					|| !$this->is_val_available('users', 'user_id', $request_params['voter_id'])) {
			return false;
		}

		if($this->check_votes_quota($request_params['voter_id'])){
			$vote_data = array(
							'contestant_id' => $request_params['contestant_id'],
							'voter_id' => $request_params['voter_id'],
							'created_at' => date('Y-m-d H:i:s')
							);
			$this->db->insert('votes',$vote_data);
			if($this->db->insert_id() > 0){
				$this->db->where('user_id',$request_params['voter_id']);
				$this->db->set('given_votes', 'given_votes + 1', FALSE);
				$updateStatus1 = $this->db->update('users');

				$this->db->where('user_id',$request_params['contestant_id']);
				$this->db->set('received_votes', 'received_votes + 1', FALSE);
				$updateStatus2 = $this->db->update('users');

				if($updateStatus1 && $updateStatus2){
					return array('received_votes' => $this->get_val_by_id('users','received_votes','user_id',$request_params['contestant_id']));;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}else{
			return 'votes_quota_empty';
		}
	}



	/**
	 * @param $request_params
	 * @return bool|null
	 */
	function fetch_voter_history($request_params)
	{
		if (!isset($request_params['user_id']) || !$this->is_val_available('users', 'user_id', $request_params['user_id'])) {
			return false;
		}

		$this->db->select('COUNT(v.vote_id) as votes,u.username');
		$this->db->from('votes v');
		$this->db->join('users u', 'u.user_id = v.contestant_id', 'INNER');
		$this->db->where('v.voter_id', $request_params['user_id']);
		$this->db->order_by('votes','DESC');
		$this->db->group_by('v.contestant_id');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$i = 0;
			foreach ($query->result() as $VoteRow) {
				$voting_history[$i]['contestant'] = $VoteRow->username;
				$voting_history[$i]['votes'] = $VoteRow->votes;
				$i++;
			}
			$response['voters'] = $voting_history;
		}else{
			$response['voters'] =  null;
		}

		if($this->get_val_by_id('users','user_type','user_id',$request_params['user_id']) == 2){
			$this->db->select('COUNT(v.vote_id) as votes,v.voter_id,u.username');
			$this->db->from('votes v');
			$this->db->join('users u', 'u.user_id = v.voter_id', 'INNER');
			$this->db->where('v.contestant_id', $request_params['user_id']);
			$this->db->order_by('votes','DESC');
			$this->db->group_by('v.voter_id');
			$query = $this->db->get();
			if ($query->num_rows() > 0) {
				$i = 0;
				foreach ($query->result() as $ContestantRow) {
					$contestant_history[$i]['voter'] = $ContestantRow->username;
					$contestant_history[$i]['votes'] = $ContestantRow->votes;
					$i++;
				}
				$response['contestants'] =  $contestant_history;
			}else{
				$response['contestants'] =  null;
			}
		}
		return $response;
	}


	/**
	 * @param $request_params
	 * @return bool|null
	 */
	function fetch_contestant_history($request_params){
		if (!isset($request_params['contestant_id']) || !$this->is_val_available('users', 'user_id', $request_params['contestant_id'])) {
			return false;
		}

		$this->db->select('v.*,u.username');
		$this->db->from('votes v');
		$this->db->join('users u', 'u.user_id = v.contestant_id', 'INNER');
		$this->db->where('v.contestant_id', $request_params['contestant_id']);
		$this->db->order_by('v.vote_id', 'DESC');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$i = 0;
			foreach ($query->result() as $row) {
				$voting_history[$i]['vote_id'] = $row->vote_id;
				$voting_history[$i]['voter'] = $row->username;
				$voting_history[$i]['voted_at'] = date("M j Y  G:i", strtotime(str_replace('-', '/', $row->created_at)));
				$i++;
			}
			return $voting_history;
		} else {
			return null;
		}
	}

	/**
	 * @param $request_params
	 * @return bool
	 */
	function fetch_voting_history($request_params){

		if (!isset($request_params['user_id']) || !$this->is_val_available('users', 'user_id', $request_params['user_id'])) {
			return false;
		}

		$this->db->select('COUNT(v.vote_id) as votes,u.username');
		$this->db->from('votes v');
		$this->db->join('users u', 'u.user_id = v.contestant_id', 'INNER');
		$this->db->where('v.voter_id', $request_params['user_id']);
		$this->db->order_by('votes','DESC');
		$this->db->group_by('v.contestant_id');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$i = 0;
			foreach ($query->result() as $VoteRow) {
				$voting_history[$i]['contestant'] = $VoteRow->username;
				$voting_history[$i]['votes'] = $VoteRow->votes;
				$i++;
			}
			$response['given_votes'] = $voting_history;
		}else{
			$response['given_votes'] =  null;
		}

		if($this->get_val_by_id('users','user_type','user_id',$request_params['user_id']) == 2){
			$this->db->select('COUNT(v.vote_id) as votes,v.voter_id,u.username');
			$this->db->from('votes v');
			$this->db->join('users u', 'u.user_id = v.voter_id', 'INNER');
			$this->db->where('v.contestant_id', $request_params['user_id']);
			$this->db->order_by('votes','DESC');
			$this->db->group_by('v.voter_id');
			$query = $this->db->get();
			if ($query->num_rows() > 0) {
				$i = 0;
				foreach ($query->result() as $ContestantRow) {
					$contestant_history[$i]['voter'] = $ContestantRow->username;
					$contestant_history[$i]['votes'] = $ContestantRow->votes;
					$i++;
				}
				$response['received_votes'] =  $contestant_history;
			}else{
				$response['received_votes'] =  null;
			}
		}
		return $response;
	}
	
	function fetch_competition_images($request_params){
		if (!isset($request_params['user_id']) || !$this->is_val_available('users', 'user_id', $request_params['user_id'])) {
			return false;
		}
		return array('images'=>$this->fetchContestantImages($request_params['user_id']));
	}

	function purchase_votes($request_params){
		if (!isset($request_params['user_id']) || !isset($request_params['votes']) || is_nan($request_params['votes'])
			|| !$this->is_val_available('users', 'user_id', $request_params['user_id'])) {
			return false;
		}
		$this->db->where('user_id',$request_params['user_id']);
		$this->db->set('given_votes_quota',"given_votes_quota + ".$request_params['votes'], FALSE);
		$this->db->update('users');
		if($this->db->affected_rows() > 0){
			return array('remain_votes_for_give'=>$this->getRemailVotesForGive($request_params['user_id']));
		}else{
			return false;
		}
	}
	/*-------- OTHER REQUIRE METHODS --------------*/


	function getProfileData($user_id){
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('user_id',$user_id);
		$this->db->limit('1');
		$query = $this->db->get();
		$userData['user_id'] = $query->row()->user_id;
		$userData['username'] = $query->row()->username;
		$userData['email'] = $query->row()->email;
		if($query->row()->profile_pic_type == 1){
			$userData['profile_pic'] = $query->row()->profile_pic != null ? profile_pics_url('thumbs/'.$query->row()->profile_pic) : null;
		}else{
			$userData['profile_pic'] = $query->row()->profile_pic;
		}
		$userData['user_type'] = $query->row()->user_type;
		$userData['given_comments'] = $this->fetchGivenComments($query->row()->user_id);
		$userData['remain_votes_for_give'] = $this->getRemailVotesForGive($query->row()->user_id);
		if($query->row()->user_type == 2){
			$userData['received_votes'] = $query->row()->received_votes;
			$userData['identity_video'] = $query->row()->identity_video != null ? video_url('identity-video/'.$query->row()->identity_video) : null;
			$userData['city'] = $query->row()->city;
			$userData['address'] = $query->row()->address;
			$userData['images'] = $this->fetchContestantImages($query->row()->user_id);
			$userData['received_comments'] = $this->fetchReceivedComments($query->row()->user_id);
		}
		return $userData;
	}
	/**
	 * @param $user_id
	 * @return int
	 */
	function getRemailVotesForGive($user_id){
		$this->db->select('given_votes,given_votes_quota');
		$this->db->from('users');
		$this->db->where('user_id',$user_id);
		$this->db->limit('1');
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->row()->given_votes_quota - $query->row()->given_votes;
		}else{
			return 0;
		}
	}
	/**
	 * @param $tbl_name
	 * @param $val_column
	 * @param $val
	 * @return bool
	 */
	function is_val_available($tbl_name,$val_column,$val){
		$this->db->select('1', FALSE);
		$this->db->where($val_column,$val);
		$query = $this->db->get($tbl_name);
		if($query->num_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	/**
	 * @param $username
	 * @param $competition_id
	 * @param null $id_column
	 * @param int $id_value
	 * @return bool
	 */
	function is_username_available($username,$competition_id,$id_column = null,$id_value = 0)
	{
		$this->db->select('1', FALSE);
		$this->db->where('LOWER(username)=', strtolower($username));
		$this->db->where('competition_id',$competition_id);
		if ($id_value > 0) {
			$this->db->where($id_column . '!=', $id_value);
		}
		$query = $this->db->get('users');
		return $query->num_rows() == 0;
	}

	/**
	 * @param $email
	 * @param $competition_id
	 * @param null $id_column
	 * @param int $id_value
	 * @return bool
	 */
	function is_email_available($email,$competition_id,$id_column = null,$id_value = 0)
	{
		$this->db->select('1', FALSE);
		$this->db->where('LOWER(email)=', strtolower($email));
		$this->db->where('competition_id',$competition_id);
		if ($id_value > 0) {
			$this->db->where($id_column . '!=', $id_value);
		}
		$query = $this->db->get('users');
		return $query->num_rows() == 0;
	}

	/**
	 * @param $login
	 * @param $competition_id
	 * @return null
	 */
	function get_user_by_login($login,$competition_id)
	{
		$this->db->where('competition_id',$competition_id);
		$this->db->where("LOWER(username) = '".strtolower($login)."' OR LOWER(email) = '".strtolower($login)."'");

		$query = $this->db->get('users');
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}

	/**
	 * Set new password key for user.
	 * This key can be used for authentication when resetting user's password.
	 *
	 * @param	int
	 * @param	string
	 * @return	bool
	 */
	function set_password_key($user_id, $new_pass_key)
	{
		$this->db->set('new_password_key', $new_pass_key);
		$this->db->set('new_password_requested', date('Y-m-d H:i:s'));
		$this->db->where('user_id', $user_id);

		$this->db->update('users');
		return $this->db->affected_rows() > 0;
	}

	/**
	 * @param $request_params
	 * @return bool|string
	 */
	function check_email($request_params){
		return $this->send_mail('cshailesh157@gmail.com','Testing Mail','This mail testing!',$attach = null);
	}

		/**
	 * @param $to
	 * @param $subject
	 * @param $message
	 * @param null $attach
	 * @return bool
	 */
	function send_mail($to,$subject,$message,$attach = null)
	{
		$this->load->library('email');

		$config = Array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.googlemail.com',
			'smtp_port' => 465,
			'smtp_user' => 'cshailesh157@gmail.com', // change it to yours
			'smtp_pass' => '15071995', // change it to yours
			'mailtype' => 'html',
			'charset' => 'iso-8859-1',
			'wordwrap' => TRUE
		);

		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		$this->email->from('Photo-Competition','shailesh.chauhan@igts.co.in');
		$this->email->to($to);
		$this->email->subject($subject);
		$this->email->message($message);
		if ($attach != null) {
			$this->email->attach($attach);
		}
		if ($this->email->send()) {
			return true;
		} else {
			return false;
		}
	}


	/**
	 * @param $image
	 * @param $upload_url
	 * @param int $image_source_type [1 = Upload image manually || 2 = Upload image from url]
	 * @param int $thumb_width
	 * @param int $thumb_height
	 * @return bool
	 */
	function upload_image($image,$upload_url,$image_source_type = 1,$thumb_width = 150,$thumb_height = 150)
	{
		if($image_source_type == 1){
			$config['upload_path'] = $upload_url;
			$config['allowed_types'] = 'gif|jpg|jpeg|png';
			$config['encrypt_name'] = TRUE;
			$this->load->library ('upload',$config);
			$this->upload->initialize($config);
			if (!$this->upload->do_upload($image)){
				/*print_r($this->upload->display_errors());die;*/
				return false;
			}else{
				$data = $this->upload->data();
				$file_name = $data ['file_name'];
			}
		}else{
			$file_info = pathinfo($image);
			$file_name = md5(date('Y-m-d H:i:s').rand(00000,99999)).".".$file_info['extension'];
			$to = $upload_url.$file_name;
			if(!copy($image,$to)){
				return false;
			}
		}

		$config1['image_library'] = 'gd2';
		$config1['source_image']	= $upload_url.$file_name;
		$config1['new_image'] = $upload_url.'thumbs/';
		$config1['create_thumb'] = FALSE;
		$config1['maintain_ratio'] = FALSE;
		$config1['width']	= $thumb_width;
		$config1['height']	= $thumb_height;
		$this->load->library('image_lib',$config1);
		$this->image_lib->resize();
		return $file_name;

	}

	function upload_video($video,$upload_url)
	{
		$config['upload_path'] = $upload_url;
		$config['allowed_types'] = 'mp4|3gp|flv|mp3';
		$config['encrypt_name'] = TRUE;
		$this->load->library ('upload',$config);
		$this->upload->initialize ($config);
		if (!$this->upload->do_upload($video)){
			return false;
		} else {
			$data = $this->upload->data();
			$file_name = $data ['file_name'];
			return $file_name;
		}
	}

	/**
	 * @param $tbl_name
	 * @param $column_name
	 * @param $id_column
	 * @param $id_value
	 * @return mixed
	 */
	function get_val_by_id($tbl_name,$column_name,$id_column,$id_value)
	{
		$this->db->select($column_name);
		$this->db->where($id_column, $id_value);
		$this->db->limit('1');
		$query = $this->db->get($tbl_name);
		if ($query->num_rows() > 0) {
			return $query->row()->$column_name;
		} else {
			return null;
		}

	}

	/**
	 * @param $tbl_name
	 * @param $id_column
	 * @param $id_value
	 * @return null
	 */
	function get_row_by_id($tbl_name,$id_column,$id_value){
		$this->db->select('*');
		$this->db->where($id_column,$id_value);
		$this->db->limit('1');
		$query = $this->db->get($tbl_name);
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return null;
		}

	}

	/**
	 * @param $image_url
	 * @return bool
	 */
	function unlink_file($image_url){
		if(file_exists($image_url)){
			if(unlink($image_url)){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	/**
	 * @param $user_id
	 * @return bool
	 */
	function check_image_quota($user_id)
	{
		$no_of_image = $this->get_val_by_id('users','no_of_image','user_id',$user_id);
		if($no_of_image !== null){
			if($no_of_image < 10){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	/**
	 * @param $user_id
	 * @param $image
	 * @param $image_type 1-Upload , 2-URL
	 * @return bool
	 */
	function addCompetitionImage($user_id,$image,$image_type)
	{
		$this->db->where('user_id', $user_id);
		$this->db->set('no_of_image', 'no_of_image + 1', FALSE);
		$updateStatus = $this->db->update('users');
		$comp_image_data = array(
			'user_id' => $user_id,
			'image' => $image,
			'image_type' => $image_type == "upload"?1:2,
			'created_at' => date('Y-m-d H:i:s')
		);
		$this->db->insert('competition_images', $comp_image_data);
		if ($updateStatus && $this->db->insert_id() > 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @param $image_id
	 * @param $image
	 * @param $image_type
	 * @return bool
	 */
	function updateCompetitionImage($image_id,$image,$image_type)
	{
		$comp_image_data = array(
			'image' => $image,
			'image_type' => $image_type == "upload" ? 1 : 2
		);
		$this->db->where('image_id', $image_id);
		$updateStatus = $this->db->update('competition_images', $comp_image_data);
		if ($updateStatus) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @param $image_id
	 * @return bool
	 */
	function deleteCompetitionImage($image_id){
		$imageRow = $this->get_row_by_id('competition_images', 'image_id',$image_id);
		if ($imageRow != null) {
			$this->db->where('user_id', $imageRow->user_id);
			$this->db->set('no_of_image', 'no_of_image - 1', FALSE);
			$updateStatus = $this->db->update('users');

			$unlink_image = $imageRow->image;
			if ($unlink_image != null && $unlink_image != '') {
				$this->unlink_file(image_dir('competition-images/'.$unlink_image));
				$this->unlink_file(image_dir('competition-images/thumbs/'.$unlink_image));
			}

			$this->db->limit('1');
			$this->db->delete('competition_images',array('image_id'=>$image_id));
			if ($updateStatus && $this->db->affected_rows() > 0) {
				return true;
			} else {
				return false;
			}
		}else{
			return false;
		}
	}

	/**
	 * @return null
	 */
	function getCurrCompetition()
	{
		$this->db->select('competition_id');
		$this->db->from('competitions');
		$this->db->where('start_datetime <= ',date('Y-m-d H:i:s'));
		$this->db->where('end_datetime >= ',date('Y-m-d H:i:s'));
		$this->db->order_by('competition_id','DESC');
		$this->db->limit('1');
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			return $competition = $query->row()->competition_id;
		} else {
			return null;
		}
	}

	/**
	 * @param $user_id
	 * @return null
	 */
	function fetchGivenComments($user_id)
	{
		$this->db->select('c.*,u.username');
		$this->db->from('comments c');
		$this->db->join('users u', 'u.user_id = c.on_user_id', 'INNER');
		$this->db->where('c.by_user_id', $user_id);
		$this->db->order_by('created_at');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$i = 0;
			foreach ($query->result() as $row) {
				$ResponseRow[$i]['comment_id'] = $row->comment_id;
				$ResponseRow[$i]['comment'] = $row->comment_text;
				$ResponseRow[$i]['commented_on'] = $row->username;
				$ResponseRow[$i]['user_id'] = $row->by_user_id;
				$ResponseRow[$i]['commented_at'] = date("M j 'y  G:i",strtotime(str_replace('-','/',$row->created_at)));
				$i++;
			}
			return $ResponseRow;
		} else {
			return null;
		}
	}

	/**
	 * @param $user_id
	 * @return null
	 */
	function fetchReceivedComments($user_id)
	{
		$this->db->select('c.*,u.username');
		$this->db->from('comments c');
		$this->db->join('users u', 'u.user_id = c.by_user_id', 'INNER');
		$this->db->where('c.on_user_id', $user_id);
		$this->db->order_by('created_at');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$i = 0;
			foreach ($query->result() as $row) {
				$ResponseRow[$i]['comment_id'] = $row->comment_id;
				$ResponseRow[$i]['comment'] = $row->comment_text;
				$ResponseRow[$i]['commented_by'] = $row->username;
				$ResponseRow[$i]['user_id'] = $row->by_user_id;
				$ResponseRow[$i]['commented_at'] = date("M j 'y  G:i",strtotime(str_replace('-','/',$row->created_at)));
				$i++;
			}
			return $ResponseRow;
		} else {
			return null;
		}
	}

	/**
	 * @param $user_id
	 * @return null
	 */
	function fetchContestantComments($user_id)
	{
		$this->db->select('c.*,u.username');
		$this->db->from('comments c');
		$this->db->join('users u', 'u.user_id = c.by_user_id', 'INNER');
		$this->db->where('c.on_user_id', $user_id);
		$this->db->order_by('created_at');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$i = 0;
			foreach ($query->result() as $row) {
				$ResponseRow[$i]['comment_id'] = $row->comment_id;
				$ResponseRow[$i]['comment'] = $row->comment_text;
				$ResponseRow[$i]['commented_by'] = $row->username;
				$ResponseRow[$i]['user_id'] = $row->by_user_id;
				$ResponseRow[$i]['commented_at'] = date("M j 'y  G:i",strtotime(str_replace('-','/',$row->created_at)));
				$i++;
			}
			return $ResponseRow;
		} else {
			return null;
		}
	}

	/**
	 * @param $user_id
	 * @return null
	 */
	function fetchContestantImages($user_id)
	{
		$this->db->select('*');
		$this->db->from('competition_images');
		$this->db->where('user_id',$user_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$i = 0;
			foreach ($query->result() as $row) {
				$ResponseRow[$i]['image_id'] = $row->image_id;
				$ResponseRow[$i]['thumb_image'] = $row->image_type == 2 ? $row->image : competition_image_url('thumbs/' . $row->image);
				$ResponseRow[$i]['image'] = $row->image_type == 2 ? $row->image : competition_image_url($row->image);
				$ResponseRow[$i]['image_type'] = $row->image_type == 2 ? 'url' : 'upload';
				$i++;
			}
			return $ResponseRow;
		} else {
			return null;
		}
	}

	/**
	 * @param $voter_id
	 * @return bool
	 */
	function check_votes_quota($voter_id){
		$this->db->select('given_votes,given_votes_quota');
		$this->db->from('users');
		$this->db->where('user_id',$voter_id);
		$this->db->limit('1');
		$query = $this->db->get();
		if($query->num_rows() > 0){
			if($query->row()->given_votes_quota > $query->row()->given_votes){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	/*-----------END CLASS-------------*/
}