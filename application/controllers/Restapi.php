<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('upload_max_filesize','30M');
require(APPPATH.'libraries/REST_Controller.php');
class Restapi extends REST_Controller
{

	protected $secretKey = 'ZSqGguknzXP4zXxudeVXg2fzHJIYFaPbGgy6Eo4U3PLsheygXUJ54OWmO2Tk5Gw0WgF6whiMwrrV';
	protected $header = '';

	function __construct()
	{
		parent::__construct();
		ini_set('upload_max_filesize','30M');
		$this->load->model("restapi_model", "api");
		$method = $this->router->fetch_method();
		if($method != 'image_upload_demo_ws' && $method != 'check_email' && $method != 'testing'){
			$this->check_api_key();
		}

	}
	/**
	 *  Index
	 */
	public function index_post()
	{
		$response["status"] = "401";
		$response["message"] = "Access denied!";
		echo json_encode($response);
	}

	/*
	 *	Check API KEY
	 */
	function check_api_key()
	{
		$this->header = $this->input->request_headers();
		if (!isset($this->header['Api-Key']) || $this->header['Api-Key'] != $this->secretKey) {
			exit(json_encode(array('success' => false, 'message' => 'Unauthorised access!')));
		}
	}

	/**
	 * @return string
	 */
	function get_request_data()
	{
		if (!empty($this->post())) {
			return $this->post();
		} elseif (!empty($this->get())) {
			return $this->get();
		} else {
			exit(json_encode(array('success' => false, 'message' => 'No request params found!')));
		}
	}

	/**
	 *    Log in
	 */
	public function login_post()
	{
		$request_params = $this->get_request_data();
		$response_data = $this->api->login($request_params);
		if ($response_data == false) {
			$response = array('success' => false, 'message' => 'Please try again, Incorrect username or password!');
		} else {
			$response = array('success' => true, 'message' => 'Successfully Login.', 'data' => $response_data);
		}
		echo json_encode($response);
	}

	/**
	 * 	Social Login
	 */
	public function social_login_post(){
		$request_params = $this->get_request_data();
		$response_data = $this->api->social_login($request_params);
		if ($response_data == false) {
			$response = array('success' => false, 'message' => 'Please try again, Something wrong!');
		} else {
			$response = array('success' => true, 'message' => 'Successfully Login.', 'data' => $response_data);
		}
		echo json_encode($response);
	}

	public function register_demo_post()
	{
		$data['post_data'] = $_POST;
		$data['files_data'] = $_FILES;
		echo json_encode($data);
	}

	/**
	 * Register User & Adviser
	 */
	public function register_post()
	{
		$request_params = $this->get_request_data();
		$response_data = $this->api->register($request_params);
		if ($response_data == false) {
			$response = array('success' => false, 'message' => 'Please try again, something wrong!');
		} elseif ($response_data === 'exist_username') {
			$response = array('success' => false, 'message' => 'Username already exist!');
		} elseif ($response_data === 'exist_email') {
			$response = array('success' => false, 'message' => 'Email already exist!');
		} elseif ($response_data === 'profile_pic_error') {
			$response = array('success' => false, 'message' => 'Improper image format selected!');
		} elseif ($response_data === 'identity_video_error') {
			$response = array('success' => false, 'message' => 'Improper video format selected!');
		} elseif ($response_data === 'unvalid_email') {
			$response = array('success' => false, 'message' => 'Please try again, your email address not found!');
		} else {
			$response = array('success' => true, 'message' => 'You have successfully registered.', 'data' => $response_data);
		}
		echo json_encode($response);
	}

	/**
	 *	Forgot Password
	 */
	public function forgot_password_post()
	{

		$request_params = $this->get_request_data();
		$response_data = $this->api->forgot_password($request_params);
		if ($response_data == false) {
			$response = array('success' => false, 'message' => 'Incorrect email address please try again!');
		} elseif ($response_data === 'not_send') {
			$response = array('success' => false, 'message' => 'Please try again, Email not sent!');
		} else {
			$response = array('success' => true, 'message' => 'An email with instructions for creating a new password has been sent to you.');
		}
		echo json_encode($response);
	}

	/**
	 *  Fetch Profile
	 */
	public function fetch_profile_post()
	{
		$request_params = $this->get_request_data();
		$response_data = $this->api->fetch_profile($request_params);
		if ($response_data == false) {
			$response = array('success' => false, 'message' => 'Please try again, User not found!');
		} else {
			$response = array('success' => true, 'message' => 'Profile data.', 'data' => $response_data);
		}
		echo json_encode($response);
	}

	/**
	 *  Update Profile
	 */
	public function update_profile_post()
	{
		$request_params = $this->get_request_data();
		$response_data = $this->api->update_profile($request_params);
		if ($response_data == false) {
			$response = array('success' => false, 'message' => 'Please try again, User not found!');
		} elseif ($response_data === 'exist_username') {
			$response = array('success' => false, 'message' => 'Username already exist!');
		} elseif ($response_data === 'exist_email') {
			$response = array('success' => false, 'message' => 'Email already exist!');
		} elseif ($response_data === 'profile_pic_error') {
			$response = array('success' => false, 'message' => 'Improper image format selected!');
		} elseif ($response_data === 'identity_video_error') {
			$response = array('success' => false, 'message' => 'Improper video format selected!');
		} else {
			$response = array('success' => true, 'message' => 'You have successfully updated profile.','data'=>$response_data);
		}
		echo json_encode($response);
	}

	/**
	 * Contestants List
	 */
	public function contestants_list_post(){
		$request_params = $this->get_request_data();
		$response_data = $this->api->contestants_list($request_params);
		if ($response_data == false) {
			$response = array('success' => false, 'message' => 'Please try again, Something wrong!');
		} elseif ($response_data === 'dummy_str_not_found') {
			$response = array('success' => false, 'message' => 'Please try again, Dummy string not match.');
		} else {
			$response = array('success' => true, 'message' => 'Contestants list.', 'data' => $response_data);
		}
		echo json_encode($response);
	}

	/**
	 * Search Contestants
	 */

	public function search_contestants_post()
	{
		$request_params = $this->get_request_data();
		$response_data = $this->api->search_contestants($request_params);
		if ($response_data == false) {
			$response = array('success' => false, 'message' => 'Please try again, Contestants not found!');
		} else {
			$response = array('success' => true, 'message' => 'Contestants list.', 'data' => $response_data);
		}
		echo json_encode($response);
	}


	/**
	 *	Add Competition Image
	 */
	public function upload_competition_image_post()
	{
		$request_params = $this->get_request_data();
		$response_data = $this->api->upload_competition_image($request_params);
		if ($response_data == false) {
			$response = array('success' => false, 'message' => 'Please try again, Contestant or image not found!');
		} elseif ($response_data === 'no_image_quota') {
			$response = array('success' => false, 'message' => 'You have already uploaded all images!');
		} elseif ($response_data === 'invalid_image') {
			$response = array('success' => false, 'message' => 'Improper image format selected!');
		} elseif ($response_data === 'wrong') {
			$response = array('success' => false, 'message' => 'Please try again, Something wrong!');
		} else {
			$response = array('success' => true, 'message' => 'You have successfully uploaded image.');
		}
		echo json_encode($response);
	}

	/**
	 *  Update Competition Image
	 */
	public function update_competition_image_post()
	{
		$request_params = $this->get_request_data();
		$response_data = $this->api->update_competition_image($request_params);
		if ($response_data == false) {
			$response = array('success' => false, 'message' => 'Please try again, Contestant or image not found!');
		} elseif ($response_data === 'invalid_image') {
			$response = array('success' => false, 'message' => 'Improper image format selected!');
		} elseif ($response_data === 'wrong') {
			$response = array('success' => false, 'message' => 'Please try again, Something wrong!');
		} else {
			$response = array('success' => true, 'message' => 'You have successfully updated image.');
		}
		echo json_encode($response);
	}

	/**
	 * 	Remove Competition Image
	 */
	public function delete_competition_image_post()
	{
		$request_params = $this->get_request_data();
		$response_data = $this->api->delete_competition_image($request_params);
		if ($response_data == false) {
			$response = array('success' => false, 'message' => 'Please try again, Contestant or Image not found!');
		} elseif ($response_data === 'wrong') {
			$response = array('success' => false, 'message' => 'Please try again, Something wrong!');
		} else {
			$response = array('success' => true, 'message' => 'You have successfully deleted image.');
		}
		echo json_encode($response);
	}

	/**
	 * 	Add Comment
	 */
	public function add_comment_post()
	{
		$request_params = $this->get_request_data();
		$response_data = $this->api->add_comment($request_params);
		if ($response_data == false) {
			$response = array('success' => false, 'message' => 'Please try again, User not found!');
		} elseif ($response_data === 'wrong') {
			$response = array('success' => false, 'message' => 'Please try again, Something wrong!');
		} else {
			$response = array('success' => true, 'message' => 'You have successfully added comment.','data'=>$response_data);
		}
		echo json_encode($response);
	}

	/**
	 * 	Update Comment
	 */
	public function update_comment_post()
	{
		$request_params = $this->get_request_data();
		$response_data = $this->api->update_comment($request_params);
		if ($response_data == false) {
			$response = array('success' => false, 'message' => 'Please try again, Comment not found!');
		} elseif ($response_data === 'wrong') {
			$response = array('success' => false, 'message' => 'Please try again, Something wrong!');
		} else {
			$response = array('success' => true, 'message' => 'You have successfully updated comment.');
		}
		echo json_encode($response);
	}

	/**
	 * 	Delete Comment
	 */
	public function delete_comment_post()
	{
		$request_params = $this->get_request_data();
		$response_data = $this->api->delete_comment($request_params);
		if ($response_data == false) {
			$response = array('success' => false, 'message' => 'Please try again, Comment not found!');
		} elseif ($response_data === 'wrong') {
			$response = array('success' => false, 'message' => 'Please try again, Something wrong!');
		} else {
			$response = array('success' => true, 'message' => 'You have successfully deleted comment.');
		}
		echo json_encode($response);
	}

	/**
	 * 	Give Vote
	 */
	public function give_vote_post()
	{
		$request_params = $this->get_request_data();
		$response_data = $this->api->give_vote($request_params);
		if ($response_data == false) {
			$response = array('success' => false, 'message' => 'Please try again, Contestant or Voter not found!');
		} elseif ($response_data === 'votes_quota_empty') {
			$response = array('success' => false, 'message' => 'You have already given all your votes!');
		} elseif ($response_data === 'wrong') {
			$response = array('success' => false, 'message' => 'Please try again, Something wrong!');
		} else {
			$response = array('success' => true, 'message' => 'You have successfully given a vote.','data'=>$response_data);
		}
		echo json_encode($response);
	}

	/**
	 * 	Fetch voter History
	 */
	public function fetch_voter_history_post()
	{
		$request_params = $this->get_request_data();
		$response_data = $this->api->fetch_voter_history($request_params);
		if ($response_data == false) {
			$response = array('success' => false, 'message' => 'Please try again, Voter not found!');
		} else {
			$response = array('success' => true, 'message' => 'Voter History.', 'data' => $response_data);
		}
		echo json_encode($response);
	}

	/**
	 * 	Fetch Contestant History
	 */
	public function fetch_contestant_history_post()
	{
		$request_params = $this->get_request_data();
		$response_data = $this->api->fetch_contestant_history($request_params);
		if ($response_data == false) {
			$response = array('success' => false, 'message' => 'Please try again, Contestant not found!');
		} else {
			$response = array('success' => true, 'message' => 'Contestant History.', 'data' => $response_data);
		}
		echo json_encode($response);
	}

	/**
	 * Fetch Voting History
	 */
	public function fetch_voting_history_post()
	{
		$request_params = $this->get_request_data();
		$response_data = $this->api->fetch_voting_history($request_params);
		if ($response_data == false) {
			$response = array('success' => false, 'message' => 'Please try again, Voter or Contestant not found!');
		} else {
			$response = array('success' => true, 'message' => 'Voting History.', 'data' => $response_data);
		}
		echo json_encode($response);
	}

	/**
	 * Fetch Competition Images
	 */
	public function fetch_competition_images_post()
	{
		$request_params = $this->get_request_data();
		$response_data = $this->api->fetch_competition_images($request_params);
		if ($response_data == false) {
			$response = array('success' => false, 'message' => 'Please try again, Contestant not found!');
		} else {
			$response = array('success' => true, 'message' => 'Competition images', 'data' => $response_data);
		}
		echo json_encode($response);
	}

	/**
	 * Purchase Votes
	 */
	public function purchase_votes_post()
	{
		$request_params = $this->get_request_data();
		$response_data = $this->api->purchase_votes($request_params);
		if ($response_data == false) {
			$response = array('success' => false, 'message' => 'Please try again, Something wrong!');
		} else {
			$response = array('success' => true, 'message' => 'You have successfully purchase votes.', 'data' => $response_data);
		}
		echo json_encode($response);
	}


	public function image_upload_demo_ws_post()
	{
		$data['post_data'] = $_POST;
		$data['files_data'] = $_FILES;
		echo json_encode($data);exit();
		if(isset($_FILES['profile_pic'])){
			$file_upload ="true";
			$file_name = md5(date('m/d/Y h:i:s a', time())).".".pathinfo($_FILES['profile_pic']['name'],PATHINFO_EXTENSION);
		//	$file_name = date('m/d/Y h:i:s a', time()).$_FILES['profile_pic']['name'];
			$path = image_dir($file_name);
			if($file_upload == "true"){
				if(move_uploaded_file ($_FILES['profile_pic']['tmp_name'], $path)){
					$profile_pic =  $file_name;
				}
				else{
					$profile_pic =  false;
				}
			}
			else{
				$profile_pic =  false;
			}

			if ($profile_pic == false) {
				$response = array('success' => false, 'message' => 'Image not upload');
			} else {
				$response = array('success' => true, 'message' => 'Image upload success', 'image_url' => image_url($profile_pic));
			}

		}else{
			$response = array('success' => false, 'message' => 'Image not found');
		}
		echo json_encode($response);
	}

	public function check_email_post(){
		$request_params = $this->post();
		$response_data = $this->api->check_email($request_params);
		if ($response_data == false) {
			$response = array('success' => false, 'message' => 'Please try again, something wrong.');
		} elseif ($response_data === 'unvalid_email') {
			$response = array('success' => false, 'message' => 'Unvalid email address.');
		} else {
			$response = array('success' => true, 'message' => 'Email is valid.');
		}
		echo json_encode($response);
	}

	public function check_email_get(){
		//$this->load->library('verifyemail');
		$response_data = true;//$this->verifyemail->check('cshailesh157@gmail.com');
		if($response_data){
			echo "Valid email!";
		}else{
			echo "Unvalid email!";
		}
	}

	function testing_get()
	{
		if ($this->post()) {
			$params['post_params'] = $this->post();
		}else{
			$params['post_params'] = array();
		}

		if($this->get()) {
			$params['get_params'] 	= $this->get();
		}else{
			$params['get_params'] = array();
		}

		if($_FILES) {
			$params['files_params'] = $_FILES;
		}else{
			$params['files_params'] = array();
		}
		echo json_encode($params);
		phpinfo();
	}
//--------END CLASS
}

?>