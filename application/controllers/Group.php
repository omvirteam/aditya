<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Group
 */
class Group extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('appmodel');
	}
        
	function index()
	{
		set_page('group/index');
	}
}