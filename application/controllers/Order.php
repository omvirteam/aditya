<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Sales_order
 */
class Order extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('appmodel');
	}
        
	function sales_order()
	{
		set_page('order/sales_order');
	}

	function purchase_order()
	{
		set_page('order/purchase_order');
	}
}