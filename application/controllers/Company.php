<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Company
 */
class Company extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('appmodel');
	}
	function index()
	{
		set_page('company/create');
	}

	function create()
	{
		set_page('company/create');
	}
}
