<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Ledger
 */
class Ledger extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('appmodel');
	}
        
	function index()
	{
		set_page('ledger/index');
	}
        
        function show_balance_sheet(){
         set_page('ledger/balance_sheet');
        }
        
	function show_profit_loss(){
		set_page('ledger/profit_loss');
	}
}