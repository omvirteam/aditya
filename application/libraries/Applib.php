<?php
/**
 * Created by PhpStorm.
 * User: iGTS05
 * Date: 17-06-16
 * Time: 10:10 AM
 */
class AppLib
{

    function __construct()
    {
        $this->ci =& get_instance();
        $this->ci->load->database();
    }

    /**
     * @param $tbl_name
     * @param $condition
     * @return mixed
     */
    function counts_rows($tbl_name,$condition = array(1=>1)){
        $this->ci->db->select('1',true);
        $this->ci->db->from($tbl_name);
        $this->ci->db->where($condition);
        $query = $this->ci->db->get();
        return $query->num_rows();
    }

    /**
     * @return mixed
     */
    function getCurrCompetition(){
        $this->ci->db->select('competition_id');
        $this->ci->db->from('competitions');
        $this->ci->db->where('start_datetime <= ',date('Y-m-d H:i:s'));
        $this->ci->db->where('end_datetime >= ',date('Y-m-d H:i:s'));
        $this->ci->db->order_by('competition_id','DESC');
        $this->ci->db->limit('1');
        $query = $this->ci->db->get();
        if($query->num_rows() > 0){
            return $query->row()->competition_id;
        }else{
            $this->ci->session->set_flashdata('success',false);
            $this->ci->session->set_flashdata('message','No any competition currently runnig.');
            redirect('competition/');
        }
    }


    /**
     * @param $competition_id
     * @return mixed
     */
    function getCompetitionDetail($competition_id){
        $this->ci->db->select('*');
        $this->ci->db->from('competitions');
        $this->ci->db->where('competition_id',$competition_id);
        $query = $this->ci->db->get();
        return $query->row();
    }

    /**
     * @param $competition_id
     * @return string
     */
    function getCompetitionStatus($competition_id){
        $this->ci->db->select('start_datetime,end_datetime');
        $this->ci->db->from('competitions');
        $this->ci->db->where('competition_id',$competition_id);
        $this->ci->db->limit('1');
        $query = $this->ci->db->get();
        if($query->row()->start_datetime < date('Y-m-d H:i:s') AND $query->row()->end_datetime < date('Y-m-d H:i:s')){
            return 'completed';
        }elseif($query->row()->start_datetime > date('Y-m-d H:i:s') AND $query->row()->end_datetime > date('Y-m-d H:i:s')){
            return 'pending';
        }else{
            return 'running';
        }
    }

    function getActiveCompetition(){
        $this->ci->db->select('competition_id');
        $this->ci->db->from('competitions');
        $this->ci->db->where('is_active',1);
        $this->ci->db->limit('1');
        $query = $this->ci->db->get();
        return $query->row()->competition_id;
    }
}