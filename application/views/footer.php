        <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->
    <script>
        $.widget.bridge('uibutton', $.ui.button);
    </script>

    <!-- Morris.js charts -->
    <script src="<?=bootstrap_url('js/raphael-min.js');?>"></script>

        <?php if($this->uri->segment(1) == ''){?>
            <script src="<?=plugins_url('morris/morris.min.js');?>"></script>
            <script src="<?=dist_url('js/pages/dashboard.js');?>"></script>
        <?php }?>
    <!-- Sparkline -->
    <script src="<?=plugins_url('sparkline/jquery.sparkline.min.js');?>"></script>
    <!-- jvectormap -->
    <script src="<?=plugins_url('jvectormap/jquery-jvectormap-1.2.2.min.js');?>"></script>
    <script src="<?=plugins_url('jvectormap/jquery-jvectormap-world-mill-en.js');?>"></script>
    <!-- jQuery Knob Chart -->
    <script src="<?=plugins_url('knob/jquery.knob.js');?>"></script>
    <!-- daterangepicker -->
    <script src="<?=bootstrap_url('js/moment.min.js');?>"></script>
    <script src="<?=plugins_url('daterangepicker/daterangepicker.js');?>"></script>
    <!-- datepicker -->
    <script src="<?=plugins_url('datepicker/bootstrap-datepicker.js');?>"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="<?=plugins_url('bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js');?>"></script>
    <!-- Slimscroll -->
    <script src="<?=plugins_url('slimScroll/jquery.slimscroll.min.js');?>"></script>

    <!--Page Specific Js-->


        <?php //if($this->uri->segment(2) == 'quotation' || $this->uri->segment(1) == 'party'){?>
            <script src="<?=plugins_url('datatables/jquery.dataTables.min.js');?>"></script>
            <script src="<?=plugins_url('datatables/dataTables.bootstrap.min.js');?>"></script>
        <?php //} ?>

    <script src="<?=plugins_url('iCheck/icheck.min.js');?>"></script>

    <!--Page Specific Js-->

    <!-- FastClick -->
    <script src="<?=plugins_url('fastclick/fastclick.js');?>"></script>
    <!-- AdminLTE App -->
    <script src="<?=dist_url('js/app.min.js');?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?=dist_url('js/demo.js');?>"></script>
    <script>
        $(document).ready(function(){
            $('input[type="radio"].address_work').iCheck({
                radioClass: 'iradio_minimal-green'
            });
            $('input[type="checkbox"].enquiry_received_by').iCheck({
                checkboxClass: 'icheckbox_flat-green',
            });
            $('#datepicker1,#datepicker2,#datepicker3').datepicker({
                format: 'dd/mm/yyyy',
                autoclose: true
            });
        });
    </script>
    </body>
</html>