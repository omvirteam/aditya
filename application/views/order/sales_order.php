<?php if($this->session->flashdata('success') == true){?>
    <div class="col-sm-4 pull-right">
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i> <?=$this->session->flashdata('message')?></h4>
        </div>
    </div>
<?php }?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="custom content-header"  >
        <h1>
            <small class="text-primary text-bold">Master : Sales Order</small>
        </h1>
    </section>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body no-padding">
                    <div class="col-md-12 padding-top-bottom-10-px">
                        New Sales Order
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-sm-3 input-sm">Order Book</label>
                            <div class="col-sm-9">
                                <input class="form-control input-sm"  name="order_book" value="Book 1, Other" type="text" />
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label class="col-sm-3 input-sm">Order No</label>
                            <div class="col-sm-9">
                                <input class="form-control input-sm"  name="order_book" value="" type="text" />
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label class="col-sm-3 input-sm">Order Date</label>
                            <div class="col-sm-9">
                                <input class="form-control input-sm"  name="order_book" value="" type="text" />
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label class="col-sm-3 input-sm">Name</label>
                            <div class="col-sm-9">
                                <textarea class="form-control" rows="2">Chaganbhai Maganbhai Adroja
12, Shiv Shakti Society,
Ramdham Main Road,
Rajkot - 360003
Mo 99240 20250</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 input-sm">References</label>
                            <div class="col-sm-9">
                                <textarea class="form-control"  rows="2"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <a href="javascript:void(0);">Add New Account</a>
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-6">
                        </div>
                        <div class="col-md-6">
                            <p>Price Is Conform By Customer</p>
                            <p>Price Not Conform By Customer</p>
                            <p>Some Price Conform Some Not</p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        &nbsp;
                    </div>
                    <div class="col-md-12 bg-low-green border-top-bottom">
                        Header Note :
                    </div>
                    <div class="col-md-12">
                        &nbsp;
                    </div>
                    <div class="col-md-12 no-padding">
                        <table class="table table-custom">
                            <thead>
                                <tr class="bg-sky-blue">
                                    <th>sr.</th>
                                    <th>Tag No.</th>
                                    <th>Item Description</th>
                                    <th>Size</th>
                                    <th>Pcs.</th>
                                    <th>Design No.</th>
                                    <th>Stone Wt.</th>
                                    <th>App Order Wt.</th>
                                    <th>Price/Grm &<br/> 10 Grm</th>
                                    <th>Metal Amnt.</th>
                                    <th>Labour%</th>
                                    <th>Labour/Grm</th>
                                    <th>Labour Amount</th>
                                    <th>Other Charges</th>
                                    <th>MRP</th>
                                    <th>Vat%</th>
                                    <th>Total</th>
                                    <th>Image Upload</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td><input type="text" class="custom-table-input" value="LGR0250"></td>
                                    <td>
                                        <select class="custom-table-input">
                                            <option>Ladies Ring[Gold 916]</option>
                                            <option>Mangal Sutra[Gold 916]</option>
                                            <option>Silver Ladies Ring[Silver 925]</option>
                                            <option>Fine Silver[999]</option>
                                        </select>
                                    </td>
                                    <td><input class="custom-table-input" type="text" value="16"></td>
                                    <td><input class="custom-table-input" type="text" value="1"></td>
                                    <td><input class="custom-table-input" type="text" value=""></td>
                                    <td><input class="custom-table-input" type="text" value="1250"></td>
                                    <td><input class="custom-table-input" type="text" value="4000Grm"></td>
                                    <td><input class="custom-table-input" type="text" value=""></td>
                                    <td><input class="custom-table-input" type="text" value=""></td>
                                    <td><input class="custom-table-input" type="text" value=""></td>
                                    <td><input class="custom-table-input" type="text" value=""></td>
                                    <td><input class="custom-table-input" type="text" value=""></td>
                                    <td><input class="custom-table-input" type="text" value=""></td>
                                    <td><input class="custom-table-input" type="text" value=""></td>
                                    <td><input class="custom-table-input" type="text" value=""></td>
                                    <td><input class="custom-table-input" type="text" value=""></td>
                                    <td>
                                        <span class="btn btn-primary btn-file btn-xs">
                                            Select file <input class="" type="file" value="">
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td><input type="text" class="custom-table-input" value="MNG0114"></td>
                                    <td>
                                        <select class="custom-table-input">
                                            <option>Ladies Ring[Gold 916]</option>
                                            <option selected>Mangal Sutra[Gold 916]</option>
                                            <option>Silver Ladies Ring[Silver 925]</option>
                                            <option>Fine Silver[999]</option>
                                        </select>
                                    </td>
                                    <td><input class="custom-table-input" type="text" value=""></td>
                                    <td><input class="custom-table-input" type="text" value="1"></td>
                                    <td><input class="custom-table-input" type="text" value=""></td>
                                    <td><input class="custom-table-input" type="text" value=""></td>
                                    <td><input class="custom-table-input" type="text" value="35.430Grm"></td>
                                    <td><input class="custom-table-input" type="text" value=""></td>
                                    <td><input class="custom-table-input" type="text" value=""></td>
                                    <td><input class="custom-table-input" type="text" value=""></td>
                                    <td><input class="custom-table-input" type="text" value=""></td>
                                    <td><input class="custom-table-input" type="text" value=""></td>
                                    <td><input class="custom-table-input" type="text" value=""></td>
                                    <td><input class="custom-table-input" type="text" value=""></td>
                                    <td><input class="custom-table-input" type="text" value=""></td>
                                    <td><input class="custom-table-input" type="text" value=""></td>
                                    <td>
                                        <span class="btn btn-primary btn-file btn-xs">
                                            Select file <input class="" type="file" value="">
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td><input type="text" class="custom-table-input" value=""></td>
                                    <td>
                                        <select class="custom-table-input">
                                            <option>Ladies Ring[Gold 916]</option>
                                            <option>Mangal Sutra[Gold 916]</option>
                                            <option selected>Silver Ladies Ring[Silver 925]</option>
                                            <option>Fine Silver[999]</option>
                                        </select>
                                    </td>
                                    <td><input class="custom-table-input" type="text" value="3"></td>
                                    <td><input class="custom-table-input" type="text" value=""></td>
                                    <td><input class="custom-table-input" type="text" value=""></td>
                                    <td><input class="custom-table-input" type="text" value="0.570"></td>
                                    <td><input class="custom-table-input" type="text" value="2.770Grm"></td>

                                    <td><input class="custom-table-input" type="text" value=""></td>
                                    <td><input class="custom-table-input" type="text" value=""></td>
                                    <td><input class="custom-table-input" type="text" value=""></td>
                                    <td><input class="custom-table-input" type="text" value=""></td>
                                    <td><input class="custom-table-input" type="text" value=""></td>
                                    <td><input class="custom-table-input" type="text" value=""></td>
                                    <td><input class="custom-table-input" type="text" value=""></td>
                                    <td><input class="custom-table-input" type="text" value=""></td>
                                    <td><input class="custom-table-input" type="text" value=""></td>
                                    <td>
                                        <span class="btn btn-primary btn-file btn-xs">
                                            Select file <input class="" type="file" value="">
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td><input type="text"  class="custom-table-input" value=""></td>
                                    <td>
                                        <select class="custom-table-input">
                                            <option>Ladies Ring[Gold 916]</option>
                                            <option>Mangal Sutra[Gold 916]</option>
                                            <option>Silver Ladies Ring[Silver 925]</option>
                                            <option selected>Fine Silver[999]</option>
                                        </select>
                                    </td>
                                    <td><input class="custom-table-input" type="text" value=" "></td>
                                    <td><input class="custom-table-input" type="text" value="10"></td>
                                    <td><input class="custom-table-input" type="text" value=""></td>
                                    <td><input class="custom-table-input" type="text" value=""></td>
                                    <td><input class="custom-table-input" type="text" value="4.980Kg"></td>

                                    <td><input class="custom-table-input" type="text" value=""></td>
                                    <td><input class="custom-table-input" type="text" value=""></td>
                                    <td><input class="custom-table-input" type="text" value=""></td>
                                    <td><input class="custom-table-input" type="text" value=""></td>
                                    <td><input class="custom-table-input" type="text" value=""></td>
                                    <td><input class="custom-table-input" type="text" value=""></td>
                                    <td><input class="custom-table-input" type="text" value=""></td>
                                    <td><input class="custom-table-input" type="text" value=""></td>
                                    <td><input class="custom-table-input" type="text" value=""></td>
                                    <td>
                                        <span class="btn btn-primary btn-file btn-xs">
                                            Select file <input class="" type="file" value="">
                                        </span>
                                    </td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th colspan="2" class="text-center">Total</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="col-md-12 bg-sky-blue border-top-bottom">
                        Footer Note :
                    </div>
                    <div class="col-md-12 padding-top-bottom-10-px">
                        <div class="col-md-8">
                            <div class="col-md-6">
                                <p>Condition:1,2,3,4,5,6,7,</p>
                                <p>Advance Received From Customer</p>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-5 input-sm">Order Due Date</label>
                                    <div class="col-sm-7">
                                        <input class="form-control input-sm"  name="order_book" value="" type="text" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="col-md-2">
                            </div>
                            <div class="col-md-10">
                                <div class="col-md-2"></div>
                                <div class="col-md-10 border">
                                    <p>Display Price All In Item</p>
                                    <p>1)916 Gold Price</p>
                                    <p>2)Fine Gold Price</p>
                                    <p>3)Silver Ornaments Price</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 padding-top-bottom-10-px">
                        <div class="col-md-5"></div>
                        <div class="col-md-6">
                            <button type="button" class="btn btn-sm btn-custom bg-sky-blue-gradient">Save & Printout</button>
                            <button type="button" class="btn btn-sm btn-custom bg-sky-blue-gradient">Save</button>
                            <button type="button" class="btn btn-sm btn-custom bg-sky-blue-gradient">Print</button>
                            <button type="button" class="btn btn-sm btn-custom bg-sky-blue-gradient">Exit</button>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.col -->
    </div>
</div>

