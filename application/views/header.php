<header class="main-header">
    <!-- Logo -->
    <a href="<?=base_url();?>" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>Jay</b>Khodiyar</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Jay </b>Khodiyar</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="javascript:void(0);" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?=isset($image) && file_exists(image_dir($image))?image_url($image):dist_url('img/user2-160x160.jpg');?>" class="user-image" alt="User Image">
                        <span class="hidden-xs"><?=isset($name)?ucwords($name):'Admin';?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?=isset($image) && file_exists(image_dir($image))?image_url($image):dist_url('img/user2-160x160.jpg');?>" class="img-circle" alt="User Image">
                            <p>
                                <?=isset($name)?ucwords($name):'Admin';?>
                                <br/>
                                <?=isset($email)?$email:'';?>
                            </p>
                        </li>


                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="<?=base_url()?>auth/profile" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="<?=base_url()?>auth/logout" class="btn btn-default btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>