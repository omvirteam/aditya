<?php if($this->session->flashdata('success') == true){?>
    <div class="col-sm-4 pull-right">
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i> <?=$this->session->flashdata('message')?></h4>
        </div>
    </div>
<?php }?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="custom content-header"  >
        <h1>
            <small class="text-primary text-bold">Group : Creation</small>
        </h1>
    </section>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-body">
                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label class="col-sm-5 input-sm">Group Name</label>
                                            <div class="col-sm-7">
                                                <input class="form-control input-sm"  name="group" type="text" />
                                            </div>
                                        </div>    
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label class="col-sm-5 input-sm">Group Print Name</label>
                                            <div class="col-sm-7">
                                                <input class="form-control input-sm"  name="group_print_name" type="text" />
                                            </div>
                                        </div>    
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label class="col-sm-5 input-sm">Under Group</label>
                                            <div class="col-sm-7">
                                                <input class="form-control input-sm auto-complete-demo"  name="under_group" type="text" />
                                            </div>
                                        </div>    
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label class="col-sm-5 input-sm"></label>
                                            <div class="col-sm-7">
                                                <input type="submit" value="Save" class="btn btn-success pull-right"/>
                                            </div>
                                        </div>                                        
                    </div>
                    <div class="col-md-7">
                        <table class="table table-bordered table-hover table-striped" id="datatable">
                            <thead>
                                <tr>
                                    <th width="30%">Group Name</th>
                                    <th width="30%">Group Print Name</th>
                                    <th width="25%">Under Group</th>
                                    <th width="15%">Action</th>
                                </tr>
                            </thead
                            <tbody>
                                <tr>
                                    <td>Group 1</td>
                                    <td>Group Print 1</td>
                                    <td>-</td>
                                    <td>
                                        <a class="btn btn-xs btn-primary">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        <a class="btn btn-xs btn-danger">
                                            <i class="fa fa-remove"></i>
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>    
        </div>
    </div>    
</div>
<script>
$(document).ready(function(){
    $("#datatable").DataTable();
    
    var availableTags = [
      "ActionScript",
      "AppleScript",
      "Asp",
      "BASIC",
      "C",
      "C++",
      "Clojure",
      "COBOL",
      "ColdFusion",
      "Erlang",
      "Fortran",
      "Groovy",
      "Haskell",
      "Java",
      "JavaScript",
      "Lisp",
      "Perl",
      "PHP",
      "Python",
      "Ruby",
      "Scala",
      "Scheme"
    ];    
    $(".auto-complete-demo").autocomplete({
        minLength: 1,
        source: availableTags
    }); 
});
</script>