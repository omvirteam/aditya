<?php
    $currUrl = $this->uri->segment(1);
    if($currUrl == ''){
        $currUrl = 'Dashboard';
    }
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title> <?=ucwords($currUrl)?> | Jay Khodiyar</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?=bootstrap_url('css/bootstrap.min.css');?>">
    <link rel="stylesheet" href="<?=bootstrap_url('css/custom.css');?>">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?=bootstrap_url('font-awesome/font-awesome.min.css');?>">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?=bootstrap_url('fonts/ionicons.min.css');?>">

    <!--Page Specific Css-->

    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="<?=plugins_url('iCheck/all.css');?>">

    <!--Page Specific Css-->

    <!-- Theme style -->
    <link rel="stylesheet" href="<?=dist_url('css/AdminLTE.min.css');?>">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?=dist_url('css/skins/_all-skins.min.css');?>">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?=plugins_url('iCheck/flat/blue.css');?>">
    <!-- Morris chart -->
    <link rel="stylesheet" href="<?=plugins_url('morris/morris.css');?>">
    <!-- jvectormap -->
    <link rel="stylesheet" href="<?=plugins_url('jvectormap/jquery-jvectormap-1.2.2.css');?>">
    <!-- Date Picker -->
    <link rel="stylesheet" href="<?=plugins_url('datepicker/datepicker3.css');?>">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?=plugins_url('daterangepicker/daterangepicker.css');?>">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="<?=plugins_url('bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css');?>">
    <?php //if($this->uri->segment(2) == 'quotation'){?>
        <!-- DataTables -->
        <link rel="stylesheet" href="<?=plugins_url('datatables/dataTables.bootstrap.css');?>">
    <?php //} ?>

    <!-- jQuery 2.2.3 -->
    <script src="<?=plugins_url('jQuery/jquery-2.2.3.min.js');?>"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="<?=bootstrap_url('js/jquery-ui.min.js');?>"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?=bootstrap_url('js/bootstrap.min.js');?>"></script>
</head>
<body class="hold-transition skin-blue sidebar-mini">

<div class="wrapper">


    <?php
        $this->load->view('header');
        $this->load->view('sidebar');
    ?>
