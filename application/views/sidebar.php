<?php
    $currUrl = $this->uri->segment(1);
?>
<!-- Main sidebar -->
<aside class="main-sidebar" style="overflow-y: auto;height: 100%;">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?=isset($image) && file_exists(image_dir($image))?image_url($image):dist_url('img/user2-160x160.jpg');?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>Admin</p>
                <a href="javascript:void(0);"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <?/*=$currUrl == ''?'class="active"':'';*/?>
            <li>
                <a href="<?=base_url();?>">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <li>
                <a href="<?=base_url()?>company/create">
                    <i class="fa fa-dashboard"></i> <span>Create Company</span>
                </a>
            </li>
            <li>
                <a href="<?=base_url()?>group">
                    <i class="fa fa-dashboard"></i> <span>Create Group</span>
                </a>
            </li>
            <li>
                <a href="<?=base_url()?>ledger">
                    <i class="fa fa-dashboard"></i> <span>Ledger</span>
                </a>
            </li>
            <li>
                <a href="<?=base_url()?>ledger/show-profit-loss">
                    <i class="fa fa-dashboard"></i> <span>Profit Loss</span>
                </a>
            </li>
            <li>
                <a href="<?=base_url()?>ledger/show-balance-sheet">
                    <i class="fa fa-dashboard"></i> <span>Balance Sheet</span>
                </a>
            </li>
            <li>
                <a href="<?=base_url()?>order/sales-order/">
                    <i class="fa fa-dashboard"></i> <span>Sales Order</span>
                </a>
            </li>
            <li>
                <a href="<?=base_url()?>order/purchase-order/">
                    <i class="fa fa-dashboard"></i> <span>Purchase Order</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
<!-- /main sidebar -->