<?php if($this->session->flashdata('success') == true){?>
    <div class="col-sm-4 pull-right">
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i> <?=$this->session->flashdata('message')?></h4>
        </div>
    </div>
<?php }?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="custom content-header"  >
        <h1>
            <small class="text-primary text-bold">Company : Creation</small>
            <button type="button" class="btn btn-info btn-xs pull-right btn_save_party">Save Company</button> 
            <button type="button" class="btn btn-primary btn-xs pull-right btn_load_party " style="margin-right:15px;">Load Company</button>
            <a href="javascript:void(0);" class="btn btn-success btn-xs pull-right" style="margin-right: 5px;">New Company</a>             
        </h1>
    </section>
    <div class="row">
        <div class="col-md-12">
            <!-- Custom Tabs -->
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab">Company Details</a></li>
                    <li><a href="#tab_2" data-toggle="tab">Address Details</a></li>
                    <li><a href="#tab_3" data-toggle="tab">Other Details</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- Horizontal Form -->
                                <div class="box box-info">
                                    <form class="form-horizontal">
                                        <input type="hidden" name="party_id" class="party_id" id="party_id">
                                        <div class="box-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label input-sm">State</label>
                                                        <div class="col-sm-9">
                                                            <select class="input-sm form-control" name="state" id="state">
                                                                <option value="">Select</option>
                                                                <option>Gujarat</option>
                                                                <option>Maharashtra</option>
                                                                <option>Rajsthan</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label input-sm">Number</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="input-sm form-control" placeholder="Number" name="number">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label input-sm">Company Name</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="input-sm form-control" placeholder="Company Name" name="company_name">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label input-sm">Company Short Name</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="input-sm form-control" placeholder="Company Short Name" name="company_short_name">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label input-sm">Company</label>
                                                        <div class="col-sm-9">
                                                            <select class="input-sm form-control" name="company">
                                                                <option value="">Select</option>
                                                                <option>With Stock</option>
                                                            </select>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label input-sm">Group Name</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="input-sm form-control" placeholder="Group Name" name="group_name">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label input-sm">Logo Image</label>
                                                        <div class="col-sm-9">
                                                            <input type="file" class="input-sm form-control" name="logo_image">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label input-sm">Nature of Business</label>
                                                        <div class="col-sm-9">
                                                            <select class="input-sm form-control" name="nature_of_business">
                                                                <option value="">Select</option>
                                                                <option>Bullion</option>
                                                                <option>Gold and Siver Manufacture</option>
                                                                <option>Retail</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="col-md-5">
                                                    <div class="col-md-12 text-center">
                                                        <label>Financial Year</label>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label input-sm">From Date</label>
                                                        <div class="col-sm-9">
                                                            <input class="form-control input-sm" type="text" name="from_date" placeholder="From Date">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label input-sm">To Date</label>
                                                        <div class="col-sm-9">
                                                            <input class="form-control input-sm" type="text" name="to_date" placeholder="To Date">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12 text-center">
                                                        <label>Security</label>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label input-sm">Security Type :</label>
                                                        <div class="col-sm-9">
                                                            <select class="input-sm form-control" name="security_type">
                                                                <option value="">Select</option>
                                                                <option>Normal</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12 text-center">
                                                        <label>Report Header</label>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label input-sm"></label>
                                                        <div class="col-sm-9">
                                                            <input class="form-control input-sm" type="text" name="to_date" placeholder="Report Header">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label input-sm"></label>
                                                        <div class="col-sm-9">
                                                            <input class="form-control input-sm" type="text" name="to_date" placeholder="Report Header">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.box -->
                            </div>
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_2">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- Horizontal Form -->
                                <div class="box box-info">
                                    <form class="form-horizontal">
                                        <div class="box-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label input-sm">Address</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="input-sm form-control" placeholder="Address 1" name="address1">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label input-sm"></label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="input-sm form-control" placeholder="Address 2" name="address2">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label input-sm"></label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="input-sm form-control" placeholder="Address 3" name="address3">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label input-sm">City</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="input-sm form-control" placeholder="City" name="city">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label input-sm">Pin code</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="input-sm form-control" placeholder="Pin Code" name="pincode">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label input-sm">Phone No</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="input-sm form-control" placeholder="Phone Number" name="phone">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label input-sm">Mobile</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="input-sm form-control" placeholder="Mobile Number" name="mobile">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label input-sm">Fax</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="input-sm form-control" placeholder="Fax" name="fax">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label input-sm">E-Mail</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="input-sm form-control" placeholder="E-Mail" name="email">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label input-sm">Website</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="input-sm form-control" placeholder="Website" name="website">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.box -->
                            </div>
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_3">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- Horizontal Form -->
                                <div class="box box-info">
                                    <form class="form-horizontal">
                                        <div class="box-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label input-sm">Tin No</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="input-sm form-control" placeholder="Tin No" name="tin_no">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label input-sm">Tin CST No</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="input-sm form-control" placeholder="Tin CST No" name="tin_cst_no">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label input-sm">PAN No</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="input-sm form-control" placeholder="PAN No" name="pan_no">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label input-sm">Excise No</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="input-sm form-control" placeholder="Excise No" name="excise_no">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label input-sm">TCS No</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="input-sm form-control" placeholder="TCS No" name="tcs_no">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label input-sm">Service Tax No</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="input-sm form-control" placeholder="Service Tax No" name="service_tax_no">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label input-sm">GST No</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="input-sm form-control" placeholder="GST No" name="gst_no">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.box -->
                            </div>
                        </div>
                    </div>
                    <?php /* <div class="row">
                        <div class="col-xs-8">
                        </div>
                        <div class="col-xs-2">
                            <a href="javascript:void(0);" class="btn input-sm btn-primary btn-block btn-flat" name="ok">Submit</a>
                        </div>
                        <div class="col-xs-2 ">
                            <button type="reset" class="btn input-sm btn-primary btn-block btn-flat" name="reset" onclick="document.location.href='createCompany.php'">Reset</button>
                        </div>
                    </div> */ ?>
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- nav-tabs-custom -->
        </div>
        <!-- /.col -->
    </div>
    <div class="box-body table-responsive no-padding">
        <div class="col-xs-12">
            <div class="row">
                <table class="table">
                    <tbody><tr>
                        <th>Action</th>
                        <th>ID</th>
                        <th>Company Name</th>
                        <th>Group Name</th>
                        <th>Nature of Business</th>
                        <th>City</th>
                        <th>Phone</th>
                        <th>Mobile</th>
                    </tr>
                    </tbody></table>
            </div>
        </div>
    </div>
</div>
