<!-- Main content -->
<div class="content-wrapper">
	<!-- Content area -->
	<div class="content">

		<!-- Advanced login -->
		<form action="<?=base_url();?>auth/login" method="post">
			<div class="panel panel-body login-form">
				<div class="text-center">
					<div class="icon-object border-slate-300 text-slate-300"><i class="icon-reading"></i></div>
					<h5 class="content-group">Login to your account <small class="display-block">Your credentials</small></h5>
				</div>

				<div class="form-group has-feedback has-feedback-left">
					<input type="text" class="form-control" placeholder="Username" name="login" id="login" value="<?=set_value('login')?>">
					<div class="form-control-feedback">
						<i class="icon-user text-muted"></i>
					</div>
					<label id="username-error" class="validation-error-label" for="username"><?php echo isset($errors['login'])?$errors['login']:''; ?></label>
				</div>

				<div class="form-group has-feedback has-feedback-left">
					<input type="password" class="form-control" placeholder="Password" name="password" value="<?=set_value('password')?>">
					<div class="form-control-feedback">
						<i class="icon-lock2 text-muted"></i>
					</div>
					<label id="password-error" class="validation-error-label" for="password"><?php echo isset($errors['password'])?$errors['password']:''; ?></label>
				</div>

				<div class="form-group login-options">
					<div class="row">
						<div class="col-sm-6">
							<label class="checkbox-inline">
								<input type="checkbox" class="styled" name="name" id="remember" value="1" checked="<?=set_value('remember')?>">
								Remember
							</label>
						</div>

						<div class="col-sm-6 text-right">
							<?php echo anchor('/auth/forgot_password/', 'Forgot password ?'); ?>
						</div>
					</div>
				</div>

				<div class="form-group">
					<button type="submit" class="btn bg-blue btn-block">Login <i class="icon-arrow-right14 position-right"></i></button>
				</div>

				<!--<div class="content-divider text-muted form-group"><span>Don't have an account?</span></div>
				<a href="<?/*=BASE_URL*/?>auth/register/" class="btn btn-default btn-block content-group">Sign up</a>-->
			</div>
		</form>
		<!-- /advanced login -->

	</div>
	<!-- /content area -->

</div>
<!-- /main content -->