<?php if($this->session->flashdata('success') == true){?>
    <div class="col-sm-4 pull-right">
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i> <?=$this->session->flashdata('message')?></h4>
        </div>
    </div>
<?php }?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="custom content-header"  >
        <h1>
            <small class="text-primary text-bold">Master : View Profit Loss</small>
        </h1>
    </section>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body no-padding">
                    <div class="col-md-6 profit-loss-left modal-success no-padding" style="border-right: 1px solid #000;">
                        <div class="col-md-12 profit-loss-header">
                            <div class="col-md-6 padding-top-bottom-10-px">
                                Particulars
                            </div>
                            <div class="col-md-6 text-center">
                                SHREE COMPUTER<br/>
                                1-Apr2014 to 1-Jun-2014
                            </div>
                        </div>
                        <div class="profit-loss-body">
                            <div class="section-item">
                                <div class="item-header">
                                    <div class="item-header-lable">
                                        Opening Stock
                                    </div>
                                    <div class="item-header-price">
                                        2,93,687.43
                                    </div>
                                </div>
                                <div class="section-item-list">
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Atx Cabinet
                                        </div>
                                        <div class="item-price">
                                            693.33
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            CcTv
                                        </div>
                                        <div class="item-price">
                                            61,965.33
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            DVD Writer
                                        </div>
                                        <div class="item-price">
                                            5369.06
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Harddisk
                                        </div>
                                        <div class="item-price">
                                            48679.20
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Keyboard
                                        </div>
                                        <div class="item-price">
                                            2607.20
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Keyboard Mouse Set
                                        </div>
                                        <div class="item-price">
                                            2798.49
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Laptop
                                        </div>
                                        <div class="item-price">
                                            19380.95
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Laptop Series
                                        </div>
                                        <div class="item-price">
                                            25222.30
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            LED
                                        </div>
                                        <div class="item-price">
                                            15423.79
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Motherboard
                                        </div>
                                        <div class="item-price">
                                            8833.80
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Mouse
                                        </div>
                                        <div class="item-price">
                                            792.14
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Network Product
                                        </div>
                                        <div class="item-price">
                                            8523.81
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Printer
                                        </div>
                                        <div class="item-price">
                                            44688.22
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Processor
                                        </div>
                                        <div class="item-price">
                                            13880.61
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Random Access Memory
                                        </div>
                                        <div class="item-price">
                                            12776.48
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            SMPS
                                        </div>
                                        <div class="item-price">
                                            5066.49
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Software
                                        </div>
                                        <div class="item-price">
                                            2577.44
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            UPS
                                        </div>
                                        <div class="item-price">
                                            2281.75
                                        </div>
                                    </div>
                                </div>
                                <div class="purchase-acc-section">
                                    <div class="purchase-acc-section-left dispaly-inline-block">
                                        <div class="purchase-acc-main-header text-semi-bold">
                                            Purchase Accounts
                                        </div>
                                        <div class="purchase-acc-sub-header padding-left-10-px">
                                            Purchase 4% A/c
                                        </div>
                                    </div>
                                    <div class="item-price purchase-acc-section-right dispaly-inline-block">
                                        &nbsp;
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="purchase-acc-section-left dispaly-inline-block">
                                        <div class="purchase-acc-main-header text-semi-bold">
                                            Gross Profit C/O
                                        </div>
                                    </div>   
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="item-price purchase-acc-section-right dispaly-inline-block text-right">
                                2,93,687.43
                            </div>                                    
                            <div class="clearfix"></div>
                            <div class="profit-loss-body">
                                <div class="col-md-12 section-item">
                            
                                    <div class="purchase-acc-section">
                                    <div class="purchase-acc-section-left dispaly-inline-block">
                                        <div class="purchase-acc-main-header text-semi-bold">
                                            Indirect Expenses
                                        </div>
                                    </div>   
                                    </div>
                                    <div class="item-row" style="margin-left: 15px;">
                                        <div class="item-lable">
                                            Account Charges
                                        </div>
                                        <div class="item-price">

                                        </div>
                                    </div>
                                    <div class="item-row" style="margin-left: 15px;">
                                        <div class="item-lable">
                                            Bank Charges A/C.
                                        </div>
                                        <div class="item-price">

                                        </div>
                                    </div>                                    
                                    <div class="item-row" style="margin-left: 15px;">
                                        <div class="item-lable">
                                            Depriciation A/C.
                                        </div>
                                        <div class="item-price">

                                        </div>
                                    </div>                                    
                                    <div class="item-row" style="margin-left: 15px;">
                                        <div class="item-lable">
                                            Electricity Bill Exp.
                                        </div>
                                        <div class="item-price">

                                        </div>
                                    </div>                                    
                                    <div class="item-row" style="margin-left: 15px;">
                                        <div class="item-lable">
                                            Legal Fee Exp. A/C.
                                        </div>
                                        <div class="item-price">

                                        </div>
                                    </div>                                    
                                </div>
                            </div>    
                            <div class="clearfix"></div>
							
                            <div class="item-price purchase-acc-section-right dispaly-inline-block text-right" style="border: none;">
                                <a href="#" style="color: red;">6 More ...</a>
                                <div class="clearfix" style="height: 2px;"></div>
                            </div>
							
                            <div class="col-md-12 section-item" style="border-top: 1px solid;border-bottom:1px solid;">
                                <div class="pull-left">Total</div>
                                <div class="pull-right">10,250</div>
                            </div>
							
                        </div>
                    </div>
                    <div class="col-md-6 profit-loss-right  modal-danger no-padding">
                        <div class="col-md-12 profit-loss-header">
                            <div class="col-md-6 padding-top-bottom-10-px">
                                Particulars
                            </div>
                            <div class="col-md-6 text-center">
                                SHREE COMPUTER<br/>
                                1-Apr2014 to 1-Jun-2014
                            </div>
                        </div>
                        <div class="profit-loss-body">
                        <div class="col-md-12 section-item">
                            
                                <div class="purchase-acc-section">
                                    <div class="purchase-acc-section-left dispaly-inline-block">
                                        <div class="purchase-acc-main-header text-semi-bold">
                                            Sales Account
                                        </div>
                                        <div class="purchase-acc-sub-header padding-left-10-px">
                                            Sales @ 12.5%
                                        </div>
                                        <div class="purchase-acc-sub-header padding-left-10-px">
                                            Sales @ 4%
                                            <div style="border-bottom: 1px solid #000;float: right;width: 100px;position:absolute;right: 90px;top: 30px;">&nbsp;</div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="purchase-acc-section-left dispaly-inline-block">
                                        <div class="purchase-acc-main-header text-semi-bold">
                                            Gross Profit C/O
                                        </div>
                                    </div>   
                                </div>                            
                                <div class="section-item-list">
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Atx Cabinet
                                        </div>
                                        <div class="item-price">
                                            693.33
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            CcTv
                                        </div>
                                        <div class="item-price">
                                            61,965.33
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            DVD Writer
                                        </div>
                                        <div class="item-price">
                                            5369.06
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Harddisk
                                        </div>
                                        <div class="item-price">
                                            48679.20
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Keyboard
                                        </div>
                                        <div class="item-price">
                                            2607.20
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Keyboard Mouse Set
                                        </div>
                                        <div class="item-price">
                                            2798.49
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Laptop
                                        </div>
                                        <div class="item-price">
                                            19380.95
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Laptop Series
                                        </div>
                                        <div class="item-price">
                                            25222.30
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            LED
                                        </div>
                                        <div class="item-price">
                                            15423.79
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Motherboard
                                        </div>
                                        <div class="item-price">
                                            8833.80
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Mouse
                                        </div>
                                        <div class="item-price">
                                            792.14
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Network Product
                                        </div>
                                        <div class="item-price">
                                            8523.81
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Printer
                                        </div>
                                        <div class="item-price">
                                            44688.22
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Processor
                                        </div>
                                        <div class="item-price">
                                            13880.61
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Random Access Memory
                                        </div>
                                        <div class="item-price">
                                            12776.48
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            SMPS
                                        </div>
                                        <div class="item-price">
                                            5066.49
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Software
                                        </div>
                                        <div class="item-price">
                                            2577.44
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            UPS
                                        </div>
                                        <div class="item-price">
                                            2281.75
                                        </div>
                                    </div>
                                </div>
								
                                <div class="purchase-acc-section">

                                    <div class="item-price purchase-acc-section-right dispaly-inline-block">
                                        &nbsp;
                                    </div>

                                    <div class="clearfix">&nbsp;</div>
                                    <div class="purchase-acc-section-left dispaly-inline-block">
                                        <div class="purchase-acc-main-header text-semi-bold">
                                            Gross Profit B/F
                                        </div>
                                    </div>
                                </div>
								<div class="item-price purchase-acc-section-right dispaly-inline-block text-right">
									2,93,687.43
								</div>
								<div class="clearfix">&nbsp;</div>
								<div class="profit-loss-body">
                                <div class="col-md-12 section-item">
                            
                                    <div class="purchase-acc-section">
                                    <div class="purchase-acc-section-left dispaly-inline-block">
                                        <div class="purchase-acc-main-header text-semi-bold">
                                            Indirect Expenses
                                        </div>
                                    </div>   
                                    </div>
                                                                       
                                    <div class="item-row" style="margin-left: 15px;">
                                        <div class="item-lable">
                                            Repairing Charges Income
                                        </div>
                                        <div class="item-price">
											10,250
                                        </div>
                                    </div>                                    
                                                                   
                                    <div class="item-row" style="margin-left: 15px;">
                                        <div class="item-lable">
                                            Roundoff (+/-)
                                        </div>
                                        <div class="item-price" style="border-bottom:1px solid;">
											&nbsp;
                                        </div>
                                    </div>                            
                                    
                                  <div class="clearfix">&nbsp;</div>
                                  <div class="clearfix">&nbsp;</div>
                                  
                                </div>
                            </div>
							</div>
                            <div class="col-md-12 section-item" style="border-top: 1px solid;border-bottom:1px solid;">
                                <div class="pull-left">Total</div>
                                <div class="pull-right">10,250</div>
                            </div>
                            
						</div>
                    </div>
                    <div class="clearfix">&nbsp;</div>
                </div>
            </div>
        </div>
        <!-- /.col -->
    </div>
</div>
