<?php if ($this->session->flashdata('success') == true) { ?>
    <div class="col-sm-4 pull-right">
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i> <?= $this->session->flashdata('message') ?></h4>
        </div>
    </div>
<?php } ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="custom content-header"  >
        <h1>
            <small class="text-primary text-bold">Master : View Balance Sheet</small>
        </h1>
    </section>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body no-padding">
                    <div class="col-md-6 profit-loss-left modal-success no-padding" style="height: 95vh;">
                        <div class="col-md-12 profit-loss-header">
                            <div class="col-md-6 padding-top-bottom-10-px">
                                Liablities
                            </div>
                            <div class="col-md-6 text-center">
                                Ravi Textile<br/>
                                as at 31-Jul-2007
                            </div>
                        </div>
                        <div class="profit-loss-body">
                            <div class="section-item">
                                <div class="item-header">
                                    <div class="item-header-lable">
                                        Capital Account
                                    </div>
                                    <div class="item-header-price">
                                        5,87,00,000.00
                                    </div>
                                </div>
                                <div class="section-item-list">
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Reserve & Surplus
                                        </div>
                                        <div class="item-price">
                                            10,00,000.00
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Drawing
                                        </div>
                                        <div class="item-price">
                                            (-)1,00,000.00
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Owner Capital
                                        </div>
                                        <div class="item-price">
                                            <span style="border-bottom:1px solid;">5,78,00,000.00</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">&nbsp;</div>
                                <div class="purchase-acc-section">
                                    <div class="purchase-acc-section-left dispaly-inline-block">
                                        <div class="purchase-acc-main-header text-semi-bold">
                                            Loans (Liablity)
                                        </div>
                                    </div>
                                    <div class="item-price dispaly-inline-block">
                                        &nbsp;
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="purchase-acc-section-left dispaly-inline-block">
                                        <div class="purchase-acc-main-header text-semi-bold">
                                            Current Liablities
                                        </div>
                                    </div>   
                                </div>
                                <div class="item-price purchase-acc-section-right text-semi-bold dispaly-inline-block text-right">
                                    1,11,17,500.00
                                </div>
                            </div>

                            <div class="clearfix">&nbsp;</div>
                            <div class="section-item-list">
                                <div class="item-row">
                                    <div class="item-lable">
                                        Sundry Creditors
                                    </div>
                                    <div class="item-price">
                                        91,97,500.00
                                    </div>
                                </div>
                                <div class="item-row">
                                    <div class="item-lable">
                                        Accured Expenses
                                    </div>
                                    <div class="item-price">
                                        5,590,000.00
                                    </div>
                                </div>
                                <div class="item-row">
                                    <div class="item-lable">
                                        Interest accrued in Long Term Loan
                                    </div>
                                    <div class="item-price">
                                        5,05,000.00
                                    </div>
                                </div>
                                <div class="item-row">
                                    <div class="item-lable">
                                        Sales Tax Payable
                                    </div>
                                    <div class="item-price">
                                        1,25,000.00
                                    </div>
                                </div>
                                <div class="item-row">
                                    <div class="item-lable">
                                        Unclaimed Dividents
                                    </div>
                                    <div class="item-price">
                                        <span style="border-bottom:1px solid;">7,00,000.00</span>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                        </div>
                    </div>
                    <div class="col-md-6 profit-loss-right  modal-danger no-padding" style="height: 100vh;border-left: solid 1px;">
                        <div class="col-md-12 profit-loss-header">
                            <div class="col-md-6 padding-top-bottom-10-px">
                                Assets
                            </div>
                            <div class="col-md-6 text-center">
                                Ravi Textile [Fiazan]<br/>
                                as at 31-Jul-2007
                            </div>
                        </div>
                        <hr/>
                        <div class="profit-loss-body">
                            <div class="col-md-12 section-item">

                                <div class="purchase-acc-section">
                                    <div class="purchase-acc-section-left dispaly-inline-block">
                                        <div class="purchase-acc-main-header text-semi-bold">
                                            Fixed Assets
                                            <div style="font-weight: bold;position: absolute;right: 15px;float:right;top: 0px;">4,39,13,000.00</div>
                                        </div>
                                    </div>
                                </div>                 
                                <div class="clearfix"></div>
                                <div class="section-item-list">
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Acc. Dep. Furniture
                                        </div>
                                        <div class="item-price">
                                            1,25,000.00
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Acc. Dep. Land
                                        </div>
                                        <div class="item-price">
                                            2,50,000.00
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Acc. Dep. Machinery
                                        </div>
                                        <div class="item-price">
                                            2,50,000.00
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Acc. Dep. Motor Vechicles
                                        </div>
                                        <div class="item-price">
                                            5,50,000.00
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Acc. Dep. Office Equipment
                                        </div>
                                        <div class="item-price">
                                            3,00,000.00s
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Computer
                                        </div>
                                        <div class="item-price">
                                            18,0000
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Furniture & Fixture
                                        </div>
                                        <div class="item-price">
                                            12,00,000.00
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Joki Machine
                                        </div>
                                        <div class="item-price">
                                            12,00,000.00
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Land & Building
                                        </div>
                                        <div class="item-price">
                                            2,50,00,000.00
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Machinery
                                        </div>
                                        <div class="item-price">
                                            75,00,000.00
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Motor Vechicles
                                        </div>
                                        <div class="item-price">
                                            1,00,00,000.00
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Suzuki Pickup
                                        </div>
                                        <div class="item-price" style="border-bottom: 1px solid #000;">
                                            50,000.00
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="purchase-acc-section">
                                    <div class="purchase-acc-section-left dispaly-inline-block">
                                        <div class="purchase-acc-main-header text-semi-bold">
                                            Investments
                                            <div style="font-weight: bold;position: absolute;right: 15px;float:right;top: 268px;">38,50,000.00</div>
                                        </div>
                                    </div>
                                </div>       
                                <div class="section-item-list">
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Long Term Investments
                                        </div>
                                        <div class="item-price" style="border-bottom: 1px solid #000;">
                                            38,50,000.00
                                        </div>
                                    </div>
                                </div>
                                <div class="purchase-acc-section">
                                    <div class="purchase-acc-section-left dispaly-inline-block">
                                        <div class="purchase-acc-main-header text-semi-bold">
                                            Current Assets
                                            <div style="font-weight: bold;position: absolute;right: 15px;float:right;top: 314px;">02,04,06.700.00</div>
                                        </div>
                                    </div>
                                </div>       
                                <div class="section-item-list">
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Closing Stock
                                        </div>
                                        <div class="item-price">
                                            84,85,200.00
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Sundry Debtors
                                        </div>
                                        <div class="item-price">
                                            31,45,000.00
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Cash In Hand
                                        </div>
                                        <div class="item-price">
                                            36,64,000.00
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Bank Accounts
                                        </div>
                                        <div class="item-price">
                                            23,12,500.00
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Deffered Expenses
                                        </div>
                                        <div class="item-price">
                                            20,00,000.00
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Rent Security
                                        </div>
                                        <div class="item-price">
                                            3,00,000.00
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Wapda Security
                                        </div>
                                        <div class="item-price"  style="border-bottom: 1px solid #000;">
                                            3,00,000.00
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="purchase-acc-section">
                                    <div class="purchase-acc-section-left dispaly-inline-block">
                                        <div class="purchase-acc-main-header text-semi-bold">
                                            Profit & Loss A/c
                                            <div style="font-weight: bold;position: absolute;right: 15px;float:right;top: 477px;">16,47,800.00</div>
                                        </div>
                                    </div>
                                </div>       
                                <div class="section-item-list">                                  
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Opening Balance
                                        </div>
                                        <div class="item-price">
                                            16,47,800.00
                                        </div>
                                    </div>
                                    <div class="item-row">
                                        <div class="item-lable">
                                            Current Period 
                                        </div>
                                        <div class="item-price" style="border-bottom: 1px solid #000;">
                                            16,47,800.00
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6 section-item" style="border-top: 1px solid;border-bottom:1px solid;">
                        <div class="pull-left">Total</div>
                        <div class="pull-right">6,98,17,500.00</div>
                    </div>
                    
                    <div class="col-md-6 section-item" style="border-left: 1px solid;border-top: 1px solid;border-bottom:1px solid;">
                        <div class="pull-left">Total</div>
                        <div class="pull-right">6,98,17,500.00</div>
                    </div>                        
                    
                </div>
            </div>
        </div>
    </div>
    <!-- /.col -->
</div>
</div>
