<?php if($this->session->flashdata('success') == true){?>
    <div class="col-sm-4 pull-right">
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i> <?=$this->session->flashdata('message')?></h4>
        </div>
    </div>
<?php }?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="custom content-header"  >
        <h1>
            <small class="text-primary text-bold">Master : View Ledger</small>


            <button type="button" class="btn btn-info btn-xs pull-right btn_save_party">Save Ledger</button>
            <button type="button" class="btn btn-info btn-xs pull-right btn_load_party" style="margin-right:15px;">Load Ledger</button>
            <a href="#" class="btn btn-success btn-xs pull-right" style="margin-right: 5px;">New Ledger</a>
        </h1>
    </section>
    <div class="row">
        <div class="col-md-12">
            <!-- Custom Tabs -->
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab">Ledger Details</a></li>
                    <li><a href="#tab_2" data-toggle="tab">Contact Person</a></li>
                    <li><a href="#tab_3" class="address-works-tab" data-toggle="tab">Address Works</a></li>
                    <li><a href="#tab_4" data-toggle="tab">Terms & Conds</a></li>
                    <li><a href="#tab_5" data-toggle="tab">Tax & Excise</a></li>
                    <li><a href="#tab_6" data-toggle="tab">General</a></li>
                    <li><a href="#tab_7" data-toggle="tab">Company Profile</a></li>
                    <li><a href="#tab_8" data-toggle="tab">Billing Terms</a></li>
                    <li><a href="#tab_9" data-toggle="tab">Delivery Addresses</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- Horizontal Form -->
                                <div class="box box-info">
                                    <form class="form-horizontal" id="partycodeform">
                                        <input type="hidden" name="party_id" class="party_id" id="party_id" value="<?php if(isset($party)){ echo $party[0]['party_id'];} ?>">
                                        <div class="box-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <fieldset class="scheduler-border">
                                                        <legend class="scheduler-border text-primary text-bold"> Account Details </legend>
                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-3 control-label input-sm">Group Name</label>

                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control input-sm" id="inputEmail3" placeholder="">
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <div class="clearfix">&nbsp;</div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label input-sm">Ledger Name</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm party_name" name="party_name" id="party_name" placeholder="" value="<?php if(isset($party)){ echo $party[0]['party_name'];} ?>">
                                                            <span style="display:none;" class="party_namespan membererror">Ledger Name must be Required.</span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label input-sm">Ledger Type</label>
                                                        <div class="col-sm-9 no-padding">
                                                            <div class="col-sm-6">
                                                                <input type="text" class="form-control input-sm party_type_1" name="party_type_1" id="party_type_1" placeholder="" value="<?php if(isset($party)){ echo $party[0]['party_type_1'];} ?>">
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <input type="text" class="form-control input-sm party_type_2" name="party_type_2" id="party_type_2" placeholder="" value="<?php if(isset($party)){ echo $party[0]['party_type_2'];} ?>">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label input-sm">Branch</label>
                                                        <div class="col-sm-9 dispaly-flex">
                                                            <input type="text" class="form-control input-sm branch" name="branch" id="branch" placeholder="" value="<?php if(isset($party)){ echo $party[0]['branch_id'];} ?>">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label input-sm">Reference</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm reference" id="reference" name="reference_id" value="<?php if(isset($party)){ echo $party[0]['reference_id'];} ?>">
                                                        </div>
                                                        <div class="clearfix">&nbsp;</div>
                                                        <div class="col-sm-3">
                                                        </div>
                                                        <div class="col-sm-9">
                                                            <textarea class="form-control" placeholder="Description" name="reference_description"><?php if(isset($party)){ echo $party[0]['reference_description'];} ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-7">
                                                    <div class="form-group dispaly-flex">
                                                        <img alt="" src="<?=image_url('2.png');?>">
                                                        <label for="inputEmail3" class="col-sm-3 control-label input-sm">Active</label>
                                                        <div class="col-sm-9">
                                                            <input type="checkbox" class="input-sm active"  name="active" <?php if(isset($party)){ echo ($party[0]['active'] == 1) ? 'checked' : '';} ?> >
                                                        </div>
                                                    </div>
                                                    <br/>
                                                    <br/>
                                                    <table class="table table-bordered">
                                                        <thead>
                                                        <tr>
                                                            <th>Sr.No.</th>
                                                            <th>Particular</th>
                                                            <th colspan="2">Opening1</th>
                                                            <th>CrOrDr</th>
                                                            <th colspan="2">Opening2</th>
                                                            <th>CrOrDr</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td>1</td>
                                                            <td>Fine Gold</td>
                                                            <td>
                                                                <input type="text" class="form-control input-sm">
                                                            </td>
                                                            <td>
                                                                <select>
                                                                    <option>Gram</option>
                                                                </select>
                                                            </td>
                                                            <td>
                                                                <select class="btn-block">
                                                                    <option>Dr</option>
                                                                </select>
                                                            </td>
                                                            <td>
                                                                <input type="text" class="form-control input-sm">
                                                            </td>
                                                            <td>
                                                                <select>
                                                                    <option>Gram</option>
                                                                </select>
                                                            </td>
                                                            <td>
                                                                <select class="btn-block">
                                                                    <option>Dr</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>1</td>
                                                            <td>Fine Silver</td>
                                                            <td>
                                                                <input type="text" class="form-control input-sm">
                                                            </td>
                                                            <td>
                                                                <select class="btn-block">
                                                                    <option>KG</option>
                                                                </select>
                                                            </td>
                                                            <td>
                                                                <select class="btn-block">
                                                                    <option>Cr</option>
                                                                </select>
                                                            </td>
                                                            <td>
                                                                <input type="text" class="form-control input-sm">
                                                            </td>
                                                            <td>
                                                                <select class="btn-block">
                                                                    <option>KG</option>
                                                                </select>
                                                            </td>
                                                            <td>
                                                                <select class="btn-block">
                                                                    <option>Cr</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>1</td>
                                                            <td>Cash</td>
                                                            <td>
                                                                <input type="text" class="form-control input-sm">
                                                            </td>
                                                            <td>
                                                                <select class="btn-block">
                                                                    <option>Rs.</option>
                                                                </select>
                                                            </td>
                                                            <td>
                                                                <select class="btn-block">
                                                                    <option>Dr</option>
                                                                </select>
                                                            </td>
                                                            <td>
                                                                <input type="text" class="form-control input-sm">
                                                            </td>
                                                            <td>
                                                                <select class="btn-block">
                                                                    <option>Rs.</option>
                                                                </select>
                                                            </td>
                                                            <td>
                                                                <select class="btn-block">
                                                                    <option>Dr</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>

                                                    <!--<div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label input-sm">OpBal</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" class="form-control input-sm opening_bal" name="opening_bal" id="opening_bal" placeholder="0.00" value="<?php /*if(isset($party)){ echo $party[0]['opening_bal'];} */?>">
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <select class="form-control input-sm">
                                                                <option>Cr</option>
                                                                <option>Dr</option>
                                                            </select>
                                                        </div>
                                                    </div>-->

                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-2 input-sm text-left"> Credit Limit</label>

                                                        <div class="col-sm-5">
                                                            <input type="text" class="form-control input-sm credit_limit" name="credit_limit" id="credit_limit" placeholder="0.00" value="<?php if(isset($party)){ echo $party[0]['credit_limit'];} ?>">
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="clearfix">&nbsp;</div>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.box -->
                            </div>
                            <div class="col-md-12">
                                <form class="form-horizontal" id="partydetailform">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <fieldset class="scheduler-border">
                                                <legend class="scheduler-border text-primary text-bold"> Ledger Details </legend>
                                                <div class="form-group">
                                                    <label for="inputEmail3" class="col-sm-3 control-label input-sm">Address</label>

                                                    <div class="col-sm-9">
                                                        <textarea class="form-control" rows="1" name="address" id="main_address"><?php if(isset($party)){ echo $party[0]['address'];} ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputEmail3" class="col-sm-3 control-label input-sm">City</label>
                                                    <div class="col-sm-9 dispaly-flex">
                                                        <input type="text" class="form-control input-sm city" name="city" id="city" placeholder="" value="<?php if(isset($party)){ echo $party[0]['city_id'];} ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputEmail3" class="col-sm-3 control-label input-sm">State</label>

                                                    <div class="col-sm-9 dispaly-flex">
                                                        <input type="text" class="form-control input-sm state" name="state" id="state" placeholder=""  value="<?php if(isset($party)){ echo $party[0]['state_id'];} ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputEmail3" class="col-sm-3 control-label input-sm">Country</label>

                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control input-sm country" name="country" id="country" placeholder=""  value="<?php if(isset($party)){ echo $party[0]['country_id'];} ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputEmail3" class="col-sm-3 control-label input-sm">Area</label>

                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control input-sm area" id="area" name="area" placeholder="" value="<?php if(isset($party)){ echo $party[0]['area'];} ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputEmail3" class="col-sm-3 control-label input-sm">Pin-Code</label>

                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control input-sm pincode" id="pincode" name="pincode" placeholder="" value="<?php if(isset($party)){ echo $party[0]['pincode'];} ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputEmail3" class="col-sm-3 control-label input-sm">Phone No.</label>
                                                    <div class="col-sm-9">
                                                        <textarea class="form-control phone_no" id="phone_no" name="phone_no" placeholder=""><?php if(isset($party)){ echo str_replace(',', "\n", $party[0]['phone_no']);} ?></textarea>
                                                        <span class="">Add multiple phone number by pressing enter key.</span>
                                                        <span style="display:none;" class="phone_nospan membererror">Phone No must be Required.</span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputEmail3" class="col-sm-3 control-label input-sm">Fax No.</label>

                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control input-sm fax_no" id="fax_no" name="fax_no" placeholder="" value="<?php if(isset($party)){ echo $party[0]['fax_no'];} ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputEmail3" class="col-sm-3 control-label input-sm">E-mail ID</label>

                                                    <div class="col-sm-9">
                                                        <textarea class="form-control email_id" id="email_id" name="email_id"><?php if(isset($party)){ echo str_replace(',', "\n", $party[0]['email_id']);} ?></textarea>
                                                        <span class="">Add multiple email by pressing enter key.</span>
                                                        <span style="display:none;" class="email_idspan membererror">E-mail ID must be Required.</span>
                                                        <span style="display:none;" class="notvalidemail membererror">Invalid Email Address...!</span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputEmail3" class="col-sm-3 control-label input-sm">Web Site</label>

                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control input-sm website" id="website" name="website"  placeholder="" value="<?php if(isset($party)){ echo $party[0]['website'];} ?>">
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <fieldset class="scheduler-border">
                                                        <legend class="scheduler-border text-primary text-bold"> Get Ledger Details </legend>
                                                        <div class="form-group">
                                                            <div class="col-sm-6">
                                                                <div class="checkbox">
                                                                    <label>
                                                                        <input type="checkbox"> From Quatation
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-sm-9">
                                                                <div class="checkbox">
                                                                    <label>
                                                                        <input type="checkbox"> From Sales Support
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <button type="button" class="btn btn-info btn-xs pull-right">Get Ledger Detail</button>
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row form-group">
                                                        <button type="button" class="btn btn-info btn-xs pull-right button-width" style="margin-top: 7px;">Payment Schedule</button>
                                                    </div>
                                                    <div class="row form-group">
                                                        <button type="button" class="btn btn-info btn-xs pull-right button-width">Preview Envelope</button>
                                                    </div>
                                                    <div class="row form-group">
                                                        <button type="button" class="btn btn-info btn-xs pull-right button-width">Export Invoice Image</button>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_2">
                        <input type="hidden" name="party_id" class="party_id" id="party_id" value="<?php echo isset($party) && isset($party[0]['party_id']) ? $party[0]['party_id']:''?>">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- Horizontal Form -->
                                <div class="box box-info">
                                    <form class="form-horizontal">
                                        <div class="box-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-3 control-label input-sm">Name</label>

                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control input-sm contact_person_name" name="contact_person_name" id="contact_person_name" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-3 control-label input-sm">Priority</label>

                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control input-sm contact_person_priority" id="contact_person_priority" name="contact_person_priority" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-3 control-label input-sm">Designation</label>

                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control input-sm contact_person_designation" id="contact_person_designation" name="contact_person_designation" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-3 control-label input-sm">Department</label>

                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control input-sm contact_person_department" id="contact_person_department" name="contact_person_department" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-3 control-label input-sm">Phone No.</label>

                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control input-sm contact_person_phone_no" name="contact_person_phone_no" id="contact_person_phone_no" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-3 control-label input-sm">Mobile No.</label>

                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control input-sm contact_person_mobile_no" name="contact_person_mobile_no" id="contact_person_mobile_no" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-3 control-label input-sm">Fax No.</label>

                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control input-sm contact_person_fax_no" name="contact_person_fax_no" id="contact_person_fax_no" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-3 control-label input-sm">Email</label>

                                                            <div class="col-sm-9">
                                                                <input type="email" class="form-control input-sm contact_person_email" name="contact_person_email" id="contact_person_email" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-3 control-label input-sm">Note</label>

                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control input-sm contact_person_note" name="contact_person_note" id="contact_person_note" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 text-left">
                                                        <!-- <button type="button" class="btn btn-info btn-xs btn_add_contact_person">Add Contact Person</button> -->
                                                        <button type="button" class="btn btn-info btn-xs btn_add_contact_person">Add Contact Person</button>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <br/>
                                                    <div class="col-md-1"></div>
                                                    <div class="col-md-10">
                                                        <table class="table custom-table tbl-contact-person">
                                                            <thead>
                                                            <tr>
                                                                <th>Priority</th>
                                                                <th>Name</th>
                                                                <th>Designation</th>
                                                                <th>Department</th>
                                                                <th>Phone No.</th>
                                                                <th>Email</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody id="tbl-contact-person-body">
                                                            <?php if(isset($party) && count($party[0]['contact_person']) > 0): ?>

                                                                <?php foreach($party[0]['contact_person'] as $person): ?>
                                                                    <tr>
                                                                        <td><?php echo $person['priority']; ?></td>
                                                                        <td><?php echo $person['name']; ?></td>
                                                                        <td><?php echo $this->app_model->get_id_by_val('designation','designation','designation_id',$person['designation_id'])?></td>
                                                                        <td><?php echo $this->app_model->get_id_by_val('department','department','department_id',$person['department_id'])?></td>
                                                                        <td><?php echo $person['phone_no']; ?></td>
                                                                        <td><?php echo $person['email']; ?></td>
                                                                    </tr>
                                                                <?php endforeach; ?>
                                                            <?php endif; ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.box -->
                            </div>
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_3">
                        <input type="hidden" name="party_id" class="party_id" value="<?php echo isset($party) && isset($party[0]['party_id']) ? $party[0]['party_id']:''?>">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- Horizontal Form -->
                                <div class="box box-info">
                                    <form class="form-horizontal">
                                        <div class="box-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="col-md-4 input-sm">
                                                                Same As Ledger Details ?
                                                            </label>
                                                            <label class="col-md-4">
                                                                <input type="radio" name="address_work" class="address_work" id="address_work" value="1" checked="checked"> Yes &nbsp;&nbsp;<input type="radio" name="address_work" id="address_work" class="address_work" value="0"> No
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-3 control-label input-sm">Address</label>

                                                            <div class="col-sm-9">
                                                                <textarea class="form-control input-sm" id="w_address" name="w_address"><?php if(isset($party)){ echo $party[0]['w_address'];} ?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-3 control-label input-sm">City</label>

                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control input-sm w_city" id="w_city" name="w_city" placeholder="" value="<?php if(isset($party)){ echo $party[0]['w_city'];} ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-3 control-label input-sm">State</label>

                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control input-sm w_state" id="w_state" name="w_state" placeholder="" value="<?php if(isset($party)){ echo $party[0]['w_state'];} ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-3 control-label input-sm">Country</label>

                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control input-sm w_country" id="w_country" name="w_country" placeholder="" value="<?php if(isset($party)){ echo $party[0]['w_country'];} ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-3 control-label input-sm">Phone 1</label>

                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control input-sm" id="w_phone1" name="w_phone1" placeholder="" value="<?php if(isset($party)){ echo $party[0]['w_phone1'];} ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-3 control-label input-sm">Email</label>

                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control input-sm" id="w_email" name="w_email" placeholder="" value="<?php if(isset($party)){ echo $party[0]['w_email'];} ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-3 control-label input-sm">Web</label>

                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control input-sm" id="w_web" name="w_web" placeholder="" value="<?php if(isset($party)){ echo $party[0]['w_web'];} ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-3 control-label input-sm">Phone 2</label>

                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control input-sm" id="w_phone2" name="w_phone2" placeholder="" value="<?php if(isset($party)){ echo $party[0]['w_phone2'];} ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-3 control-label input-sm">Phone 3</label>

                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control input-sm" id="w_phone3" name="w_phone3" placeholder="" value="<?php if(isset($party)){ echo $party[0]['w_phone3'];} ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-3 control-label input-sm">Note</label>

                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control input-sm" id="w_note" name="w_note" placeholder="" value="<?php if(isset($party)){ echo $party[0]['w_note'];} ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.box -->
                            </div>
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_4">

                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_5">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-md-3 control-label input-sm">TIN VAT No.</label>

                                <div class="col-sm-9">
                                    <input type="text" class="form-control input-sm" name="tin_vat_no" id="tin_vat_no" placeholder="" value="<?php if(isset($party)){ echo $party[0]['tin_vat_no'];} ?>">
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-md-3 control-label input-sm">TIN CST No.</label>

                                <div class="col-sm-9">
                                    <input type="text" class="form-control input-sm" name="tin_cst_no" id="tin_cst_no" placeholder="" value="<?php if(isset($party)){ echo $party[0]['tin_cst_no'];} ?>">
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-md-3 control-label input-sm">ECC No.</label>

                                <div class="col-sm-9">
                                    <input type="text" class="form-control input-sm" name="ecc_no" id="ecc_no" placeholder="" value="<?php if(isset($party)){ echo $party[0]['ecc_no'];} ?>">
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-md-3 control-label input-sm">PAN No.</label>

                                <div class="col-sm-9">
                                    <input type="text" class="form-control input-sm" name="pan_no" id="pan_no" placeholder="" value="<?php if(isset($party)){ echo $party[0]['pan_no'];} ?>">
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-md-3 control-label input-sm">Range</label>

                                <div class="col-sm-9">
                                    <input type="text" class="form-control input-sm" name="range" id="range" placeholder="" value="<?php if(isset($party)){ echo $party[0]['range'];} ?>">
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-md-3 control-label input-sm">Division</label>

                                <div class="col-sm-9">
                                    <input type="text" class="form-control input-sm" name="division" id="division" placeholder="" value="<?php if(isset($party)){ echo $party[0]['division'];} ?>">
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-md-3 control-label input-sm">Service Tax No.</label>

                                <div class="col-sm-9">
                                    <input type="text" class="form-control input-sm" name="service_tax_no" id="service_tax_no" placeholder="" value="<?php if(isset($party)){ echo $party[0]['service_tax_no'];} ?>">
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_6">

                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_7">

                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_8">

                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_9">

                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- nav-tabs-custom -->
        </div>
        <!-- /.col -->
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="party_list_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:80%;height: auto;max-height: 100%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Select Party</h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive" style="overflow-x:hidden">
                    <table id="party_list_table" class="table custom-table table-striped">
                        <thead>
                        <tr>
                            <th>Action</th>
                            <th>Party Code</th>
                            <th>Party Name</th>
                            <th>Phone No</th>
                            <th>Email</th>
                            <th>Contact Persons</th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if(!empty($parties)){
                            foreach($parties as $party){
                                ?>
                                <tr>
                                    <td><a href="javascript:void(0);" class="delete_button" data-href="<?=base_url('party/delete/'.$party->party_id);?>"><span class="glyphicon glyphicon-remove"></span></a></td>
                                    <td><a href="javascript:void(0);" class="feed-party-detail" data-party_id="<?=$party->party_id;?>"><?=$party->party_code;?></a></td>
                                    <td><a href="javascript:void(0);" class="feed-party-detail" data-party_id="<?=$party->party_id;?>"><?=$party->party_name;?></a></td>
                                    <td>
                                        <a href="javascript:void(0);" class="feed-party-detail" data-party_id="<?=$party->party_id;?>">
                                            <?php
                                            $phones = explode(",", $party->phone_no);
                                            foreach($phones as $phone){
                                                echo $phone."<br />";
                                            }
                                            ?>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="javascript:void(0);" class="feed-party-detail" data-party_id="<?=$party->party_id;?>">
                                            <?php
                                            $emails = explode(",", $party->email_id);
                                            foreach($emails as $email)
                                            {
                                                echo $email."<br />";
                                            }
                                            ?>

                                        </a>
                                    </td>
                                    <td>
                                        <?php
                                        $contacts = $this->app_model->getPartyContactPersons($party->party_id);
                                        if(count($contacts) > 0)
                                        {
                                            foreach($contacts as $contact)
                                            {
                                                echo '<a href="javascript:void(0);" class="feed-party-detail" data-party_id="<?=$party->party_id;?>">'.$contact->name.'</a><br />';
                                            }
                                        }
                                        ?>
                                    </td>

                                </tr>
                                <?php
                            }
                        }
                        ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Action</th>
                            <th>Party Code</th>
                            <th>Party Name</th>
                            <th>Phone No</th>
                            <th>Email</th>
                            <th>Contact Person</th>

                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<script>
    $(document).ready(function() {
        var availableTags = [
            "ActionScript",
            "AppleScript",
            "Asp",
            "BASIC",
            "C",
            "C++",
            "Clojure",
            "COBOL",
            "ColdFusion",
            "Erlang",
            "Fortran",
            "Groovy",
            "Haskell",
            "Java",
            "JavaScript",
            "Lisp",
            "Perl",
            "PHP",
            "Python",
            "Ruby",
            "Scala",
            "Scheme"
        ];
        $(".contact_person_designation,.contact_person_department,.branch,.party_type_2,.reference,.city, .w_city,.state, .w_state,.country, .w_country.party_type_1").autocomplete({
            minLength: 1,
            source: availableTags
        });
        $('#tab_3').find('input[type="text"],textarea').each(function(){
            $(this).attr('disabled','disabled');
        });
        $(document).on('ifChanged','#address_work',function(){
            if($(this).val() == 1){
                $('#tab_3').find('input[type="text"],textarea').each(function(){
                    $(this).attr('disabled','disabled');
                    $(this).val('');
                });
            }else{
                $('#tab_3').find('input[type="text"],textarea').each(function(){
                    $(this).removeAttr('disabled');
                });
            }
        });

    });
</script>
