<!DOCTYPE html>
<html>
<head>
  <?php include("include/header.php"); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <?php include("include/mainheader.php"); ?>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Aditya</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <?php include("include/menu.php"); ?>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Create Stock Item
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Create Stock Item</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="box-body">
			<?php //include("msg.php"); ?>
			<form action="" method="post" name="create_stock_item" id="create_stock_item">
				<div class="form-group has-feedback">
					<div class="col-xs-12">
						<div class="row">
							<div class="col-xs-2">
								<label>Name</label>
							</div>
							<div class="col-xs-4">
								<input type="text" class="form-control" placeholder="Stock Item Name" name="stock_item_name"/>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-2">
								<label>Description</label>
							</div>
							<div class="col-xs-4">
								<input type="text" class="form-control" placeholder="Description" name="description"/>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-2">
								<label>Remarks</label>
							</div>
							<div class="col-xs-4">
								<input type="text" class="form-control" placeholder="Remarks" name="remarks"/>
							</div>
						</div>
					</div>
					<br/>
					<br/>
					<div class="col-xs-6">
						<div class="row">
							<div class="col-xs-4">
								<label>Under</label>
							</div>
							<div class="col-xs-4">
								<select class="form-control" name="under">
									<option value="">Select</option>
									<option>Capital Account</option>
									<option>Expence Account</option>
									<option>Income Account</option>
								</select>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-4">
								<label>Units</label>
							</div>
							<div class="col-xs-8">
								<div class="radio">
									<label>
										<input type="radio" name="optionsRadios" id="optionsRadios1" value="N">
										Not Applicable
									</label>
									<label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
									<label>
										<input type="radio" name="optionsRadios" id="optionsRadios2" value="Y">
										Applicable
									</label>
								</div>  
							</div>
						</div>
						<div class="row">
							<div class="col-xs-4">
								<label>Set Standard Rates ?</label>
							</div>
							<div class="col-xs-6">
								<div class="radio">
									<label>
										<input type="radio" name="optionsRadios" id="optionsRadios1" value="Y">
										Yes
									</label>
									<label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
									<label>
										<input type="radio" name="optionsRadios" id="optionsRadios2" value="N">
										No
									</label>
								</div>  
							</div>
						</div>
					</div>
					<div class="col-xs-6">
						<div class="row">
							<div class="col-xs-4">
							</div>
							<div class="col-xs-4">
								<label>Tax Information</label>
							</div>
							<div class="col-xs-4">
							</div>
						</div>
						<div class="row">
							<div class="col-xs-6">
								<label>Tarrif Classification</label>
							</div>
							<div class="col-xs-6">
								<textarea class="form-control" name="tarrif_classification" placeholder="Tarrif Classification"></textarea>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-6">
								<label>Rate of Duty (e.g 5)</label>
							</div>
							<div class="col-xs-6">
								<input type="text" class="form-control" placeholder="Rate of Duty (e.g 5)" name="rate_of_duty"/>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-6">
								<label>Rate of VAT %</label>
							</div>
							<div class="col-xs-6">
								<input type="text" class="form-control" placeholder="Rate of VAT %" name="rate_of_vat"/>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-6">
								<label>HSN Code</label>
							</div>
							<div class="col-xs-6">
								<input type="text" class="form-control" placeholder="HSN Code" name="hsn_code"/>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-6">
								<label>Company Name</label>
							</div>
							<div class="col-xs-6">
								<input type="text" class="form-control" placeholder="Company Name" name="company_name"/>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-6">
								<label>Others</label>
							</div>
							<div class="col-xs-6">
								<input type="text" class="form-control" placeholder="Others" name="others"/>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-6">
								<label>Additional Tax Exempted ?</label>
							</div>
							<div class="col-xs-6">
								<div class="radio">
									<label>
										<input type="radio" name="optionsRadios" id="optionsRadios1" value="Y">
										Yes
									</label>
									<label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
									<label>
										<input type="radio" name="optionsRadios" id="optionsRadios2" value="N">
										No
									</label>
								</div>  
							</div>
						</div>
					</div>
					<div class="col-xs-12">
						<div class="row">
							<div class="col-xs-4">
							</div>
							<div class="col-xs-2">
								<label>Qty</label>
							</div>
							<div class="col-xs-2">
								<label>Rate</label>
							</div>
							<div class="col-xs-2">
								<label>Per</label>
							</div>
							<div class="col-xs-2">
								<label>Value</label>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-4">
								<label>Opening Balance</label>
							</div>
							<div class="col-xs-2">
								
							</div>
							<div class="col-xs-2">
								
							</div>
							<div class="col-xs-2">
								
							</div>
							<div class="col-xs-2">
								
							</div>
						</div>
						<div class="row">
							<div class="col-xs-2">
								<button type="submit" class="btn btn-primary btn-block btn-flat" name="ok">Submit</button>
							</div>
							<div class="col-xs-2">
								<button type="reset" class="btn btn-primary btn-block btn-flat" name="reset" onClick="document.location.href='createGropu.php'"/>Reset</button>
							</div>
							<!-- /.col -->
						</div>
					</div>
				</div>	
			</form>
		</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <?php include("include/footer.php"); ?>
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
	<?php include("include/filelinks.php"); ?>
</body>
</html>
