<!DOCTYPE html>
<html>
<head>
  <?php include("include/header.php"); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <?php include("include/mainheader.php"); ?>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Aditya</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <?php include("include/menu.php"); ?>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Metal Balance Sheet
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Metal Balance Sheet</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="box-body">
			<?php //include("msg.php"); ?>
			<form action="" method="post" name="create_ledger" id="create_ledger">
				<div class="form-group has-feedback">
					<div class="col-xs-6">
						<div class="row">
							<div class="col-xs-4">
								<label>Pariculars</label>
							</div>
							<div class="col-xs-2">
								
							</div>
							<div class="col-xs-6" align="center">
								<label>Aditya JaweLLers</label></br>
								<label>From <?php echo date("d-M-Y"); ?> To <?php echo date("d-M-Y"); ?></label>
							</div>
						</div>
						<br/>
						<br/>
						<br/>
						<div class="row">
							<div class="col-xs-4">
								<label>Total</label>
							</div>
							<div class="col-xs-2">
								
							</div>
							<div class="col-xs-6">
								<label>Amount</label></br>
							</div>
						</div>
					</div>	
					<div class="col-xs-6">
						<div class="row">
							<div class="col-xs-4">
								<label>Pariculars</label>
							</div>
							<div class="col-xs-2">
								
							</div>
							<div class="col-xs-6" align="center">
								<label>Aditya JaweLLers</label></br>
								<label>From <?php echo date("d-M-Y"); ?> To <?php echo date("d-M-Y"); ?></label>
							</div>
						</div>
						<br/>
						<br/>
						<br/>
						<div class="row">
							<div class="col-xs-4">
								<label>Total</label>
							</div>
							<div class="col-xs-2">
								
							</div>
							<div class="col-xs-6">
								<label>Amount</label></br>
							</div>
						</div>
					</div>
					<div class="col-xs-12">
						<div class="row">
							<div class="col-xs-2">
								<button type="submit" class="btn btn-primary btn-block btn-flat" name="ok">Submit</button>
							</div>
							<div class="col-xs-2">
								<button type="reset" class="btn btn-primary btn-block btn-flat" name="reset" onClick="document.location.href='createGropu.php'"/>Reset</button>
							</div>
							<!-- /.col -->
						</div>
					</div>
				</div>
			</form>
		</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <?php include("include/footer.php"); ?>
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
	<?php include("include/filelinks.php"); ?>
</body>
</html>
