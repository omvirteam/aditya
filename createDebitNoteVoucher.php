<!DOCTYPE html>
<html>
<head>
  <?php include("include/header.php"); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <?php include("include/mainheader.php"); ?>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Aditya</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <?php include("include/menu.php"); ?>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Create Debit Note Voucher
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Create Debit Note Voucher</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="box-body">
			<div class="col-xs-12">
				<?php //include("msg.php"); ?>
				<form action="" method="post" name="create_debit_note_voucher" id="create_debit_note_voucher">
					<div class="form-group has-feedback">
						<div class="row">
							<div class="col-xs-2">
								<label>Debit Note</label>
							</div>
							<div class="col-xs-2">
								<label>No.</label>
							</div>
							<div class="col-xs-4">
							</div>
							<div class="col-xs-2">
								<label><?php echo date("d-M-Y"); ?></label>
							</div>
							<div class="col-xs-2">
								<label><?php echo date("l"); ?></label>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-2">
								<label>Ref</label>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-2">
								<label>Party'S Account Name</label>
							</div>
							<div class="col-xs-4">
								<select class="form-control" name="account_from">
									<option value="">Select</option>
								</select>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-2">
								<label>Current Balance</label>
							</div>
							<div class="col-xs-4">
								<label></label>
							</div>
						</div>
						<br/>
						<br/>
						<div class="row">
							<div class="col-xs-3">
								<label>Name of Item</label>
							</div>
							<div class="col-xs-2">
								<label>Quantity</label>
							</div>
							<div class="col-xs-2">
								<label>Rate</label>
							</div>
							<div class="col-xs-2">
								<label>Per</label>
							</div>
							<div class="col-xs-1">
								<label>Dis %</label>
							</div>
							<div class="col-xs-2">
								<label>Amount</label>
							</div>
						</div>
						<br/>
						<br/>
						<div class="row">
							<div class="col-xs-3">
								<select class="form-control" name="account_to">
									<option value="">Select</option>
								</select>
							</div>
							<div class="col-xs-2">
								<label>Quantity</label>
							</div>
							<div class="col-xs-2">
								<label>Rate</label>
							</div>
							<div class="col-xs-2">
								<label>Per</label>
							</div>
							<div class="col-xs-1">
								<label>Dis %</label>
							</div>
							<div class="col-xs-2">
								<label>Amount</label>
							</div>
						</div>
						</br>
						</br>
						<div class="row">
							<div class="col-xs-6">
								<label>Show Statutory Details ? </label>
							</div>
							<div class="col-xs-6">
								<div class="radio">
									<label>
										<input type="radio" name="optionsRadios" id="optionsRadios1" value="Y">
										Yes
									</label>
									<label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
									<label>
										<input type="radio" name="optionsRadios" id="optionsRadios2" value="N">
										No
									</label>
								</div>  
							</div>
						</div>
						</br>
						</br>
						<div class="row">
							<div class="col-xs-3">
								<label>Narration</label>
							</div>
							<div class="col-xs-2">
								<label>Total</label>
							</div>
							<div class="col-xs-5">
							</div>
							<div class="col-xs-2">
								<label>Total</label>
							</div>							
						</div>
						<div class="row">
							<div class="col-xs-2">
								<textarea name="narration"></textarea>
							</div>
						</div>
					</div>
					<div class="row">
							<div class="col-xs-2">
								<button type="submit" class="btn btn-primary btn-block btn-flat" name="ok">Submit</button>
							</div>
							<div class="col-xs-2">
								<button type="reset" class="btn btn-primary btn-block btn-flat" name="reset" onClick="document.location.href='createGropu.php'"/>Reset</button>
							</div>
							<!-- /.col -->
					</div>
				</form>
			</div>
		</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <?php include("include/footer.php"); ?>
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
	<?php include("include/filelinks.php"); ?>
</body>
</html>
