<?php include("include/config.php"); ?>
<?php 
	if(isset($_POST['action']) && $_POST['action'] == 'update'){
		$order_id = $_POST['order_id'];
		$order_date = date('Y-m-d',strtotime($_POST['order_date']));
		$query = "UPDATE `orders` SET 
					`voucher_type_id`='{$_POST['voucher_type_id']}',
					`order_no`='{$_POST['order_no']}',
					`order_date`='{$order_date}',
					`name`='{$_POST['name']}',
					`address`='{$_POST['address']}',
					`city`='{$_POST['city']}',
					`mobile`='{$_POST['mobile']}',
					`area_id`='{$_POST['area_id']}',
					`references_id`='{$_POST['references_id']}',
					`price_value_id`='{$_POST['price_value_id']}',
					`total_advance`='{$_POST['total_advance']}'
				WHERE `id` = '{$order_id}'";
		mysqli_query($con,$query);
		$i = 0;
		foreach($_POST['total'] as $row){
			if(isset($_POST['order_detail_id'][$i])){
				if($_POST['total'][$i] != '' && $_POST['total'][$i] != null){
					$query = "UPDATE `order_details` SET
							`tag_no`='{$_POST['tag_no'][$i]}',
							`item`='{$_POST['item'][$i]}',
							`size`='{$_POST['size'][$i]}',
							`pcs`='{$_POST['pcs'][$i]}',
							`stone_wt`='{$_POST['stone_wt'][$i]}',
							`order_wt`='{$_POST['order_wt'][$i]}',
							`price_per_gram`='{$_POST['price_per_gram'][$i]}',
							`metal_amount`='{$_POST['metal_amount'][$i]}',
							`labour_per`='{$_POST['labour_per'][$i]}',
							`discount_per`='{$_POST['discount_per'][$i]}',
							`labour_gram`='{$_POST['labour_gram'][$i]}',
							`discount_rs`='{$_POST['discount_rs'][$i]}',
							`labour_amt`='{$_POST['labour_amt'][$i]}',
							`charges`='{$_POST['charges'][$i]}',
							`mrp`='{$_POST['mrp'][$i]}',
							`vat`='{$_POST['vat'][$i]}',
							`total`='{$_POST['total'][$i]}'
						WHERE `id` = '{$_POST['order_detail_id'][$i]}'";
					mysqli_query($con,$query);
				}
			}else{
				if($_POST['total'][$i] != '' && $_POST['total'][$i] != null){
					$query = "INSERT INTO `order_details`
					(`order_id`, `tag_no`, `item`, `size`, `pcs`, `stone_wt`, `order_wt`, `price_per_gram`, `metal_amount`,
					`labour_per`, `discount_per`, `labour_gram`, `discount_rs`, `labour_amt`, `charges`, `mrp`, `vat`, `total`) VALUES
					('{$order_id}','{$_POST['tag_no'][$i]}','{$_POST['item'][$i]}','{$_POST['size'][$i]}','{$_POST['pcs'][$i]}','{$_POST['stone_wt'][$i]}'
					,'{$_POST['order_wt'][$i]}','{$_POST['price_per_gram'][$i]}','{$_POST['metal_amount'][$i]}','{$_POST['labour_per'][$i]}','{$_POST['discount_per'][$i]}'
					,'{$_POST['labour_gram'][$i]}','{$_POST['discount_rs'][$i]}','{$_POST['labour_amt'][$i]}','{$_POST['charges'][$i]}','{$_POST['mrp'][$i]}','{$_POST['vat'][$i]}','{$_POST['total'][$i]}')";
					mysqli_query($con,$query);
				}	
			}
			$i++;
		}
	}
	else if(isset($_POST['order_submit']))
	{
		$order_date = date('Y-m-d',strtotime($_POST['order_date']));
		$query = "INSERT INTO `orders`
			(`voucher_type_id`, `order_no`, `order_date`, `name`, `address`, `city`, `mobile`, `area_id`, `references_id`, `price_value_id`, `total_advance`) VALUES
			('{$_POST['voucher_type_id']}','{$_POST['order_no']}','{$order_date}','{$_POST['name']}','{$_POST['address']}',
			'{$_POST['city']}','{$_POST['mobile']}','{$_POST['area_id']}','{$_POST['references_id']}','{$_POST['price_value_id']}','{$_POST['total_advance']}')";
		mysqli_query($con,$query);
		$order_id = mysqli_insert_id($con);
		
		$i = 0;
		foreach($_POST['total'] as $row){
			if($_POST['total'][$i] != '' && $_POST['total'][$i] != null){
				$query = "INSERT INTO `order_details`
						(`order_id`, `tag_no`, `item`, `size`, `pcs`, `stone_wt`, `order_wt`, `price_per_gram`, `metal_amount`, 
						`labour_per`, `discount_per`, `labour_gram`, `discount_rs`, `labour_amt`, `charges`, `mrp`, `vat`, `total`) VALUES
						('{$order_id}','{$_POST['tag_no'][$i]}','{$_POST['item'][$i]}','{$_POST['size'][$i]}','{$_POST['pcs'][$i]}','{$_POST['stone_wt'][$i]}'
						,'{$_POST['order_wt'][$i]}','{$_POST['price_per_gram'][$i]}','{$_POST['metal_amount'][$i]}','{$_POST['labour_per'][$i]}','{$_POST['discount_per'][$i]}'
						,'{$_POST['labour_gram'][$i]}','{$_POST['discount_rs'][$i]}','{$_POST['labour_amt'][$i]}','{$_POST['charges'][$i]}','{$_POST['mrp'][$i]}','{$_POST['vat'][$i]}','{$_POST['total'][$i]}')";
				mysqli_query($con,$query);
			}	
			$i++;
		}
	}
	header('location:create_sales_order_entry.php');
?>