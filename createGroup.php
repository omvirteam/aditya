<!DOCTYPE html>
<html>
<head>
  <?php include("include/header.php"); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <?php include("include/mainheader.php"); ?>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Aditya</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <?php include("include/menu.php"); ?>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1 align="center">
        Create Group
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Create Group</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="box-body">
			<div class="col-xs-12">
				<?php //include("msg.php"); ?>
				<form action="" method="post" name="create_group" id="create_group">
					<div class="form-group has-feedback">
						<div class="row">
							<div class="col-xs-2">
								<label>Create Group</label>
							</div>
							<div class="col-xs-4">
								<input type="text" class="form-control" placeholder="Group Name" name="group_name"/>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-2">
								<label>Alias Name</label>
							</div>
							<div class="col-xs-4">
								<input type="text" class="form-control" placeholder="Group Alias Name" name="group_alias_name"/>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-2">
								<label>Under Group</label>
							</div>
							<div class="col-xs-4">
								<select class="form-control" name="under">
									<option value="">Select</option>
									<option>Current Assets</option>
									<option>Laibilities</option>
								</select>
							</div>
						</div>
						<br/>
						<div class="row">
							<div class="col-xs-2">
								<label>Its a Primary Group</label>
							</div>
							<div class="col-xs-4">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="primary_group_yes" id="primary_group_yes" value="Yes">
										Yes
									</label>
								</div>  
							</div>
						</div>
						<div class="row">
							<div class="col-xs-2">
								<label>Type of Group</label>
							</div>
							<div class="col-xs-4">
								<select class="form-control" name="under">
									<option value="">Select</option>
									<option>Current Assets</option>
									<option>Laibilities</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
							<div class="col-xs-2">
								<button type="submit" class="btn btn-primary btn-block btn-flat" name="ok">Submit</button>
							</div>
							<div class="col-xs-2">
								<button type="reset" class="btn btn-primary btn-block btn-flat" name="reset" onClick="document.location.href='createGroup.php'"/>Reset</button>
							</div>
							<!-- /.col -->
					</div>
				</form>
			</div>
		</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <?php include("include/footer.php"); ?>
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
	<?php include("include/filelinks.php"); ?>
</body>
</html>
