<?php include("include/config.php"); 
	/* $flag = 0;
	$order_row = array();
	if(isset($_GET['id']))
	{
		$id=$_GET['id'];
		$flag = 1;
		$order_query = "SELECT * FROM `orders` WHERE `id` = '{$id}' LIMIT 1";
		$order_result = mysqli_query($con,$order_query);
		$order_row = mysqli_fetch_object($order_result);
		$order_detail_query = "SELECT * FROM `order_details` WHERE `order_id` = '{$order_row->id}'";
		$order_detail_result = mysqli_query($con,$order_detail_query);
	}else{
		$order_query = "SELECT * FROM `orders` LIMIT 1";
		$order_result = mysqli_query($con,$order_query);
		if(mysqli_num_rows($order_result) > 0){
			$flag = 1;
			$order_row = mysqli_fetch_object($order_result);
			$order_detail_query = "SELECT * FROM `order_details` WHERE `order_id` = '{$order_row->id}'";
			$order_detail_result = mysqli_query($con,$order_detail_query);
		}
	} */
?>

<!DOCTYPE html>
<html>
<head>
  <?php include("include/header.php"); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <?php include("include/mainheader.php"); ?>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Aditya</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <?php include("include/menu.php"); ?>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Create Sales Order</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content custom-content">
		<form action="create_sales_order_entry_db.php" method="post" name="create_group" id="create_group">
			<?php //if($flag == 1){?>
			<!-- <input type="hidden" name="action" value="update"/>
					<input type="hidden" name="order_id" value="<?//=$order_row->id?>" />
					<input type="hidden" name="total_advance" id="total_advance" value="<?//=$order_row->total_advance?>"/> -->
			<?php //} ?>
			<input type="hidden" name="total_advance" id="total_advance"/>
			<div class="container">
				<h2>Create Sales Order</h2>
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#home">Sales Order</a></li>
					<li><a data-toggle="tab" href="#upload-photo">Photo Upload</a></li>
					<li><a data-toggle="tab" href="#advance-received">Advance Received</a></li>
					<li><a data-toggle="tab" href="#advance-metal">Advance Metal</a></li>
					<li><a data-toggle="tab" href="#material-shample">Material & Shample</a></li>
				</ul>

				<div class="custom tab-content">
					<div id="home" class="tab-pane fade in active">
						<h3>Sales Order</h3>
						<div class="col-xs-12 no-padding">
							<div class="box no-margin">
								<div class="box-body">
									<div class="col-md-4">
										<div class="col-md-5">
											<label>Voucher Type</label>
										</div>
										<div class="col-md-7">
											<select class="form-control" name="voucher_type_id">
												<option value="">Select</option>
												<?php $sSQL = "SELECT id,type FROM voucher_type ORDER BY id"; $rs = mysqli_query($con,$sSQL) or print(mysqli_error());
												while($row = mysqli_fetch_array($rs)){
													?>
													<option value=<?=$row["id"]?>><?=$row["type"]?></option>
													<?php
												}?>
											</select>
										</div>
									</div>
									<div class="col-md-4">
										<div class="col-md-4">
											<label>Order No.</label>
										</div>
										<div class="col-md-6">
											<input type="text" class="form-control" placeholder="" name="order_no" />
										</div>
									</div>
									<div class="col-md-4">
										<div class="row">
											<div class="col-md-4">
												<label>Order Date</label>
											</div>
											<div class="col-md-6">
												<input type="text" class="form-control" placeholder="DD-MM-YYYY" name="order_date" />
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 no-padding">
							<div class="box no-margin">
								<div class="box-body">
									<div class="col-md-6">
										<div class="col-md-4">
											<label>Customer Name</label>
										</div>
										<div class="col-xs-7">
											<input type="text" class="form-control" placeholder="" name="name"/>
										</div>
									</div>
									<div class="col-md-6">
										<div class="col-md-4">
											<label>Address</label>
										</div>
										<div class="col-xs-7">
											<input type="text" class="form-control" placeholder="" name="address" />
										</div>
									</div>
									<div class="clearfix margin-bottom-10px"></div>
									<div class="col-md-6">
										<div class="col-md-4">
											<label>Mobile</label>
										</div>
										<div class="col-xs-7">
											<input type="text" class="form-control" placeholder="" name="mobile"/>
										</div>
									</div>
									<div class="col-md-6">
										<div class="col-md-4">
											<label>Area</label>
										</div>
										<div class="col-xs-7">
											<select class="form-control" name="area_id">
												<option value="">Select</option>
												<?php $sSQL = "SELECT id,name FROM area ORDER BY id"; $rs = mysqli_query($con,$sSQL) or print(mysqli_error());
												while($row = mysqli_fetch_array($rs)){
													?>
													<option value="<?=$row["id"]?>"><?=$row["name"]?></option>
												<?php }?>
											</select>
										</div>
									</div>
									<div class="clearfix margin-bottom-10px"></div>
									<div class="col-md-6">
										<div class="col-md-4">
											<label>City</label>
										</div>
										<div class="col-xs-7">
											<input type="text" class="form-control" placeholder="" name="city" />
										</div>
									</div>
									<div class="col-md-6">
										<div class="col-md-4">
											<label>References</label>
										</div>
										<div class="col-xs-7">
											<select class="form-control" name="references_id">
												<option value="">Select</option>
												<?php $sSQL = "SELECT * FROM `references`"; $rs = mysqli_query($con,$sSQL) or print(mysqli_error());
												while($row = mysqli_fetch_array($rs)){
													?>
													<option value="<?=$row["id"]?>"><?=$row["name"]?></option>
												<?php }?>
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 no-padding">
							<div class="box no-margin">
								<div class="box-body text-center">
									<div class="col-xs-4">
									</div>
									<div class="col-xs-4">
										<select class="form-control" name="price_value_id">
											<option value="">Select</option>
											<?php $sSQL = "SELECT * FROM `price_value`"; $rs = mysqli_query($con,$sSQL) or print(mysqli_error());
											while($row = mysqli_fetch_array($rs)){
												?>
												<option value="<?=$row["id"]?>"><?=$row["value"]?></option>
											<?php }?>
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 no-padding">
							<div class="box">
								<div class="box-body table-responsive  margin-bottom-10px">
									<table id="psList" class="table custom table-bordered table-hover">
										<thead>
										<tr>
											<th>Sr No.</th>
											<th class="width-50px">Tag No.</th>
											<th>Item</th>
											<th class="width-50px">Size</th>
											<th class="width-50px">Pcs.</th>
											<th class="width-50px">Stone Wt.</th>
											<th class="width-50px">Order Wt.</th>
											<th class="width-100px">Price Per 1 Gram</th>
											<th class="width-100px">Metal Amount</th>
											<th class="width-50px">Labour In %</th>
											<th class="width-50px">Discount %</th>
											<th class="width-100px">Labour / Gram</th>
											<th class="width-100px">Discount Rs.</th>
											<th class="width-100px">Labour Amt.</th>
											<th class="width-100px">Other Charges</th>
											<th class="width-100px">MRP</th>
											<th class="width-50px">Vat %</th>
											<th class="width-100px">Total</th>
										</tr>
										</thead>
										<tbody>
										<?php
										//if($flag == 1){
										//	$j = 1;
										//	while ($orderDetailRow = mysqli_fetch_object($order_detail_result)){
										?>
										<!--
								<tr id="1" class="repeat" data-row="1">
									<td align="center" class="sno">
										<?//=$j;$j++;?>
										<input type="hidden" name="order_detail_id[]" value="<?//=$orderDetailRow->id?>"/>
									</td>
									<td>
										<input type="text" name="tag_no[]" id="tag_no" dataIndex="1" class="form-control" value="<?//=$orderDetailRow->tag_no;?>">
									</td>
									<td>
										<input type="text" name="item[]" id="item" dataIndex="1" class="form-control" value="<?//=$orderDetailRow->item;?>">
									</td>
									<td>
										<input type="text" name="size[]" id="size" dataIndex="1" class="form-control" value="<?//=$orderDetailRow->size;?>">
									</td>
									<td>
										<input type="text" name="pcs[]" id="pcs" dataIndex="1" class="form-control" value="<?//=$orderDetailRow->pcs;?>">
									</td>
									<td>
										<input type="text" name="stone_wt[]" id="stone_wt" dataIndex="1" class="form-control" value="<?//=$orderDetailRow->stone_wt;?>">
									</td>
									<td>
										<input type="text" name="order_wt[]" id="order_wt" dataIndex="1" class="form-control metal_amount_box labour_amt_box" value="<?//=$orderDetailRow->order_wt;?>">
									</td>
									<td>
										<input type="text" name="price_per_gram[]" id="price_per_gram" dataIndex="1" class="form-control metal_amount_box labour_gram_box" value="<?//=$orderDetailRow->price_per_gram;?>">
									</td>
									<td>
										<input type="text" name="metal_amount[]" id="metal_amount" dataIndex="1" class="form-control discount_rs_box mrp_box" value="<?//=$orderDetailRow->metal_amount;?>">
									</td>
									<td>
										<input type="text" name="labour_per[]" id="labour_per" dataIndex="1" class="custom-control labour_gram_box" value="<?//=$orderDetailRow->labour_per;?>"><span> %</span>
									</td>
									<td>
										<input type="text" name="discount_per[]" id="discount_per" dataIndex="1" class="custom-control discount_rs_box" value="<?//=$orderDetailRow->discount_per;?>"><span> %</span>
									</td>
									<td>
										<input type="text" name="labour_gram[]" id="labour_gram" dataIndex="1" class="form-control labour_amt_box" value="<?//=$orderDetailRow->labour_gram;?>">
									</td>
									<td>
										<input type="text" name="discount_rs[]" id="discount_rs" dataIndex="1" class="form-control mrp_box" value="<?//=$orderDetailRow->discount_rs;?>">
									</td>
									<td>
										<input type="text" name="labour_amt[]" id="labour_amt" dataIndex="1" class="form-control mrp_box" value="<?//=$orderDetailRow->labour_amt;?>">
									</td>
									<td>
										<input type="text" name="charges[]" id="charges" dataIndex="1" class="form-control mrp_box" value="<?//=$orderDetailRow->charges;?>">
									</td>
									<td>
										<input type="text" name="mrp[]" id="mrp" dataIndex="1" class="form-control total_box" value="<?//=$orderDetailRow->mrp;?>">
									</td>
									<td>
										<input type="text" name="vat[]" id="vat" dataIndex="1" class="form-control total_box" value="<?//=$orderDetailRow->vat;?>">
									</td>
									<td>
										<input type="text" name="total[]" id="total" dataIndex="1" class="form-control row_total" size="50" value="<?//=$orderDetailRow->total;?>">
									</td>
								</tr>  -->
										<?php
										//	}
										//}else{
										?>
										<tr id="1" class="repeat" data-row="1">
											<td align="center" class="sno">
												1
											</td>
											<td>
												<input type="text" name="tag_no[]" id="tag_no" dataIndex="1" class="form-control">
											</td>
											<td>
												<input type="text" name="item[]" id="item" dataIndex="1" class="">
											</td>
											<td>
												<input type="text" name="size[]" id="size" dataIndex="1" class="form-control">
											</td>
											<td>
												<input type="text" name="pcs[]" id="pcs" dataIndex="1" class="form-control onlynum ">
											</td>
											<td>
												<input type="text" name="stone_wt[]" id="stone_wt" dataIndex="1" class="form-control">
											</td>
											<td>
												<input type="text" name="order_wt[]" id="order_wt" dataIndex="1" class="form-control metal_amount_box labour_amt_box">
											</td>
											<td>
												<input type="text" name="price_per_gram[]" id="price_per_gram" dataIndex="1" class="form-control metal_amount_box labour_gram_box">
											</td>
											<td>
												<input type="text" name="metal_amount[]" id="metal_amount" dataIndex="1" class="form-control discount_rs_box mrp_box">
											</td>
											<td>
												<input type="text" name="labour_per[]" id="labour_per" dataIndex="1" class="form-control labour_gram_box">
											</td>
											<td>
												<input type="text" name="discount_per[]" id="discount_per" dataIndex="1" class="form-control discount_rs_box">
											</td>
											<td>
												<input type="text" name="labour_gram[]" id="labour_gram" dataIndex="1" class="form-control labour_amt_box">
											</td>
											<td>
												<input type="text" name="discount_rs[]" id="discount_rs" dataIndex="1" class="form-control mrp_box">
											</td>
											<td>
												<input type="text" name="labour_amt[]" id="labour_amt" dataIndex="1" class="form-control mrp_box">
											</td>
											<td>
												<input type="text" name="charges[]" id="charges" dataIndex="1" class="form-control mrp_box">
											</td>
											<td>
												<input type="text" name="mrp[]" id="mrp" dataIndex="1" class="form-control total_box">
											</td>
											<td>
												<input type="text" name="vat[]" id="vat" dataIndex="1" class="form-control total_box">
											</td>
											<td>
												<input type="text" name="total[]" id="total" dataIndex="1" size="50" class="form-control row_total">
											</td>
										</tr>
										<?php
										//	}
										?>
										<tr id="1" class="hidden" data-row="1">
											<td align="center" class="sno">
												2
											</td>
											<td>
												<input type="text" name="tag_no[]" id="tag_no" dataIndex="1" class="form-control">
											</td>
											<td>
												<input type="text" name="item[]" id="item" dataIndex="1" class="form-control">
											</td>
											<td>
												<input type="text" name="size[]" id="size" dataIndex="1" class="form-control">
											</td>
											<td>
												<input type="text" name="pcs[]" id="pcs" dataIndex="1" class="form-control onlynum ">
											</td>
											<td>
												<input type="text" name="stone_wt[]" id="stone_wt" dataIndex="1" class="form-control">
											</td>
											<td>
												<input type="text" name="order_wt[]" id="order_wt" dataIndex="1" class="form-control metal_amount_box labour_amt_box">
											</td>
											<td>
												<input type="text" name="price_per_gram[]" id="price_per_gram" dataIndex="1" class="form-control metal_amount_box labour_gram_box">
											</td>
											<td>
												<input type="text" name="metal_amount[]" id="metal_amount" dataIndex="1" class="form-control discount_rs_box mrp_box">
											</td>
											<td>
												<input type="text" name="labour_per[]" id="labour_per" dataIndex="1" class="form-control labour_gram_box">
											</td>
											<td>
												<input type="text" name="discount_per[]" id="discount_per" dataIndex="1" class="form-control discount_rs_box">
											</td>
											<td>
												<input type="text" name="labour_gram[]" id="labour_gram" dataIndex="1" class="form-control labour_amt_box">
											</td>
											<td>
												<input type="text" name="discount_rs[]" id="discount_rs" dataIndex="1" class="form-control mrp_box">
											</td>
											<td>
												<input type="text" name="labour_amt[]" id="labour_amt" dataIndex="1" class="form-control mrp_box">
											</td>
											<td>
												<input type="text" name="charges[]" id="charges" dataIndex="1" class="form-control mrp_box">
											</td>
											<td>
												<input type="text" name="mrp[]" id="mrp" dataIndex="1" class="form-control total_box">
											</td>
											<td>
												<input type="text" name="vat[]" id="vat" dataIndex="1" class="form-control total_box">
											</td>
											<td>
												<input type="text" name="total[]" id="total" dataIndex="1" size="50" class="form-control row_total">
											</td>
										</tr>
										</tbody>
										<tfoot>
										<tr>
											<th class="no_total text-center" colspan="3">Total</th>
											<th></th>
											<th></th>
											<th align="right" style="text-align: right" id="tQty"></th>
											<th></th>
											<th></th>
											<th align="right" style="text-align: right" id="tAmount"></th>
											<th align="right" style="text-align: right" id="tWeight"></th>
											<th></th>
											<th></th>
											<th class="discount_amt"></th>
											<th></th>
											<th></th>
											<th class="vat_tax_amt vat_tax_amt_mrp"></th>
											<th class="vat_tax_amt vat_tax_amt_vat"></th>
											<th class="total_amt"></th>
										</tr>
										</tfoot>
									</table>
								</div>
								<div class="col-xs-12">
									<div class="col-xs-6" style="float: right;">
										<div class="box">
											<div class="box-body table-responsive">
												<table class="table table-bordered" id="table-total">
													<thead>
													<tr>
														<th style="width: 200px!important;">Total</th>
														<td class="total_amt"></td>
													</tr>
													<tr>
														<th>Discount On Labour(in % and in amount)</th>
														<td class="discount_amt"></td>
													</tr>
													<tr>
														<th>Vat Tax</th>
														<td class="vat_tax_amt"></td>
													</tr>
													<tr>
														<th>Total Advance</th>
														<td class="advance_amt no-padding"><input type="text" name="advance_amt" id="advance_amt" class="form-control"></td>
													</tr>
													<tr>
														<th>Approx Pending Amt</th>
														<td class="pending_amt"></td>
													</tr>
													</thead>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- /.box -->
						</div>
					</div>
					<div id="upload-photo" class="tab-pane fade">
						<h3>Upload Photo</h3>
						<div class="col-xs-12">
							<div class="col-xs-3">
								<div class="thumbnail image_card bg-info">
									<div class="custom thumb">
										<img src="dist/img/avatar5.png" alt="">
									</div>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="thumbnail image_card bg-info">
									<div class="custom thumb">
										<img src="dist/img/avatar2.png" alt="">
									</div>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="thumbnail image_card bg-info">
									<div class="custom thumb">
										<img src="dist/img/avatar04.png" alt="">
									</div>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="thumbnail image_card bg-info" style="height: 239px;">
									<div style="background-color: #c7c7c7;height: 100%;width: 100%;display: inline-block;">
										<div class="custom thumb" style="margin-top: 46%;margin-left: 39px;">
											<input type="file">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div id="advance-received" class="tab-pane fade">
						<h3>Advance received from customer</h3>
						<div class="col-xs-12 no-padding">
							<div class="box">
								<div class="box-body table-responsive  margin-bottom-10px">
									<table class="table custom table-bordered table-hover">
										<thead>
											<tr class="thead">
												<th>Sr No</th>
												<th>Voucher number</th>
												<th>Voucher type</th>
												<th>Amount</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>1</td>
												<td><input type="text" name="test" id="tag_no" value="1500" class="form-control"></td>
												<td>
													<select class="form-control" name="voucher_type_id">
														<option value="">Select</option>
														<?php $sSQL = "SELECT id,type FROM voucher_type ORDER BY id"; $rs = mysqli_query($con,$sSQL) or print(mysqli_error());
														while($row = mysqli_fetch_array($rs)){
															?>
															<option value=<?=$row["id"]?> selected="selected"><?=$row["type"]?></option>
															<?php
														}?>
													</select>
												</td>
												<td><input type="text" name="test" id="tag_no" value="250000.00" class="form-control"></td>
											</tr>
											<tr>
												<td>2</td>
												<td><input type="text" name="test" id="tag_no" value="1000" class="form-control"></td>
												<td>
													<select class="form-control" name="voucher_type_id">
														<option value="">Select</option>
														<?php $sSQL = "SELECT id,type FROM voucher_type ORDER BY id"; $rs = mysqli_query($con,$sSQL) or print(mysqli_error());
														while($row = mysqli_fetch_array($rs)){
															?>
															<option value=<?=$row["id"]?>  selected="selected"><?=$row["type"]?></option>
															<?php
														}?>
													</select>
												</td>
												<td><input type="text" name="test" id="tag_no" value="25000.00" class="form-control"></td>
											</tr>
											<tr>
												<td>3</td>
												<td><input type="text" name="test" id="tag_no" value="" class="form-control"></td>
												<td>
													<select class="form-control" name="voucher_type_id">
														<option value="">Select</option>
														<?php $sSQL = "SELECT id,type FROM voucher_type ORDER BY id"; $rs = mysqli_query($con,$sSQL) or print(mysqli_error());
														while($row = mysqli_fetch_array($rs)){
															?>
															<option value=<?=$row["id"]?>><?=$row["type"]?></option>
															<?php
														}?>
													</select>
												</td>
												<td><input type="text" name="test" id="tag_no" value="" class="form-control"></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div id="advance-metal" class="tab-pane fade">
						<h3>Advance metal received from customer</h3>
						<div class="col-xs-12 no-padding">
							<div class="box">
								<div class="box-body table-responsive  margin-bottom-10px">
									<table class="table custom table-bordered table-hover">
										<thead>
										<tr class="thead">
											<th>Sr No</th>
											<th>Old metal</th>
											<th>Item</th>
											<th>Gr. Wt.</th>
											<th>Net. Wt.</th>
											<th>Touch</th>
											<th>Fine</th>
											<th>Unit</th>
											<th>Rate/Gram or 10</th>
											<th>Total</th>
										</tr>
										</thead>
										<tbody>
										<tr>
											<td>1</td>
											<td><input type="text" name="test" id="tag_no" value="Old Gold" class="form-control"></td>
											<td><input type="text" name="test" id="tag_no" value="Butti" class="form-control"></td>
											<td><input type="text" name="test" id="tag_no" value="10.75" class="form-control"></td>
											<td><input type="text" name="test" id="tag_no" value="10.25" class="form-control"></td>
											<td><input type="text" name="test" id="tag_no" value="91.00" class="form-control"></td>
											<td><input type="text" name="test" id="tag_no" value="09.00" class="form-control"></td>
											<td><input type="text" name="test" id="tag_no" value="gm" class="form-control"></td>
											<td><input type="text" name="test" id="tag_no" value="1010.00" class="form-control"></td>
											<td><input type="text" name="test" id="tag_no" value="10000.00" class="form-control"></td>
										</tr>
										<tr>
											<td>2</td>
											<td><input type="text" name="test" id="tag_no" value="Old Gold" class="form-control"></td>
											<td><input type="text" name="test" id="tag_no" value="Butti" class="form-control"></td>
											<td><input type="text" name="test" id="tag_no" value="10.75" class="form-control"></td>
											<td><input type="text" name="test" id="tag_no" value="10.25" class="form-control"></td>
											<td><input type="text" name="test" id="tag_no" value="91.00" class="form-control"></td>
											<td><input type="text" name="test" id="tag_no" value="09.00" class="form-control"></td>
											<td><input type="text" name="test" id="tag_no" value="gm" class="form-control"></td>
											<td><input type="text" name="test" id="tag_no" value="1010.00" class="form-control"></td>
											<td><input type="text" name="test" id="tag_no" value="10000.00" class="form-control"></td>
										</tr>
										<tr>
											<td>3</td>
											<td><input type="text" name="test" id="tag_no" value="" class="form-control"></td>
											<td><input type="text" name="test" id="tag_no" value="" class="form-control"></td>
											<td><input type="text" name="test" id="tag_no" value="" class="form-control"></td>
											<td><input type="text" name="test" id="tag_no" value="" class="form-control"></td>
											<td><input type="text" name="test" id="tag_no" value="" class="form-control"></td>
											<td><input type="text" name="test" id="tag_no" value="" class="form-control"></td>
											<td><input type="text" name="test" id="tag_no" value="" class="form-control"></td>
											<td><input type="text" name="test" id="tag_no" value="" class="form-control"></td>
											<td><input type="text" name="test" id="tag_no" value="" class="form-control"></td>
										</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div id="material-shample" class="tab-pane fade">
						<h3>Advance material and shample detail from customer</h3>
						<div class="col-xs-12 no-padding">
							<div class="box">
								<div class="box-body table-responsive  margin-bottom-10px">
									<table class="table custom table-bordered table-hover">
										<thead>
										<tr class="thead">
											<th>Sr No</th>
											<th>Type of material</th>
											<th>Weight</th>
											<th>Unit</th>
										</tr>
										</thead>
										<tbody>
											<tr>
												<td>1</td>
												<td>
													<select class="form-control" name="voucher_type_id">
														<option value="">Select</option>
														<?php $sSQL = "SELECT id,type FROM voucher_type ORDER BY id"; $rs = mysqli_query($con,$sSQL) or print(mysqli_error());
														while($row = mysqli_fetch_array($rs)){
															?>
															<option value=<?=$row["id"]?> selected="selected"><?=$row["type"]?></option>
															<?php
														}?>
													</select>
												</td>
												<td><input type="text" name="test" id="tag_no" value="10.25" class="form-control"></td>
												<td><input type="text" name="test" id="tag_no" value="Gram" class="form-control"></td>

											</tr>
											<tr>
												<td>2</td>
												<td>
													<select class="form-control" name="voucher_type_id">
														<option value="">Select</option>
														<?php $sSQL = "SELECT id,type FROM voucher_type ORDER BY id"; $rs = mysqli_query($con,$sSQL) or print(mysqli_error());
														while($row = mysqli_fetch_array($rs)){
															?>
															<option value=<?=$row["id"]?>  selected="selected"><?=$row["type"]?></option>
															<?php
														}?>
													</select>
												</td>
												<td><input type="text" name="test" id="tag_no" value="15.25" class="form-control"></td>
												<td><input type="text" name="test" id="tag_no" value="Gram" class="form-control"></td>

											</tr>
											<tr>
												<td>3</td>
												<td>
													<select class="form-control" name="voucher_type_id">
														<option value="">Select</option>
														<?php $sSQL = "SELECT id,type FROM voucher_type ORDER BY id"; $rs = mysqli_query($con,$sSQL) or print(mysqli_error());
														while($row = mysqli_fetch_array($rs)){
															?>
															<option value=<?=$row["id"]?>><?=$row["type"]?></option>
															<?php
														}?>
													</select>
												</td>
												<td><input type="text" name="test" id="tag_no" value="" class="form-control"></td>
												<td><input type="text" name="test" id="tag_no" value="" class="form-control"></td>

											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-12">
				<div class="col-xs-10 no-padding" style="float: right;">
					<div class="box-body no-padding">
						<div class="col-xs-2">
						</div>
						<div class="col-xs-2">
							<input type="submit" name="order_submit" id="order_submit" value="Save" class="btn btn-block btn-primary" />
						</div>
						<div class="col-xs-2">
							<a href="dashboard.php" class="btn btn-block btn-primary">Reset</a>
						</div>
						<div class="col-xs-2">
							<input type="submit" name="order_submit" id="order_submit" value="Save & Print" class="btn btn-block btn-primary" />
						</div>
						<div class="col-xs-2">
							<input type="submit" name="order_submit" id="order_submit" value="Order Printout" class="btn btn-block btn-primary" />
						</div>
						<div class="col-xs-2">
							<a href="dashboard.php" class="btn btn-block btn-primary">Reset</a>
						</div>
					</div>
				</div>
			</div>
		</form>

		</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <?php include("include/footer.php"); ?>
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
	<?php include("include/filelinks.php"); ?>
<script type="text/javascript">
	$(document).ready(function(){
		$(".file-input-ajax").fileinput({
			showPreview: true,
			showUpload: true,
			showCaption: false,
			browseIcon: '<i class="icon-file-plus"></i>',
			uploadIcon: '<i class="icon-file-upload2"></i>',
			removeIcon: '<i class="icon-cross3"></i>',
			defaultPreviewContent: '<img src="asdasdas.jpg" alt="Your Avatar" style="width:auto;height:160px;margin-top: 10px;">',
		});
	});
		$('body').on('focus','table#psList > tbody > tr.repeat:last > td > .row_total',function(){
			if($(this).val() != '' && $(this).val() != null){
				var clone = $('tr.hidden').clone();
				//console.log($('tr.repeat:last'));
				RowCount = $('table#psList > tbody > tr.repeat').length + 1;
				clone.insertAfter('tr.repeat:last').removeClass('hidden').addClass('repeat').find('td.sno').text(RowCount);
			}
		});

		$('body').on('input', '.metal_amount_box', function() {
			var tr = $(this).closest('tr');
			var order_wt = tr.find("#order_wt").val();
			var price_per_gram = tr.find("#price_per_gram").val();
			var amount = (order_wt * price_per_gram);
			tr.find("#metal_amount").val(amount);
		});
		$('body').on('input', '.labour_gram_box', function() {
			var tr = $(this).closest('tr');
			var price_per_gram = tr.find("#price_per_gram").val();
			var labour_per = tr.find("#labour_per").val();
			var amount = (price_per_gram * labour_per) / 100;
			tr.find("#labour_gram").val(amount);
		});
		$('body').on('input', '.discount_rs_box', function() {
			var tr = $(this).closest('tr');
			var metal_amount = tr.find("#metal_amount").val();
			var discount_per = tr.find("#discount_per").val();
			var amount = (metal_amount * discount_per) / 100;
			tr.find("#discount_rs").val(amount);
		});
		$('body').on('input', '.labour_amt_box', function() {
			var tr = $(this).closest('tr');
			var order_wt = tr.find("#order_wt").val();
			var labour_gram = tr.find("#labour_gram").val();
			var amount = (order_wt * labour_gram);
			tr.find("#labour_amt").val(amount);
		});
		$('body').on('input', '.mrp_box', function() {
			var tr = $(this).closest('tr');
			var metal_amount = tr.find("#metal_amount").val();
			var discount_rs = tr.find("#discount_rs").val();
			var labour_amt = tr.find("#labour_amt").val();
			var charges = tr.find("#charges").val();
			var amount = (get_value(metal_amount) - get_value(discount_rs) + get_value(labour_amt) + get_value(charges));
			tr.find("#mrp").val(amount);
		});
		$('body').on('input', '.total_box', function() {
			var tr = $(this).closest('tr');
			var mrp = tr.find("#mrp").val();
			var vat = tr.find("#vat").val();
			var amount = get_value(mrp) + ((get_value(mrp) * get_value(vat)) / 100);
			tr.find("#total").val(amount);
		});

		/* Start for Total Advance */
		$('body').on('input', '#advance_amt', function() {
			var advance_amt = $("#advance_amt").val();
			$("#total_advance").val(advance_amt);
		});
		/* End for Total Advance */
		
		var arrayVal = [];
		$('table#psList > thead > tr > th').each(function(){
			arrayVal[$(this).index()] = 0;
		});
		
		$('table#psList > tbody > tr.repeat > td').each(function(){
			arrayVal[$(this).index()] = +arrayVal[$(this).index()] + +$(this).find('input[type="text"]').val();
		});
		$('table#psList > tfoot > tr > th').each(function(){
			if(!$(this).hasClass('no_total')){
				//$(this).text(arrayVal[$(this).index()]);
			}
			/*calPendingAmount($(this));
			setPendingAmount();*/
		});
		var mrp = 1; var vat_tax_amt = 0; var vat = 1;
		$('body').on('input', 'input[type="text"]',function(){

			$('table#psList > thead > tr > th').each(function(){
				arrayVal[$(this).index()] = 0;
			});
			
			$('table#psList > tbody > tr.repeat > td').each(function(){
				arrayVal[$(this).index()] = +arrayVal[$(this).index()] + +$(this).find('input[type="text"]').val();
			});
			
			$('table#psList > tfoot > tr > th').each(function(){
				if(!$(this).hasClass('no_total')){
					$(this).text(arrayVal[$(this).index()]);
				}
				calPendingAmount($(this));
				setPendingAmount();
				/* Start calculation for #table-total  */	
				/*if($(this).hasClass('total_amt')){
					$("input[name='total_amt']").val(arrayVal[$(this).index()]);
				}
				if($(this).hasClass('discount_amt')){
					$("input[name='discount_amt']").val(arrayVal[$(this).index()]);
				}
				if($(this).hasClass('vat_tax_amt')){
					if($(this).hasClass('vat_tax_amt_mrp')) {
						mrp = arrayVal[$(this).index()];
					}
					if($(this).hasClass('vat_tax_amt_vat')) {
						vat = arrayVal[$(this).index()];
					}
					vat_tax_amt = (mrp * vat) / 100;
					$("input[name='vat_tax_amt']").val(vat_tax_amt);
				}*/
				/* End calculation for #table-total  */
			});

		});
		function get_value(value)
		{
			if(value == '' || value == null)
			{
				return 0;
			}
			else 
			{
				return parseInt(value);
			}
		}

		function setPendingAmount()
		{
			var total_amt = $("#total_amt").val();
			var discount_amt = $("#discount_amt").val();
			var vat_tax_amt = $("#vat_tax_amt").val();
			var advance_amt = $("#advance_amt").val();
			var amount = (get_value(total_amt) + get_value(vat_tax_amt)) - (get_value(discount_amt) + get_value(advance_amt)) ;
			$("#pending_amt").val(amount);
		}
		function calPendingAmount($this_value)
		{
			if($($this_value).hasClass('total_amt')){
				$("input[name='total_amt']").val(arrayVal[$($this_value).index()]);
			}
			if($($this_value).hasClass('discount_amt')){
				$("input[name='discount_amt']").val(arrayVal[$($this_value).index()]);
			}
			if($($this_value).hasClass('vat_tax_amt')){
				if($($this_value).hasClass('vat_tax_amt_mrp')) {
					mrp = arrayVal[$($this_value).index()];
				}
				if($($this_value).hasClass('vat_tax_amt_vat')) {
					vat = arrayVal[$($this_value).index()];
				}
				vat_tax_amt = (mrp * vat) / 100;
				$("input[name='vat_tax_amt']").val(vat_tax_amt);
			}
		}
</script>
</body>
</html>
