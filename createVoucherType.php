<!DOCTYPE html>
<html>
<head>
  <?php include("include/header.php"); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <?php include("include/mainheader.php"); ?>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Aditya</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <?php include("include/menu.php"); ?>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Create Voucher Type
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Create Voucher Type</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="box-body">
			<?php //include("msg.php"); ?>
			<form action="" method="post" name="create_voucher_type" id="create_voucher_type">
				<div class="form-group has-feedback">
					<div class="col-xs-12">
						<div class="row">
							<div class="col-xs-2">
								<label>Name</label>
							</div>
							<div class="col-xs-4">
								<input type="text" class="form-control" placeholder="Voucher Name" name="voucher_name"/>
							</div>
						</div>
						
					</div>
					<br/>
					<br/>
					<div class="col-xs-6">
						<div class="row">
							<div class="col-xs-2">
								<label></label>
							</div>
							<div class="col-xs-2">
								<label>General</label>
							</div>
							<div class="col-xs-2">
								<label></label>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-2">
								<label>Type of Voucher</label>
							</div>
							<div class="col-xs-4">
								<input type="text" class="form-control" placeholder="Type of Voucher" name="type_of_voucher"/>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-2">
								<label>Abbr. </label>
							</div>
							<div class="col-xs-4">
								<input type="text" class="form-control" placeholder="Abbr." name="abbr"/>
							</div>
						</div>
						<br/>
						<div class="row">
							<div class="col-xs-6">
								<label>Method of Voucher Numbering  ? </label>
							</div>
							<div class="col-xs-6">
								<div class="radio">
									
								</div>  
							</div>
						</div>
						<div class="row">
							<div class="col-xs-6">
								<label>Use EFFECTIVE Dates for Voucher ? </label>
							</div>
							<div class="col-xs-6">
								<div class="radio">
									<label>
										<input type="radio" name="optionsRadios" id="optionsRadios1" value="Y">
										Yes
									</label>
									<label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
									<label>
										<input type="radio" name="optionsRadios" id="optionsRadios2" value="N">
										No
									</label>
								</div>  
							</div>
						</div>
						<div class="row">
							<div class="col-xs-6">
								<label>Make Optional as Default ? </label>
							</div>
							<div class="col-xs-6">
								<div class="radio">
									<label>
										<input type="radio" name="optionsRadios" id="optionsRadios1" value="Y">
										Yes
									</label>
									<label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
									<label>
										<input type="radio" name="optionsRadios" id="optionsRadios2" value="N">
										No
									</label>
								</div>  
							</div>
						</div>
						<div class="row">
							<div class="col-xs-6">
								<label>Use Comman Narration ? </label>
							</div>
							<div class="col-xs-6">
								<div class="radio">
									<label>
										<input type="radio" name="optionsRadios" id="optionsRadios1" value="Y">
										Yes
									</label>
									<label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
									<label>
										<input type="radio" name="optionsRadios" id="optionsRadios2" value="N">
										No
									</label>
								</div>  
							</div>
						</div>
						<div class="row">
							<div class="col-xs-6">
								<label>Narration for Each Entry ? </label>
							</div>
							<div class="col-xs-6">
								<div class="radio">
									<label>
										<input type="radio" name="optionsRadios" id="optionsRadios1" value="Y">
										Yes
									</label>
									<label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
									<label>
										<input type="radio" name="optionsRadios" id="optionsRadios2" value="N">
										No
									</label>
								</div>  
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-4">
					<div class="row">
						<div class="col-xs-4">
						</div>
						<div class="col-xs-4">
							<label>Printing</label>	
						</div>
						<div class="col-xs-4">
						</div>
					</div>
					<div class="row">
						<div class="col-xs-6">
							<label>Print after Voucher ? </label>
						</div>
						<div class="col-xs-6">
							<div class="radio">
								<label>
									<input type="radio" name="optionsRadios" id="optionsRadios1" value="Y">
									Yes
								</label>
								<label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
								<label>
									<input type="radio" name="optionsRadios" id="optionsRadios2" value="N">
									No
								</label>
							</div>  
						</div>
					</div>
				</div>
				<div class="col-xs-2">
					<div class="row">
						<div class="col-xs-12">
							<label>Name of Class</label>	
						</div>
					</div>
				</div>	
				<div class="col-xs-12">
					<div class="row">
						<div class="col-xs-2">
							<button type="submit" class="btn btn-primary btn-block btn-flat" name="ok">Submit</button>
						</div>
						<div class="col-xs-2">
							<button type="reset" class="btn btn-primary btn-block btn-flat" name="reset" onClick="document.location.href='createGropu.php'"/>Reset</button>
						</div>
						<!-- /.col -->
					</div>
				</div>
			</form>
		</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <?php include("include/footer.php"); ?>
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
	<?php include("include/filelinks.php"); ?>
</body>
</html>
