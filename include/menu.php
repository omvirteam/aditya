<!--<ul class="sidebar-menu">
	<li class="treeview">
	  <a href="#">
		<i class="fa fa-dashboard"></i> <span>Master</span> <i class="fa fa-angle-left pull-right"></i>
	  </a>
	  <ul class="treeview-menu">
		<li><a href="#"><i class="fa fa-circle-o"></i><span>Account Info</span></a></li>
		<li><a href="#"><i class="fa fa-circle-o"></i><span>Inventory Info</span></a></li>
	  </ul>
	</li>
	<li class="treeview">
	  <a href="#">
		<i class="fa fa-dashboard"></i> <span>Transaction</span> <i class="fa fa-angle-left pull-right"></i>
	  </a>
	  <ul class="treeview-menu">
		<li><a href="#"><i class="fa fa-circle-o"></i><span>Accounting Vouchers</span></a></li>
		<li><a href="#"><i class="fa fa-circle-o"></i><span>Inventory Vouchers</span></a></li>
	  </ul>
	</li>
	<li class="treeview">
	  <a href="#">
		<i class="fa fa-dashboard"></i> <span>Import</span> <i class="fa fa-angle-left pull-right"></i>
	  </a>
	  <ul class="treeview-menu">
		<li><a href="#"><i class="fa fa-circle-o"></i><span>Import of Data</span></a></li>
	  </ul>
	</li>
	<li class="treeview">
	  <a href="#">
		<i class="fa fa-dashboard"></i> <span>Reports</span> <i class="fa fa-angle-left pull-right"></i>
	  </a>
	  <ul class="treeview-menu">
		<li><a href="#"><i class="fa fa-circle-o"></i><span>Balance Sheet</span></a></li>
		<li><a href="#"><i class="fa fa-circle-o"></i><span>Profit & Loss A/C</span></a></li>
		<li><a href="#"><i class="fa fa-circle-o"></i><span>Stock Summary</span></a></li>
		<li><a href="#"><i class="fa fa-circle-o"></i><span>Ratio Analysis</span></a></li>
		<li><a href="#"><i class="fa fa-circle-o"></i><span>Display</span></a></li>
		<li><a href="#"><i class="fa fa-circle-o"></i><span>Multi Account Printing</span></a></li>
		<li><a href="#"><i class="fa fa-circle-o"></i><span>Quit</span></a></li>
		<li><a href="#"><i class="fa fa-circle-o"></i><span>Payment to Receipt</span></a></li>
	  </ul>
	</li>
</ul>-->
<ul class="sidebar-menu">
		<li><a href="createCompany.php"><i class="fa fa-circle-o"></i><span>   Create Company</span></a></li>
		<li><a href="createGroup.php"><i class="fa fa-circle-o"></i><span>   Create Group</span></a></li>
		<li><a href="createItemGroup.php"><i class="fa fa-circle-o"></i><span>   Create Item Group</span></a></li>
		<li><a href="createItemCategory.php"><i class="fa fa-circle-o"></i><span>   Create Item Category</span></a></li>
		<li><a href="createItem.php"><i class="fa fa-circle-o"></i><span>   Create Item</span></a></li>
		<li><a href="createGroup.php"><i class="fa fa-circle-o"></i><span>   Create Group</span></a></li>
		<li><a href="createLedger.php"><i class="fa fa-circle-o"></i><span>   Create Ledger</span></a></li>
		<li><a href="createVoucherType.php"><i class="fa fa-circle-o"></i><span>   Create Voucher Type</span></a></li>
		<li><a href="createStockCategory.php"><i class="fa fa-circle-o"></i><span>   Create Stock Category</span></a></li>
		<li><a href="createStockGroup.php"><i class="fa fa-circle-o"></i><span>   Create Stock Group</span></a></li>
		<li><a href="tarrifClassification.php"><i class="fa fa-circle-o"></i><span>   Tarrif Classification</span></a></li>
		<li><a href="createStockItem.php"><i class="fa fa-circle-o"></i><span>   Create Stock Item</span></a></li>
		<li><a href="createUnit.php"><i class="fa fa-circle-o"></i><span>   Create Unit</span></a></li>
		<li><a href="createContraVoucher.php"><i class="fa fa-circle-o"></i><span>   Create Contra Voucher</span></a></li>
		<li><a href="createSalesVoucher.php"><i class="fa fa-circle-o"></i><span>   Create Sales Voucher</span></a></li>
		<li><a href="createPurchaseVoucher.php"><i class="fa fa-circle-o"></i><span>   Create Purchase Voucher</span></a></li>
		<li><a href="createPaymentVoucher.php"><i class="fa fa-circle-o"></i><span>   Create Payment Voucher</span></a></li>
		<li><a href="createReceiptVoucher.php"><i class="fa fa-circle-o"></i><span>   Create Receipt Voucher</span></a></li>
		<li><a href="createJournalVoucher.php"><i class="fa fa-circle-o"></i><span>   Create Journal Voucher</span></a></li>
		<li><a href="createMetalJournalVoucher.php"><i class="fa fa-circle-o"></i><span>   Create Metal Voucher</span></a></li>
		<li><a href="createKarigarIssueVoucher.php"><i class="fa fa-circle-o"></i><span>   Create Karigar Issue Voucher</span></a></li>
		<li><a href="createKarigarReceiveVoucher.php"><i class="fa fa-circle-o"></i><span>   Create Karigar Receive Voucher</span></a></li>
		<li><a href="createRepairingInwardVoucher.php"><i class="fa fa-circle-o"></i><span>   Create Reparing Inward Voucher</span></a></li>
		<li><a href="createRepairingOutwardVoucher.php"><i class="fa fa-circle-o"></i><span>   Create Reparing Outward Voucher</span></a></li>
		<li><a href="createProductMeargeVoucher.php"><i class="fa fa-circle-o"></i><span>   Create Product Merge Voucher</span></a></li>
		<li><a href="createProductDemeargeVoucher.php"><i class="fa fa-circle-o"></i><span>   Create Product Demerge voucher</span></a></li>
		<li><a href="createPurchaseReturnVoucher.php"><i class="fa fa-circle-o"></i><span>   Create Purchase Return Voucher</span></a></li>
		<li><a href="createSalesReturnVoucher.php"><i class="fa fa-circle-o"></i><span>   Create Sales Return Voucher</span></a></li>
		<li><a href="createCreditNoteVoucher.php"><i class="fa fa-circle-o"></i><span>   Create Credit Note</span></a></li>
		<li><a href="createDebitNoteVoucher.php"><i class="fa fa-circle-o"></i><span>   Create Debit Note Voucher</span></a></li>
		<li><a href="createPurchaseOrderVoucher.php"><i class="fa fa-circle-o"></i><span>   Create Purchase Order Voucher</span></a></li>
		<li><a href="createSalesOrderVoucher.php"><i class="fa fa-circle-o"></i><span>   Create Sales Order Voucher</span></a></li>
		<li><a href="balanceSheet.php"><i class="fa fa-circle-o"></i><span>   Balance Sheet</span></a></li>
		<li><a href="profitAndLossAccount.php"><i class="fa fa-circle-o"></i><span>   Profit And Loss Account</span></a></li>
		<li><a href="metalBalanceSheet.php"><i class="fa fa-circle-o"></i><span>   Metal Balance Sheet</span></a></li>
		<li><a href="stockDetail.php"><i class="fa fa-circle-o"></i><span>   Stock Detail</span></a></li>
		<li><a href="listOfTaxClassification.php"><i class="fa fa-circle-o"></i><span>   Tax Classification</span></a></li>
		<li><a href="tagCancel.php"><i class="fa fa-circle-o"></i><span>   Tag Cancel</span></a></li>
		<li><a href="create_sales_order_entry.php"><i class="fa fa-circle-o"></i><span>   Create Sales Order </span></a></li>
</ul>