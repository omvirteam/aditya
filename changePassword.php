<?php session_start();
if(isset($_SESSION['userName']))
{
	include_once('include/config.php');

	if(isset($_POST["oldPassword"]) && isset($_POST["newPassword"]) && isset($_POST["rePassword"]) && isset($_POST["userId"] ))
	{
		$passWord = '';
		$userQry = "SELECT passWord FROM user WHERE userId = " .$_POST["userId"];
		$rsUser = mysqli_query($con,$userQry) or print(mysqli_error($con));
		
		if(mysqli_num_rows($rsUser) > 0)
		{
			$row1 = mysqli_fetch_array($rsUser);
			$passWord = $row1["passWord"];
		}
		if($passWord!=$_POST["oldPassword"])
		{
			$_SESSION['error']="Old password does not match with your password. Please enter correct old password";
			header("location:changePassword.php");
			exit;
		}
		else if($_POST["newPassword"] != $_POST["rePassword"])
		{
			$_SESSION['error']="Confirm password must be match with the new password";
			header("Location:changePassword.php");
			exit;
		}
		else
		{
    
			$userUpdateQry = "UPDATE user SET ";
			$userUpdateQry .= "passWord='".$_POST["newPassword"]."'";
			$userUpdateQry .= " WHERE userId='". $_POST["userId"] ."'";
			mysqli_query($con,$userUpdateQry);

			$_SESSION['success']="Password updated successfully";
			header("Location:changePassword.php");
			exit;
		}
	}
?>

<!DOCTYPE html>
<html>
<head>
  <?php include("include/header.php"); ?>
  <script type="text/javascript">
	window.onload = function(){
		setTimeout(function () {document.getElementById('msg').style.display='none'}, 3000);
	}
	
  </script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <?php include("include/mainheader.php"); ?>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Aditya</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <?php include("include/menu.php"); ?>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Change Password
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Change Password</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
		<div class="box-body">
			<div id="msg">
				<?php include("msg.php"); ?>
			</div>
			<div class="col-xs-6">
				<form action="" method="post" name="changePassword" id="changePassword">
					<input type="hidden" name="userId" id="userId" value="<?php echo $_SESSION["userId"]; ?>">
					<div class="form-group has-feedback">
						<input type="password" class="form-control" placeholder="Old Password" name="oldPassword"/>
						<span class="glyphicon glyphicon-lock form-control-feedback"></span>
					</div>
					<div class="form-group has-feedback">
						<input type="password" class="form-control" placeholder="New Password" name="newPassword"/>
						<span class="glyphicon glyphicon-lock form-control-feedback"></span>
					</div>
					<div class="form-group has-feedback">
						<input type="password" class="form-control" placeholder="Retype password" name="rePassword" />
						<span class="glyphicon glyphicon-log-in form-control-feedback"></span>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<button type="submit" class="btn btn-primary btn-block btn-flat" name="submit">Change Password</button>
						</div><!-- /.col -->
					</div>
				</form>
			</div>
		</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <?php include("include/footer.php"); ?>
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
	<?php include("include/filelinks.php"); ?>
</body>
</html>
<?php
}
else
{
	header('location:index.php');
}
?>