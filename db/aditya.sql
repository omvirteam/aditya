-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 09, 2016 at 07:54 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aditya`
--

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `companyId` int(11) NOT NULL,
  `state` varchar(155) NOT NULL,
  `number` int(11) NOT NULL,
  `companyName` varchar(155) NOT NULL,
  `companyShortName` varchar(155) NOT NULL,
  `company` varchar(155) NOT NULL,
  `groupName` varchar(155) NOT NULL,
  `logo` varchar(155) NOT NULL,
  `natureOfBusiness` varchar(155) NOT NULL,
  `fromDate` date NOT NULL,
  `toDate` date NOT NULL,
  `securityType` varchar(155) NOT NULL,
  `reportHeader` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(155) NOT NULL,
  `pincode` varchar(55) NOT NULL,
  `phone` varchar(55) NOT NULL,
  `mobile` varchar(55) NOT NULL,
  `fax` varchar(55) NOT NULL,
  `email` varchar(155) NOT NULL,
  `website` varchar(155) NOT NULL,
  `tinNo` varchar(55) NOT NULL,
  `tinCstNo` varchar(55) NOT NULL,
  `panNo` varchar(55) NOT NULL,
  `exciseNo` varchar(55) NOT NULL,
  `tcsNo` varchar(55) NOT NULL,
  `serviceTaxNo` varchar(55) NOT NULL,
  `gstNo` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `group_id` int(11) NOT NULL,
  `group_name` varchar(255) NOT NULL,
  `under_group` varchar(255) NOT NULL,
  `is_primary` enum('Yes','No') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`group_id`, `group_name`, `under_group`, `is_primary`) VALUES
(1, 'Current Assets', '', 'Yes'),
(2, 'Bank Account', 'Current Assets', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `userId` int(11) NOT NULL,
  `userName` varchar(55) NOT NULL,
  `passWord` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`userId`, `userName`, `passWord`) VALUES
(1, 'admin', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`companyId`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`group_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`userId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `companyId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
  

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `voucher_type_id` int(11) NOT NULL,
  `order_no` int(11) NOT NULL,
  `order_date` date NOT NULL,
  `name` varchar(200) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(100) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `area_id` int(11) NOT NULL,
  `references_id` int(11) NOT NULL,
  `price_value_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `voucher_type_id`, `order_no`, `order_date`, `name`, `address`, `city`, `mobile`, `area_id`, `references_id`, `price_value_id`) VALUES
(19, 2, 0, '2016-06-15', 'sd', 'jnh', 'jh', 'hj', 2, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE IF NOT EXISTS `order_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `tag_no` varchar(200) NOT NULL,
  `item` varchar(200) NOT NULL,
  `size` varchar(50) NOT NULL,
  `pcs` int(11) NOT NULL,
  `stone_wt` double NOT NULL,
  `order_wt` double NOT NULL,
  `price_per_gram` int(15) NOT NULL,
  `metal_amount` int(15) NOT NULL,
  `labour_per` int(15) NOT NULL,
  `discount_per` int(15) NOT NULL,
  `labour_gram` double NOT NULL,
  `discount_rs` int(15) NOT NULL,
  `labour_amt` int(15) NOT NULL,
  `charges` int(15) NOT NULL,
  `mrp` int(15) NOT NULL,
  `vat` int(15) NOT NULL,
  `total` int(15) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`id`, `order_id`, `tag_no`, `item`, `size`, `pcs`, `stone_wt`, `order_wt`, `price_per_gram`, `metal_amount`, `labour_per`, `discount_per`, `labour_gram`, `discount_rs`, `labour_amt`, `charges`, `mrp`, `vat`, `total`) VALUES
(5, 19, '645', '3123', '12', 0, 12, 23, 21, 2, 23, 232, 4.83, 5, 21, 55, 7412, 23, 9117),
(6, 19, '2', '23', '21', 332, 12, 32, 333, 10656, 0, 0, 0, 0, 736, 23, 981, 1, 745),
(7, 19, '444', '444', '44', 44, 44, 44, 44, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `order_details`
--
ALTER TABLE `order_details`
  ADD CONSTRAINT `order_details_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;



--
-- Table structure for table `area`
--

CREATE TABLE IF NOT EXISTS `area` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `area`
--

INSERT INTO `area` (`id`, `name`) VALUES
(1, 'Bedipara'),
(2, 'Hudko'),
(3, 'Ranchhod Nagar');

-- --------------------------------------------------------

--
-- Table structure for table `price_value`
--

CREATE TABLE IF NOT EXISTS `price_value` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `price_value`
--

INSERT INTO `price_value` (`id`, `value`) VALUES
(1, 'Price Fixed With Customer'),
(2, 'Price Not Fixed With Customer');

-- --------------------------------------------------------

--
-- Table structure for table `references`
--

CREATE TABLE IF NOT EXISTS `references` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `references`
--

INSERT INTO `references` (`id`, `name`) VALUES
(1, 'Anil Mehta'),
(2, 'Kishor Patel'),
(3, 'Suresh Vala');

-- --------------------------------------------------------

--
-- Table structure for table `voucher_type`
--

CREATE TABLE IF NOT EXISTS `voucher_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `voucher_type`
--

INSERT INTO `voucher_type` (`id`, `type`) VALUES
(1, 'Estimate Book'),
(2, 'Tax Invoice Estimate'),
(3, 'Retail Invoice Estimate');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

ALTER TABLE `orders` ADD `total_advance` DOUBLE NOT NULL ;


