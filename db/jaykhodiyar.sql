-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 10, 2016 at 04:31 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `jaykhodiyar`
--

-- --------------------------------------------------------

--
-- Table structure for table `agent`
--

CREATE TABLE IF NOT EXISTS `agent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agent_name` varchar(100) NOT NULL,
  `commission` double NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `agent`
--

INSERT INTO `agent` (`id`, `agent_name`, `commission`, `created_by`, `created_at`, `updated_at`) VALUES
(5, 'Test', 45.12, 'admin', '2016-09-06 17:22:40', '2016-09-06 11:52:40'),
(6, 'Demo', 45.12, 'admin', '2016-09-06 17:22:48', '2016-09-06 11:52:48'),
(7, 'hsd', 78.45, 'admin', '2016-09-06 17:23:12', '2016-09-06 11:53:12');

-- --------------------------------------------------------

--
-- Table structure for table `billing_template`
--

CREATE TABLE IF NOT EXISTS `billing_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `billing_template`
--

INSERT INTO `billing_template` (`id`, `template`, `created_at`, `updated_at`) VALUES
(3, 'test', '2016-09-08 11:49:02', '2016-09-08 06:19:02'),
(4, 'template1', '2016-09-08 11:49:08', '2016-09-08 06:19:08');

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE IF NOT EXISTS `branch` (
  `branch_id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_code` varchar(100) NOT NULL,
  `branch` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`branch_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `branch`
--

INSERT INTO `branch` (`branch_id`, `branch_code`, `branch`, `created_at`, `updated_at`) VALUES
(1, 'BRC0001', 'BRANCH00001', '2016-08-20 18:10:04', '2016-08-20 12:40:34'),
(2, 'BRC0002', 'BRANCH00002', '2016-08-20 18:10:04', '2016-08-20 12:40:39'),
(3, '', '', '0000-00-00 00:00:00', '2016-08-21 09:30:22');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE IF NOT EXISTS `city` (
  `city_id` int(11) NOT NULL AUTO_INCREMENT,
  `state_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `city` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`city_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`city_id`, `state_id`, `country_id`, `city`, `created_at`, `updated_at`) VALUES
(12, 1, 1, 'Rajkot', '2016-09-06 13:00:28', '2016-09-06 07:30:28'),
(17, 1, 1, 'Morbi', '2016-09-06 14:27:36', '2016-09-06 08:57:36');

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE IF NOT EXISTS `company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `contact_no` varchar(15) NOT NULL,
  `pan_no` varchar(255) NOT NULL,
  `tan_no` varchar(255) NOT NULL,
  `pf_no` varchar(255) NOT NULL,
  `esic_no` varchar(255) NOT NULL,
  `pt_no` varchar(255) NOT NULL,
  `service_tax_no` varchar(255) NOT NULL,
  `vat_no` varchar(255) NOT NULL,
  `cst_no` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`id`, `name`, `address`, `contact_no`, `pan_no`, `tan_no`, `pf_no`, `esic_no`, `pt_no`, `service_tax_no`, `vat_no`, `cst_no`, `created_at`, `updated_at`) VALUES
(4, 'Jay Khodiyar', 'Gokul Dham Rajkot', '78998745609', '111111111119', '222222222229', '3333333333339', '44444444449', '555555555559', '6666669', '7777777779', '8888888889', '2016-09-07 12:47:26', '2016-09-07 07:17:26');

-- --------------------------------------------------------

--
-- Table structure for table `contact_person`
--

CREATE TABLE IF NOT EXISTS `contact_person` (
  `contact_person_id` int(11) NOT NULL AUTO_INCREMENT,
  `party_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone_no` varchar(20) NOT NULL,
  `mobile_no` varchar(20) NOT NULL,
  `fax_no` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `designation_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `note` text NOT NULL,
  `priority` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`contact_person_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=156 ;

--
-- Dumping data for table `contact_person`
--

INSERT INTO `contact_person` (`contact_person_id`, `party_id`, `name`, `phone_no`, `mobile_no`, `fax_no`, `email`, `designation_id`, `department_id`, `note`, `priority`, `created_at`, `updated_at`) VALUES
(12, 17, 'cnt1', '987987955', '9879879875', '7899877899', 'cnt1@gmail.com', 1, 2, 'Note', 5, '2016-08-22 18:54:55', '2016-08-22 13:24:55'),
(13, 17, 'cnt2', '999999999', '999999999', '99999999', 'cnt2@gmail.com', 1, 2, 'Note 3', 6, '2016-08-22 18:56:09', '2016-08-22 13:26:09'),
(14, 18, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 16:08:56', '2016-08-29 10:38:56'),
(15, 0, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 16:08:57', '2016-08-29 10:38:57'),
(16, 19, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 16:09:00', '2016-08-29 10:39:00'),
(17, 0, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 16:09:01', '2016-08-29 10:39:01'),
(18, 20, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 16:17:09', '2016-08-29 10:47:09'),
(19, 20, 'jay', '5465465', '5566646', '5645', 'admin@jvgjk.com', 1, 2, 'kdfdfd', 1, '2016-08-29 16:21:07', '2016-08-29 10:51:07'),
(20, 0, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 16:25:03', '2016-08-29 10:55:03'),
(21, 21, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 16:25:29', '2016-08-29 10:55:29'),
(22, 22, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 16:49:46', '2016-08-29 11:19:46'),
(23, 0, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 16:49:48', '2016-08-29 11:19:48'),
(24, 23, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 16:50:04', '2016-08-29 11:20:04'),
(25, 24, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 17:03:21', '2016-08-29 11:33:21'),
(26, 0, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 17:03:43', '2016-08-29 11:33:43'),
(27, 25, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 17:04:23', '2016-08-29 11:34:23'),
(28, 0, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 17:04:30', '2016-08-29 11:34:30'),
(29, 26, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 17:04:31', '2016-08-29 11:34:31'),
(30, 0, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 17:04:52', '2016-08-29 11:34:52'),
(31, 27, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 17:13:19', '2016-08-29 11:43:19'),
(32, 28, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 17:55:07', '2016-08-29 12:25:07'),
(33, 0, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 17:55:50', '2016-08-29 12:25:50'),
(34, 29, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 17:56:03', '2016-08-29 12:26:03'),
(35, 0, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 17:57:03', '2016-08-29 12:27:03'),
(36, 30, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 17:57:26', '2016-08-29 12:27:26'),
(37, 0, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 17:57:43', '2016-08-29 12:27:43'),
(38, 31, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 17:57:49', '2016-08-29 12:27:49'),
(39, 0, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 17:59:14', '2016-08-29 12:29:14'),
(40, 32, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 17:59:31', '2016-08-29 12:29:31'),
(41, 0, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 18:00:21', '2016-08-29 12:30:21'),
(42, 33, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 18:00:31', '2016-08-29 12:30:31'),
(43, 0, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 18:00:36', '2016-08-29 12:30:36'),
(44, 34, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 18:02:50', '2016-08-29 12:32:50'),
(45, 35, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 18:03:57', '2016-08-29 12:33:57'),
(46, 0, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 18:04:17', '2016-08-29 12:34:17'),
(47, 36, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 18:04:21', '2016-08-29 12:34:21'),
(48, 37, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 18:05:16', '2016-08-29 12:35:16'),
(49, 37, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 18:05:32', '2016-08-29 12:35:32'),
(50, 37, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 18:05:42', '2016-08-29 12:35:42'),
(51, 37, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 18:05:45', '2016-08-29 12:35:45'),
(52, 37, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 18:05:55', '2016-08-29 12:35:55'),
(53, 37, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 18:06:09', '2016-08-29 12:36:09'),
(54, 37, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 18:06:19', '2016-08-29 12:36:19'),
(55, 37, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 18:07:14', '2016-08-29 12:37:14'),
(56, 37, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 18:07:40', '2016-08-29 12:37:40'),
(57, 38, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 18:08:16', '2016-08-29 12:38:16'),
(58, 0, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 18:17:58', '2016-08-29 12:47:58'),
(59, 0, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 18:18:08', '2016-08-29 12:48:08'),
(60, 0, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 18:18:48', '2016-08-29 12:48:48'),
(61, 0, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 18:20:46', '2016-08-29 12:50:46'),
(62, 0, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 18:49:19', '2016-08-29 13:19:19'),
(63, 0, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 18:50:22', '2016-08-29 13:20:22'),
(64, 0, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 18:50:38', '2016-08-29 13:20:38'),
(65, 0, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 18:51:10', '2016-08-29 13:21:10'),
(66, 0, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 18:51:22', '2016-08-29 13:21:22'),
(67, 39, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 18:57:38', '2016-08-29 13:27:38'),
(68, 39, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 18:58:04', '2016-08-29 13:28:04'),
(69, 39, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 18:59:20', '2016-08-29 13:29:20'),
(70, 39, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 19:02:46', '2016-08-29 13:32:46'),
(71, 40, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 19:03:08', '2016-08-29 13:33:08'),
(72, 40, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 19:03:44', '2016-08-29 13:33:44'),
(73, 40, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 19:04:03', '2016-08-29 13:34:03'),
(74, 40, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 19:04:17', '2016-08-29 13:34:17'),
(75, 40, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 19:06:21', '2016-08-29 13:36:21'),
(76, 40, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 19:09:33', '2016-08-29 13:39:33'),
(77, 41, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 19:13:27', '2016-08-29 13:43:27'),
(78, 41, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 19:13:46', '2016-08-29 13:43:46'),
(79, 42, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 19:16:05', '2016-08-29 13:46:05'),
(80, 42, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 19:16:17', '2016-08-29 13:46:17'),
(81, 42, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 19:16:27', '2016-08-29 13:46:27'),
(82, 43, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 19:29:59', '2016-08-29 13:59:59'),
(83, 43, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 19:31:19', '2016-08-29 14:01:19'),
(84, 43, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 19:35:44', '2016-08-29 14:05:44'),
(85, 43, '', '', '', '', '', 1, 1, '', 1, '2016-08-29 19:35:58', '2016-08-29 14:05:58'),
(86, 43, '', '', '', '', '', 3, 3, '', 0, '2016-08-29 19:43:23', '2016-08-29 14:13:23'),
(87, 43, '', '', '', '', '', 3, 3, '', 0, '2016-08-29 19:43:25', '2016-08-29 14:13:25'),
(88, 44, '', '', '', '', '', 3, 3, '', 0, '2016-08-29 19:51:30', '2016-08-29 14:21:30'),
(89, 0, '', '', '', '', '', 1, 1, '', 1, '2016-08-30 10:34:01', '2016-08-30 05:04:01'),
(90, 0, '', '', '', '', '', 1, 1, '', 1, '2016-08-30 10:34:35', '2016-08-30 05:04:35'),
(91, 0, '', '', '', '', '', 1, 1, '', 1, '2016-08-30 10:34:56', '2016-08-30 05:04:56'),
(92, 0, '', '', '', '', '', 1, 1, '', 1, '2016-08-30 10:36:13', '2016-08-30 05:06:13'),
(93, 0, '', '', '', '', '', 1, 1, '', 1, '2016-08-30 10:38:10', '2016-08-30 05:08:10'),
(94, 0, '', '', '', '', '', 1, 1, '', 1, '2016-08-30 10:38:14', '2016-08-30 05:08:14'),
(95, 0, '', '', '', '', '', 1, 1, '', 1, '2016-08-30 10:38:27', '2016-08-30 05:08:27'),
(96, 0, '', '', '', '', '', 1, 1, '', 1, '2016-08-30 10:39:18', '2016-08-30 05:09:18'),
(97, 0, '', '', '', '', '', 1, 1, '', 1, '2016-08-30 10:41:16', '2016-08-30 05:11:16'),
(98, 0, '', '', '', '', '', 1, 1, '', 1, '2016-08-30 10:47:34', '2016-08-30 05:17:34'),
(99, 0, '', '', '', '', '', 1, 1, '', 1, '2016-08-30 10:48:01', '2016-08-30 05:18:01'),
(100, 0, '', '', '', '', '', 1, 1, '', 1, '2016-08-30 10:48:40', '2016-08-30 05:18:40'),
(101, 45, '', '', '', '', '', 1, 1, '', 1, '2016-08-30 11:00:36', '2016-08-30 05:30:36'),
(102, 45, '', '', '', '', '', 1, 1, '', 1, '2016-08-30 11:00:45', '2016-08-30 05:30:45'),
(103, 45, '', '', '', '', '', 1, 1, '', 1, '2016-08-30 11:01:06', '2016-08-30 05:31:06'),
(104, 45, '', '', '', '', '', 1, 1, '', 1, '2016-08-30 11:01:25', '2016-08-30 05:31:25'),
(105, 45, '', '', '', '', '', 1, 1, '', 1, '2016-08-30 11:01:35', '2016-08-30 05:31:35'),
(106, 45, '', '', '', '', '', 1, 1, '', 1, '2016-08-30 11:01:43', '2016-08-30 05:31:43'),
(107, 45, '', '', '', '', '', 1, 1, '', 1, '2016-08-30 11:02:27', '2016-08-30 05:32:27'),
(108, 46, '', '', '', '', '', 3, 3, '', 0, '2016-08-30 11:10:02', '2016-08-30 05:40:02'),
(109, 47, '', '', '', '', '', 1, 1, '', 1, '2016-08-30 11:30:46', '2016-08-30 06:00:46'),
(110, 48, '', '', '', '', '', 1, 1, '', 1, '2016-08-30 11:31:10', '2016-08-30 06:01:10'),
(111, 48, '', '', '', '', '', 1, 1, '', 1, '2016-08-30 11:31:17', '2016-08-30 06:01:17'),
(112, 49, '', '', '', '', '', 3, 3, '', 0, '2016-08-30 11:35:11', '2016-08-30 06:05:11'),
(113, 49, 'asd', '', '', '', '', 3, 3, '', 0, '2016-08-30 11:35:14', '2016-08-30 06:05:14'),
(114, 49, '', '', '', '', '', 3, 3, '', 0, '2016-08-30 11:35:17', '2016-08-30 06:05:17'),
(115, 50, 'cxzc', '', '', '', '', 3, 3, '', 0, '2016-08-30 11:40:17', '2016-08-30 06:10:17'),
(116, 50, '', '', '', '', '', 1, 1, '', 1, '2016-08-30 11:40:39', '2016-08-30 06:10:39'),
(117, 51, 'vfccv', '', '', '', '', 3, 3, '', 0, '2016-08-30 11:49:53', '2016-08-30 06:19:53'),
(118, 51, 'ccv', '', '', '', '', 3, 3, '', 0, '2016-08-30 11:50:29', '2016-08-30 06:20:29'),
(119, 51, 'sd', '', '', '', '', 3, 3, '', 0, '2016-08-30 11:52:05', '2016-08-30 06:22:05'),
(120, 52, 'fg', '', '', '', '', 3, 3, '', 0, '2016-08-30 11:52:46', '2016-08-30 06:22:46'),
(121, 53, 'c1', '', '', '', '', 3, 3, '', 0, '2016-08-30 11:59:46', '2016-08-30 06:29:46'),
(122, 53, '', '', '', '', '', 3, 3, '', 2, '2016-08-30 12:00:28', '2016-08-30 06:30:28'),
(123, 53, '', '', '', '', '', 1, 1, '', 1, '2016-08-30 12:00:59', '2016-08-30 06:30:59'),
(124, 54, '', '', '', '', '', 3, 3, '', 0, '2016-08-30 12:36:52', '2016-08-30 07:06:52'),
(125, 35, 'wrwrqe', '', '', '', '', 3, 3, '', 0, '2016-08-30 14:38:15', '2016-08-30 09:08:15'),
(155, 76, 'cccc', '', '', '', '', 3, 3, '', 0, '2016-08-30 16:43:17', '2016-08-30 11:13:17');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE IF NOT EXISTS `country` (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `country` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`country_id`, `country`, `created_at`, `updated_at`) VALUES
(1, 'India', '2016-08-20 16:39:42', '2016-08-20 11:43:57'),
(32, 'UK', '2016-09-06 11:43:53', '2016-09-06 06:13:53');

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE IF NOT EXISTS `currency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `currency` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`id`, `currency`, `created_at`, `updated_at`) VALUES
(3, 'INR', '2016-09-08 18:05:05', '2016-09-08 12:35:05'),
(4, 'USD', '2016-09-08 18:05:14', '2016-09-08 12:35:14'),
(5, 'POUND', '2016-09-08 18:05:19', '2016-09-08 12:35:19');

-- --------------------------------------------------------

--
-- Table structure for table `default_terms_condition`
--

CREATE TABLE IF NOT EXISTS `default_terms_condition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template_name` varchar(255) NOT NULL,
  `group_name` varchar(255) NOT NULL,
  `terms_condition` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `default_terms_condition`
--

INSERT INTO `default_terms_condition` (`id`, `template_name`, `group_name`, `terms_condition`, `created_at`, `updated_at`) VALUES
(1, 'Inquiry', 'Delivery', 'sad', '2016-09-09 11:44:09', '2016-09-09 06:14:09'),
(4, 'Quatation', 'Packing', 'zdfdf', '2016-09-09 11:44:46', '2016-09-09 06:14:46'),
(5, 'Sales', 'Test', 'Ipsum', '2016-09-09 11:45:03', '2016-09-09 06:15:03');

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE IF NOT EXISTS `department` (
  `department_id` int(11) NOT NULL AUTO_INCREMENT,
  `department` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`department_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`department_id`, `department`, `created_at`, `updated_at`) VALUES
(8, 'Purchase', '2016-09-06 15:38:56', '2016-09-06 10:08:56'),
(9, 'Account', '2016-09-06 15:39:03', '2016-09-06 10:09:03'),
(10, 'All', '2016-09-06 15:39:13', '2016-09-06 10:09:13');

-- --------------------------------------------------------

--
-- Table structure for table `designation`
--

CREATE TABLE IF NOT EXISTS `designation` (
  `designation_id` int(11) NOT NULL AUTO_INCREMENT,
  `designation` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`designation_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `designation`
--

INSERT INTO `designation` (`designation_id`, `designation`, `created_at`, `updated_at`) VALUES
(5, 'Manager', '2016-09-06 15:24:29', '2016-09-06 09:54:29'),
(6, 'Asst. Manager', '2016-09-06 15:24:38', '2016-09-06 09:54:38'),
(7, 'CEO', '2016-09-06 15:24:44', '2016-09-06 09:54:44');

-- --------------------------------------------------------

--
-- Table structure for table `drawing_number`
--

CREATE TABLE IF NOT EXISTS `drawing_number` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `drawing_number` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `drawing_number`
--

INSERT INTO `drawing_number` (`id`, `drawing_number`, `created_at`, `updated_at`) VALUES
(4, 'new value 1', '2016-09-08 10:08:09', '2016-09-08 04:38:09'),
(5, 'new value 2', '2016-09-08 10:08:19', '2016-09-08 04:38:19'),
(6, 'new value 3', '2016-09-08 10:08:54', '2016-09-08 04:38:54');

-- --------------------------------------------------------

--
-- Table structure for table `excise`
--

CREATE TABLE IF NOT EXISTS `excise` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  `sub_heading` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `excise`
--

INSERT INTO `excise` (`id`, `description`, `sub_heading`, `created_at`, `updated_at`) VALUES
(2, 'party excise', 'sub heading1', '2016-09-08 10:55:09', '2016-09-08 05:25:09'),
(3, 'party excise test', 'sub heading2', '2016-09-08 10:55:29', '2016-09-08 05:25:29');

-- --------------------------------------------------------

--
-- Table structure for table `industry_type`
--

CREATE TABLE IF NOT EXISTS `industry_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `industry_type` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `industry_type`
--

INSERT INTO `industry_type` (`id`, `industry_type`, `created_at`, `updated_at`) VALUES
(4, 'Engginering & manufacturing', '2016-09-08 16:46:04', '2016-09-08 11:16:04'),
(6, 'test', '2016-09-08 16:46:21', '2016-09-08 11:16:21');

-- --------------------------------------------------------

--
-- Table structure for table `inquiry`
--

CREATE TABLE IF NOT EXISTS `inquiry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inquiry_no` varchar(100) NOT NULL,
  `customer_code1` varchar(255) NOT NULL,
  `customer_code2` varchar(255) NOT NULL,
  `kind_attn` varchar(255) NOT NULL,
  `contact_no` varchar(15) NOT NULL,
  `email_id` varchar(150) NOT NULL,
  `reference` varchar(255) NOT NULL,
  `indusrty_type` varchar(255) NOT NULL,
  `inquiry_stage` varchar(255) NOT NULL,
  `priority` varchar(255) NOT NULL,
  `inquiry_date` varchar(255) NOT NULL,
  `reference_date` date NOT NULL,
  `review_date` date NOT NULL,
  `status` varchar(255) NOT NULL,
  `assigned_to` varchar(255) NOT NULL,
  `lead_owner` varchar(255) NOT NULL,
  `lead_provider` varchar(255) NOT NULL,
  `lead_details` varchar(255) NOT NULL,
  `lead_end_date` date NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `inquiry_confirmation`
--

CREATE TABLE IF NOT EXISTS `inquiry_confirmation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inquiry_confirmation` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `inquiry_confirmation`
--

INSERT INTO `inquiry_confirmation` (`id`, `inquiry_confirmation`, `created_at`, `updated_at`) VALUES
(3, 'Email', '2016-09-09 10:44:52', '2016-09-09 05:14:52'),
(4, 'Phone', '2016-09-09 10:44:54', '2016-09-09 05:14:54'),
(5, 'Verbal', '2016-09-09 10:44:58', '2016-09-09 05:14:58'),
(6, 'Fax', '2016-09-09 10:45:01', '2016-09-09 05:15:01');

-- --------------------------------------------------------

--
-- Table structure for table `inquiry_stage`
--

CREATE TABLE IF NOT EXISTS `inquiry_stage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inquiry_stage` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `inquiry_stage`
--

INSERT INTO `inquiry_stage` (`id`, `inquiry_stage`, `created_at`, `updated_at`) VALUES
(18, 'A', '2016-09-08 13:03:03', '2016-09-08 07:33:03'),
(19, 'B', '2016-09-08 13:03:05', '2016-09-08 07:33:05'),
(20, 'C', '2016-09-08 13:03:07', '2016-09-08 07:33:07');

-- --------------------------------------------------------

--
-- Table structure for table `inquiry_status`
--

CREATE TABLE IF NOT EXISTS `inquiry_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inquiry_status` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `inquiry_status`
--

INSERT INTO `inquiry_status` (`id`, `inquiry_status`, `created_at`, `updated_at`) VALUES
(7, 'test', '2016-09-08 16:55:24', '2016-09-08 11:25:24'),
(8, 'a', '2016-09-08 16:55:37', '2016-09-08 11:25:37');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE IF NOT EXISTS `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_category` varchar(255) NOT NULL,
  `item_code1` varchar(255) NOT NULL,
  `item_code2` varchar(255) NOT NULL,
  `uom` varchar(255) NOT NULL,
  `qty` double NOT NULL,
  `item_type` varchar(255) NOT NULL,
  `stock` int(11) NOT NULL,
  `document` varchar(100) NOT NULL,
  `item_group` varchar(255) NOT NULL,
  `item_active` int(11) NOT NULL COMMENT '0- Active 1-Deactive',
  `item_name` varchar(255) NOT NULL,
  `cfactor` varchar(255) NOT NULL,
  `conv_uom` varchar(255) NOT NULL,
  `item_mpt` varchar(255) NOT NULL,
  `item_status` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `item_category`, `item_code1`, `item_code2`, `uom`, `qty`, `item_type`, `stock`, `document`, `item_group`, `item_active`, `item_name`, `cfactor`, `conv_uom`, `item_mpt`, `item_status`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Finished Goods', 'dsfds', '21323', 'Select UOM', 0, 'Select Item Type', 0, 'New_Text_Document.txt', 'gooo', 1, 'sdfsddd', '', 'Select Conv. UOM', 'Sub-Contracting', 'In USe', 'clock_1.jpg', '2016-09-07 16:43:21', '2016-09-07 11:13:21'),
(4, 'Finished Goods', 'sds123', '12323', 'Select UOM', 0, 'Select Item Type', 0, '0', 'gooo', 1, 'Item Name', '', 'Select Conv. UOM', 'Select Material Process Type', 'Select Status', '0', '2016-09-07 16:54:10', '2016-09-07 11:24:10'),
(5, '6', 'HJD12', 'DSS12', '4', 15, '5', 0, '0', '2', 1, 'gdfgfd', 'dfgdf', '5', '5', '', '0', '2016-09-09 18:01:20', '2016-09-09 12:31:20');

-- --------------------------------------------------------

--
-- Table structure for table `item_category`
--

CREATE TABLE IF NOT EXISTS `item_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_category` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `item_category`
--

INSERT INTO `item_category` (`id`, `item_category`, `created_at`, `updated_at`) VALUES
(6, 'Finished Goods', '2016-09-07 17:17:45', '2016-09-07 11:47:45'),
(7, 'Raw Materials', '2016-09-07 17:17:56', '2016-09-07 11:47:56'),
(8, 'Job Work', '2016-09-07 17:18:04', '2016-09-07 11:48:04');

-- --------------------------------------------------------

--
-- Table structure for table `item_class`
--

CREATE TABLE IF NOT EXISTS `item_class` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_class` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `item_class`
--

INSERT INTO `item_class` (`id`, `item_class`, `created_at`, `updated_at`) VALUES
(3, 'A', '2016-09-08 10:34:54', '2016-09-08 05:04:54'),
(4, 'B', '2016-09-08 10:34:57', '2016-09-08 05:04:57'),
(5, 'C', '2016-09-08 10:35:01', '2016-09-08 05:05:01');

-- --------------------------------------------------------

--
-- Table structure for table `item_group_code`
--

CREATE TABLE IF NOT EXISTS `item_group_code` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ig_code` varchar(255) NOT NULL,
  `ig_description` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `item_group_code`
--

INSERT INTO `item_group_code` (`id`, `ig_code`, `ig_description`, `created_at`, `updated_at`) VALUES
(2, '456sd', 'sdfdssd', '2016-09-07 17:29:12', '2016-09-07 11:59:12'),
(4, '14dsd', 'dsfdfs sdfasd', '2016-09-07 17:35:45', '2016-09-07 12:05:45');

-- --------------------------------------------------------

--
-- Table structure for table `item_make`
--

CREATE TABLE IF NOT EXISTS `item_make` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_make` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `item_make`
--

INSERT INTO `item_make` (`id`, `item_make`, `created_at`, `updated_at`) VALUES
(3, 'new value 1', '2016-09-07 19:41:42', '2016-09-07 14:11:42'),
(4, 'new value 2', '2016-09-07 19:41:55', '2016-09-07 14:11:55'),
(5, 'new value 3', '2016-09-07 19:42:05', '2016-09-07 14:12:05'),
(6, 'new value 4', '2016-09-07 19:42:15', '2016-09-07 14:12:15');

-- --------------------------------------------------------

--
-- Table structure for table `item_status`
--

CREATE TABLE IF NOT EXISTS `item_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_status` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `item_status`
--

INSERT INTO `item_status` (`id`, `item_status`, `created_at`, `updated_at`) VALUES
(5, 'In USe', '2016-09-07 18:29:47', '2016-09-07 12:59:47'),
(6, 'Hold For Dispatch', '2016-09-07 18:29:54', '2016-09-07 12:59:54'),
(7, 'Sub-Contracting', '2016-09-07 18:30:06', '2016-09-07 13:00:06');

-- --------------------------------------------------------

--
-- Table structure for table `item_type`
--

CREATE TABLE IF NOT EXISTS `item_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_type` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `item_type`
--

INSERT INTO `item_type` (`id`, `item_type`, `created_at`, `updated_at`) VALUES
(5, 'Sales', '2016-09-07 18:14:30', '2016-09-07 12:44:30'),
(6, 'Purchase', '2016-09-07 18:14:41', '2016-09-07 12:44:41'),
(7, 'General', '2016-09-07 18:14:50', '2016-09-07 12:44:50'),
(8, 'Services', '2016-09-07 18:14:59', '2016-09-07 12:44:59');

-- --------------------------------------------------------

--
-- Table structure for table `lead_contact_person`
--

CREATE TABLE IF NOT EXISTS `lead_contact_person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_name` varchar(100) NOT NULL,
  `department` varchar(100) NOT NULL,
  `designation` varchar(100) NOT NULL,
  `phone_no` varchar(15) NOT NULL,
  `mobile_no` varchar(15) NOT NULL,
  `fax_no` varchar(20) NOT NULL,
  `email_id` varchar(100) NOT NULL,
  `division` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `lead_contact_person`
--

INSERT INTO `lead_contact_person` (`id`, `person_name`, `department`, `designation`, `phone_no`, `mobile_no`, `fax_no`, `email_id`, `division`, `address`, `created_at`, `updated_at`) VALUES
(8, 'dfds', 'dfddf', 'dsfdsfdfd', 'hhgjj', '', '', '', '', '', '2016-09-10 15:48:00', '2016-09-10 10:18:00'),
(9, 'dfsd', 'dsfsd', 'sdf', '', '', '', '', '', '', '2016-09-10 15:48:21', '2016-09-10 10:18:21'),
(10, 'dsfds', 'sdfsd', '', '', '', '', '', '', '', '2016-09-10 15:51:44', '2016-09-10 10:21:44');

-- --------------------------------------------------------

--
-- Table structure for table `lead_details`
--

CREATE TABLE IF NOT EXISTS `lead_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lead_no` varchar(255) NOT NULL,
  `lead_title` varchar(255) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `lead_detail` text NOT NULL,
  `industry_type` varchar(255) NOT NULL,
  `lead_owner` varchar(255) NOT NULL,
  `assigned_to` varchar(255) NOT NULL,
  `priority` varchar(255) NOT NULL,
  `lead_source` varchar(255) NOT NULL,
  `lead_stage` varchar(255) NOT NULL,
  `lead_provider` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `review_date` date NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `lead_details`
--

INSERT INTO `lead_details` (`id`, `lead_no`, `lead_title`, `start_date`, `end_date`, `lead_detail`, `industry_type`, `lead_owner`, `assigned_to`, `priority`, `lead_source`, `lead_stage`, `lead_provider`, `status`, `review_date`, `created_at`, `updated_at`) VALUES
(16, 'no12', 'title2', '2016-09-10', '2016-09-10', '', 'Engineering & Manufacturing', '', '', 'low', 'By Mail', 'IN PROCESS', '', 'Closed', '2016-09-10', '2016-09-10 15:48:21', '2016-09-10 10:18:21'),
(17, 'dsfsd23', 'sdfsdfsdf', '2016-09-10', '2016-09-10', 'sdfsdf', 'Engineering & Manufacturing', '', '', 'low', 'By Mail', 'IN PROCESS', '', 'Closed', '2016-09-10', '2016-09-10 15:51:44', '2016-09-10 10:21:44');

-- --------------------------------------------------------

--
-- Table structure for table `lead_provider`
--

CREATE TABLE IF NOT EXISTS `lead_provider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lead_provider` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `lead_provider`
--

INSERT INTO `lead_provider` (`id`, `lead_provider`, `created_at`, `updated_at`) VALUES
(3, 'aaa', '2016-09-08 17:30:27', '2016-09-08 12:00:27'),
(4, 'Test', '2016-09-08 17:30:29', '2016-09-08 12:00:29');

-- --------------------------------------------------------

--
-- Table structure for table `main_group`
--

CREATE TABLE IF NOT EXISTS `main_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_group` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `main_group`
--

INSERT INTO `main_group` (`id`, `item_group`, `code`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Group1', 'TGC1', 'TGC1', '2016-09-07 19:00:43', '2016-09-07 13:30:43'),
(4, 'Group2', 'TGC2', 'TGC2', '2016-09-07 19:01:43', '2016-09-07 13:31:43');

-- --------------------------------------------------------

--
-- Table structure for table `master_branch`
--

CREATE TABLE IF NOT EXISTS `master_branch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `master_branch`
--

INSERT INTO `master_branch` (`id`, `branch`, `created_at`, `updated_at`) VALUES
(3, 'branch1', '2016-09-08 11:34:02', '2016-09-08 06:04:02'),
(4, 'branch2', '2016-09-08 11:34:06', '2016-09-08 06:04:06');

-- --------------------------------------------------------

--
-- Table structure for table `material_process_type`
--

CREATE TABLE IF NOT EXISTS `material_process_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `material_process_type` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `material_process_type`
--

INSERT INTO `material_process_type` (`id`, `material_process_type`, `created_at`, `updated_at`) VALUES
(4, 'Process -In -House', '2016-09-07 18:39:28', '2016-09-07 13:09:28'),
(5, 'Procured', '2016-09-07 18:39:36', '2016-09-07 13:09:36'),
(6, 'Sub-Contracting', '2016-09-07 18:39:44', '2016-09-07 13:09:44');

-- --------------------------------------------------------

--
-- Table structure for table `material_specification`
--

CREATE TABLE IF NOT EXISTS `material_specification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `material` varchar(255) NOT NULL,
  `material_specification` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `material_specification`
--

INSERT INTO `material_specification` (`id`, `material`, `material_specification`, `created_at`, `updated_at`) VALUES
(2, 'FG456', 'test', '2016-09-08 10:24:41', '2016-09-08 04:54:41'),
(4, 'FG45612', 'test', '2016-09-08 10:25:35', '2016-09-08 04:55:35');

-- --------------------------------------------------------

--
-- Table structure for table `party`
--

CREATE TABLE IF NOT EXISTS `party` (
  `party_id` int(11) NOT NULL AUTO_INCREMENT,
  `party_code` varchar(255) NOT NULL,
  `party_name` varchar(255) NOT NULL,
  `party_type_1` int(11) DEFAULT NULL,
  `party_type_2` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `address` text NOT NULL,
  `area` varchar(255) NOT NULL,
  `pincode` varchar(15) NOT NULL,
  `city_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `phone_no` varchar(20) NOT NULL,
  `fax_no` varchar(50) NOT NULL,
  `email_id` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL,
  `opening_bal` float NOT NULL,
  `credit_limit` float NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1-Active ,2-Deactive',
  `tin_vat_no` varchar(100) NOT NULL,
  `tin_cst_no` varchar(100) NOT NULL,
  `ecc_no` varchar(100) NOT NULL,
  `pan_no` varchar(100) NOT NULL,
  `range` varchar(100) NOT NULL,
  `division` varchar(100) NOT NULL,
  `service_tax_no` varchar(100) NOT NULL,
  `address_work` varchar(100) NOT NULL,
  `w_address` text NOT NULL,
  `w_city` int(11) DEFAULT NULL,
  `w_state` int(11) DEFAULT NULL,
  `w_country` int(11) DEFAULT NULL,
  `w_email` varchar(100) NOT NULL,
  `w_web` varchar(100) NOT NULL,
  `w_phone1` varchar(20) NOT NULL,
  `w_phone2` varchar(20) NOT NULL,
  `w_phone3` varchar(20) NOT NULL,
  `w_note` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`party_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=39 ;

--
-- Dumping data for table `party`
--

INSERT INTO `party` (`party_id`, `party_code`, `party_name`, `party_type_1`, `party_type_2`, `branch_id`, `address`, `area`, `pincode`, `city_id`, `state_id`, `country_id`, `phone_no`, `fax_no`, `email_id`, `website`, `opening_bal`, `credit_limit`, `active`, `tin_vat_no`, `tin_cst_no`, `ecc_no`, `pan_no`, `range`, `division`, `service_tax_no`, `address_work`, `w_address`, `w_city`, `w_state`, `w_country`, `w_email`, `w_web`, `w_phone1`, `w_phone2`, `w_phone3`, `w_note`, `created_at`, `updated_at`) VALUES
(17, 'prt1', 'prt1', 1, 2, 1, 'At adsads', 'sadfdf', '343434', 1, 1, 1, '9879789787', '9789877877', 'prt1@gmail.com', 'www.prt1.com', 0, 0, 0, '', '', '', '', '', '', '', '', '', 0, 0, 0, '', '', '', '', '', '', '2016-08-22 18:54:55', '2016-08-22 13:24:55'),
(20, 'dfds', 'dfds', 1, 1, 1, 'dsfsd', 'dsf', 'dfds', 6, 2, 2, 'sdf', 'sdfd', 'dsfsd', 'fsdf', 0, 0, 0, '', '', '', '', '', '', '', '', '', 0, 0, 0, '', '', '', '', '', '', '2016-08-29 16:17:09', '2016-08-29 10:55:03'),
(21, 'dfds1', 'dfds', 1, 1, 1, 'dsfsd', 'dsf', 'dfds', 6, 2, 2, 'sdf', 'sdfd', 'dsfsd', 'fsdf', 0, 0, 0, '', '', '', '', '', '', '', '', '', 0, 0, 0, '', '', '', '', '', '', '2016-08-29 16:25:29', '2016-08-29 10:55:29'),
(22, 'sdfd', 'sdfd', 1, 1, 1, 'dfsdfsdf', 'sd', '4564', 2, 1, 1, '546', '5445', '4545dasa', 'sdfdsfs', 0, 0, 0, '', '', '', '', '', '', '', '', '', 0, 0, 0, '', '', '', '', '', '', '2016-08-29 16:49:46', '2016-08-29 11:19:46'),
(35, 'dsfsd', 'dsfdssdf', 1, 1, 1, 'fds', 'sdfs', 'fdf', 1, 1, 1, 'sdfsd', 'SDFSDF', '', '', 0, 0, 0, '', '', '', '', '', '', '', '', '', 0, 0, 0, '', '', '', '', '', '', '2016-08-29 18:03:55', '2016-08-29 12:33:55'),
(36, 'dsfsd', 'dsfdssdf', 1, 1, 1, 'fds', 'sdfs', 'fdf', 1, 1, 1, 'sdfsd', 'SDFSDF', '', '', 0, 0, 0, '', '', '', '', '', '', '', '', '', 0, 0, 0, '', '', '', '', '', '', '2016-08-29 18:04:20', '2016-08-29 12:34:20'),
(37, 'sdfds123', 'sdfsd', 1, 1, 1, 'asdfdsf', '', '', 1, 1, 2, '', '', '', '', 0, 0, 0, '', '', '', '', '', '', '', '', '', 0, 0, 0, '', '', '', '', '', '', '2016-08-29 18:05:16', '2016-08-29 12:37:40'),
(38, 'sdf', 'sdf', NULL, NULL, 3, 'sdf', '', '', NULL, NULL, NULL, '', '', '', '', 0, 0, 0, '', '', '', '', '', '', '', '1', '', NULL, NULL, NULL, '', '', '', '', '', '', '2016-09-06 19:09:17', '2016-09-06 13:39:17');

-- --------------------------------------------------------

--
-- Table structure for table `party_detail`
--

CREATE TABLE IF NOT EXISTS `party_detail` (
  `party_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `party_id` int(11) NOT NULL,
  PRIMARY KEY (`party_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `party_type_1`
--

CREATE TABLE IF NOT EXISTS `party_type_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `party_type_1`
--

INSERT INTO `party_type_1` (`id`, `type`, `created_at`, `updated_at`) VALUES
(5, 'Customer-Export', '2016-09-06 14:43:41', '2016-09-06 09:13:41'),
(6, 'Customer-Domestic', '2016-09-06 14:44:12', '2016-09-06 09:14:12'),
(7, 'Test', '2016-09-06 14:44:30', '2016-09-06 09:14:30');

-- --------------------------------------------------------

--
-- Table structure for table `party_type_2`
--

CREATE TABLE IF NOT EXISTS `party_type_2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `party_type_2`
--

INSERT INTO `party_type_2` (`id`, `type`, `created_at`, `updated_at`) VALUES
(7, 'Other', '2016-09-06 14:51:20', '2016-09-06 09:21:20'),
(8, 'Manufacturer', '2016-09-06 14:51:36', '2016-09-06 09:21:36');

-- --------------------------------------------------------

--
-- Table structure for table `priority`
--

CREATE TABLE IF NOT EXISTS `priority` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `priority` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `priority`
--

INSERT INTO `priority` (`id`, `priority`, `created_at`, `updated_at`) VALUES
(4, 'Medium', '2016-09-08 17:02:23', '2016-09-08 11:32:23'),
(5, 'High', '2016-09-08 17:02:27', '2016-09-08 11:32:27'),
(6, 'Low', '2016-09-08 17:02:29', '2016-09-08 11:32:29');

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE IF NOT EXISTS `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`id`, `project`, `created_at`, `updated_at`) VALUES
(3, 'project1', '2016-09-08 11:39:25', '2016-09-08 06:09:25'),
(4, 'project2', '2016-09-08 11:39:30', '2016-09-08 06:09:30'),
(5, 'project3', '2016-09-08 11:39:36', '2016-09-08 06:09:36');

-- --------------------------------------------------------

--
-- Table structure for table `quotation_reason`
--

CREATE TABLE IF NOT EXISTS `quotation_reason` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quotation_reason` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `quotation_reason`
--

INSERT INTO `quotation_reason` (`id`, `quotation_reason`, `created_at`, `updated_at`) VALUES
(3, 'a', '2016-09-08 17:59:43', '2016-09-08 12:29:43'),
(4, 'b', '2016-09-08 17:59:44', '2016-09-08 12:29:44'),
(5, 'test', '2016-09-08 17:59:49', '2016-09-08 12:29:49');

-- --------------------------------------------------------

--
-- Table structure for table `quotation_stage`
--

CREATE TABLE IF NOT EXISTS `quotation_stage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quotation_stage` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `quotation_stage`
--

INSERT INTO `quotation_stage` (`id`, `quotation_stage`, `created_at`, `updated_at`) VALUES
(3, 'Stage 1', '2016-09-09 10:30:43', '2016-09-09 05:00:43'),
(4, 'Stage 2', '2016-09-09 10:30:47', '2016-09-09 05:00:47'),
(5, 'Stage 3', '2016-09-09 10:30:51', '2016-09-09 05:00:51');

-- --------------------------------------------------------

--
-- Table structure for table `quotation_type`
--

CREATE TABLE IF NOT EXISTS `quotation_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quotation_type` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `quotation_type`
--

INSERT INTO `quotation_type` (`id`, `quotation_type`, `created_at`, `updated_at`) VALUES
(5, 'a', '2016-09-08 17:46:23', '2016-09-08 12:16:23'),
(6, 'b', '2016-09-08 17:46:24', '2016-09-08 12:16:24'),
(7, 'test', '2016-09-08 17:46:32', '2016-09-08 12:16:32');

-- --------------------------------------------------------

--
-- Table structure for table `reference`
--

CREATE TABLE IF NOT EXISTS `reference` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reference` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `reference`
--

INSERT INTO `reference` (`id`, `reference`, `created_at`, `updated_at`) VALUES
(4, '	By Email', '2016-09-06 15:07:59', '2016-09-06 09:37:59'),
(5, 'From Website', '2016-09-06 15:08:12', '2016-09-06 09:38:12'),
(6, 'Search Engine', '2016-09-06 15:08:33', '2016-09-06 09:38:33');

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE IF NOT EXISTS `sales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sales` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`id`, `sales`, `created_at`, `updated_at`) VALUES
(3, 'Export', '2016-09-08 18:15:55', '2016-09-08 12:45:55'),
(4, 'Domestic', '2016-09-08 18:16:01', '2016-09-08 12:46:01');

-- --------------------------------------------------------

--
-- Table structure for table `sales_action`
--

CREATE TABLE IF NOT EXISTS `sales_action` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sales_action` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `sales_action`
--

INSERT INTO `sales_action` (`id`, `sales_action`, `created_at`, `updated_at`) VALUES
(3, 'Action1', '2016-09-09 10:17:01', '2016-09-09 04:47:01'),
(4, 'Action2', '2016-09-09 10:17:05', '2016-09-09 04:47:05'),
(5, 'Action3', '2016-09-09 10:17:10', '2016-09-09 04:47:10');

-- --------------------------------------------------------

--
-- Table structure for table `sales_status`
--

CREATE TABLE IF NOT EXISTS `sales_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sales_status` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `sales_status`
--

INSERT INTO `sales_status` (`id`, `sales_status`, `created_at`, `updated_at`) VALUES
(3, 'Cold', '2016-09-09 10:37:43', '2016-09-09 05:07:43'),
(4, 'Hot', '2016-09-09 10:37:46', '2016-09-09 05:07:46');

-- --------------------------------------------------------

--
-- Table structure for table `sales_type`
--

CREATE TABLE IF NOT EXISTS `sales_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sales_type` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `sales_type`
--

INSERT INTO `sales_type` (`id`, `sales_type`, `created_at`, `updated_at`) VALUES
(3, 'a', '2016-09-08 17:55:20', '2016-09-08 12:25:20'),
(4, 'b', '2016-09-08 17:55:21', '2016-09-08 12:25:21'),
(5, 'test', '2016-09-08 17:55:24', '2016-09-08 12:25:24');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE IF NOT EXISTS `staff` (
  `staff_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `bdate` date NOT NULL,
  `marriagedate` date NOT NULL,
  `qualification` varchar(255) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `middlename` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `mothername` varchar(255) NOT NULL,
  `maritalstatus` varchar(15) NOT NULL,
  `husband_wifename` varchar(255) NOT NULL,
  `bloodgroup` varchar(10) NOT NULL,
  `contactno` varchar(15) NOT NULL,
  `address` text NOT NULL,
  `designation` varchar(255) NOT NULL,
  `department` varchar(255) NOT NULL,
  `grade` varchar(30) NOT NULL,
  `doj` datetime NOT NULL,
  `dol` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`staff_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`staff_id`, `name`, `email`, `pass`, `image`, `bdate`, `marriagedate`, `qualification`, `gender`, `middlename`, `lastname`, `mothername`, `maritalstatus`, `husband_wifename`, `bloodgroup`, `contactno`, `address`, `designation`, `department`, `grade`, `doj`, `dol`, `created_at`, `updated_at`, `is_active`) VALUES
(1, 'admin', 'admin@gmail.com', '61cc0e405f4b518d264c089ac8b642ef', 'staff/user2.jpg', '0000-00-00', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-08-30 11:30:40', 1);

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE IF NOT EXISTS `state` (
  `state_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `state` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`state_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`state_id`, `country_id`, `state`, `created_at`, `updated_at`) VALUES
(1, 1, 'Gujarat', '2016-08-20 17:49:53', '2016-08-20 11:44:34'),
(2, 32, 'uk state', '2016-09-06 12:03:45', '2016-09-06 06:33:45'),
(3, 1, 'demo', '2016-09-06 12:04:48', '2016-09-06 06:34:48'),
(4, 1, 'UP', '2016-09-06 12:12:20', '2016-09-06 06:42:20');

-- --------------------------------------------------------

--
-- Table structure for table `sub_group`
--

CREATE TABLE IF NOT EXISTS `sub_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sub_item_group` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `sub_group`
--

INSERT INTO `sub_group` (`id`, `sub_item_group`, `created_at`, `updated_at`) VALUES
(3, 'new1', '2016-09-07 19:17:18', '2016-09-07 13:47:18'),
(4, 'new2', '2016-09-07 19:17:22', '2016-09-07 13:47:22'),
(5, 'new3', '2016-09-07 19:17:26', '2016-09-07 13:47:26'),
(6, 'sub item', '2016-09-07 19:17:33', '2016-09-07 13:47:33');

-- --------------------------------------------------------

--
-- Table structure for table `terms_condition_detail`
--

CREATE TABLE IF NOT EXISTS `terms_condition_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `group_name1` varchar(255) NOT NULL,
  `group_name2` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `terms_condition_detail`
--

INSERT INTO `terms_condition_detail` (`id`, `code`, `group_name1`, `group_name2`, `description`, `created_at`, `updated_at`) VALUES
(5, 'AVCS', 'sdd', 'Payment', 'Ipsum', '2016-09-09 11:28:26', '2016-09-09 05:58:26'),
(6, 'DJH', 'jhd', 'Packing', 'Lorem', '2016-09-09 11:28:47', '2016-09-09 05:58:47');

-- --------------------------------------------------------

--
-- Table structure for table `terms_condition_group`
--

CREATE TABLE IF NOT EXISTS `terms_condition_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `terms_condition_group`
--

INSERT INTO `terms_condition_group` (`id`, `code`, `description`, `created_at`, `updated_at`) VALUES
(4, 'TPAY', 'Payment', '2016-09-09 11:07:18', '2016-09-09 05:37:18'),
(5, 'TPAK', 'Packing', '2016-09-09 11:07:31', '2016-09-09 05:37:31'),
(6, 'TPED', 'Delivery', '2016-09-09 11:07:42', '2016-09-09 05:37:42');

-- --------------------------------------------------------

--
-- Table structure for table `terms_condition_template`
--

CREATE TABLE IF NOT EXISTS `terms_condition_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template_name` varchar(255) NOT NULL,
  `group_name` varchar(255) NOT NULL,
  `terms_condition` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `terms_condition_template`
--

INSERT INTO `terms_condition_template` (`id`, `template_name`, `group_name`, `terms_condition`, `created_at`, `updated_at`) VALUES
(3, 'Lorem', 'Packing', 'sdfsdf', '2016-09-09 12:08:41', '2016-09-09 06:38:41'),
(4, 'fdsf', 'Delivery', 'dfsf', '2016-09-09 12:08:57', '2016-09-09 06:38:57');

-- --------------------------------------------------------

--
-- Table structure for table `terms_group`
--

CREATE TABLE IF NOT EXISTS `terms_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `terms_group` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `terms_group`
--

INSERT INTO `terms_group` (`id`, `terms_group`, `created_at`, `updated_at`) VALUES
(4, 'Payment', '2016-09-06 15:59:27', '2016-09-06 10:29:27'),
(5, 'Packing', '2016-09-06 15:59:35', '2016-09-06 10:29:35'),
(6, 'Delivery', '2016-09-06 15:59:41', '2016-09-06 10:29:41');

-- --------------------------------------------------------

--
-- Table structure for table `uom`
--

CREATE TABLE IF NOT EXISTS `uom` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uom` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `uom`
--

INSERT INTO `uom` (`id`, `uom`, `created_at`, `updated_at`) VALUES
(3, 'kill', '2016-09-08 17:37:31', '2016-09-08 12:07:31'),
(4, 'C.M.', '2016-09-08 17:37:43', '2016-09-08 12:07:43'),
(5, 'Miter', '2016-09-08 17:37:49', '2016-09-08 12:07:49');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `phone_number` varchar(15) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_type`, `username`, `phone_number`, `email`, `password`, `created_at`, `updated_at`) VALUES
(5, 'Administrator', 'admin', '897981465', 'nikhil.w3nuts@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '2016-09-06 18:25:21', '0000-00-00 00:00:00'),
(6, 'Administrator', 'test', '8789745642', 'testasdasdasd@fama.com', 'e10adc3949ba59abbe56e057f20f883e', '2016-09-06 18:25:43', '0000-00-00 00:00:00'),
(7, 'Administrator', 'demo', '7897894654', 'demo@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '2016-09-06 18:26:03', '0000-00-00 00:00:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
